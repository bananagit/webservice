# AIFap.net - Web service

This repository contains the source code behind the aifap.net web service (i.e. 
the website, database, and the user interface.)

## Setup notes

> WARNING: This isn't a finished product that can be pushed up somewhere and set up quickly. I've done my best to package it up and document how it works, but AIFap was still a WIP at the time this code was dumped, and as such it contains unfinished code, broken features, and some suboptimal decision-making. Proceed at your own risk.
>
> I would have liked to make this easier to set up, but due to lack of time and poor health I'm not able to do much more than this currently.

If you're looking to set this up, follow this super high-level checklist:

- Set up nginx, point at the `public/` directory. Refer to <https://laravel.com/docs/5.8/deployment#nginx> 
- Replace the gfycat credentials in these locations with those of your own gfycat client:
  - `app/Console/Commands/FixGfycatPosts.php:41`
  - `app/Console/Commands/TestIndexPosts.php:35`
  - `app/Services/ImportService.php:60`
- Replace the recservice domain in the these locations with the recservice you've setup:
  - `app/Console/Commands/TrainRecommendations.php:20`
  - `app/Services/RecommendationServiceV3.php:38`
- Replace the S3 bucket in these locations with the one your recservice pulls from:
  - `app/Services/RecommendationService.php:187`
  - `app/Services/RecommendationService.php:212`
  - `app/Services/RecommendationService.php:240`
  - `app/Services/RecommendationService.php:266`
  - `app/Services/RecommendationServiceV3.php:114`
  - `app/Services/RecommendationServiceV3.php:140`
  - `app/Services/RecommendationServiceV3.php:160`
  - `app/Services/RecommendationServiceV3.php:178`
- Replace the Sphinxsearch parameters in `config/sphinxsearch.php` to point to your instance of Sphinx. (see [Search](#search))
- Edit `.env` :
  - `APP_URL` - URL of the app, i.e. `https://example.com`
  - `DB_*` - details of your MySQL server
  - `REDIS_*` - your Redis host
  - `AWS_*` - AWS credentials for the SQS queue and app S3 bucket
  - `AWS_*_2` - AWS credentials for the S3 bucket shared with the recservice
- Run:
  - `composer install` - install PHP dependencies
  - `npm install` - install JS dependencies
  - `php artisan migrate` - create DB schema
  - `php artisan key:generate` - generate encryption key

## Overview

The web service is a Laravel 5.5 app. Refer to the [Laravel documentation](https://laravel.com/docs/5.5) 
for general framework information.

### Service overview

![](doc/aif_diagram_services.png)

### Key data structures

Most data which the web service interacts with are modelled using Eloquent and stored in the databse.

#### Post

![](doc/aif_diagram_post.png)

#### Subreddit

![](doc/aif_diagram_sub.png)

#### Feed session

A feed session represents a user's position and state in a given feed. For example, if you start browsing the New feed, the feed session that is created will keep track of the point you started viewing it - this prevents any pagination issues due to the underlying feed changing.

In the old V1 feed system it kept track of all of the results of a feed. In the V2 feed  system it just keeps track of feed parameters and browsing position.

![](doc/aif_diagram_feedsession.png)

#### Recommendations

In the V1 feed system, recommendations were a relation on the feed session.

![](doc/aif_diagram_rec_v1.png)

In the V2 feed system, recommendations are separate from feed results. They are recorded in sets - each set is the results for a given call to the rec service.

![](doc/aif_diagram_rec_v2.png)



#### Saved List

![](doc/aif_diagram_list.png)



#### Tagging

![](doc/aif_diagram_tag.png)



![](doc/aif_diagram_tagsuggestion.png)



#### User

![](doc/aif_diagram_user.png)

### Application services

`App/Services` contains the majority of the more complex business logic. Each sub-service has a specific focus and set of concerns.

#### FeedService

> The FeedService is responsible for creating and managing different types of feeds.

When we need to retrieve a feed for a given user, we typically use `FeedService::findOrCreateSession`. This looks up an existing feed session (if a matching one exists) and returns it; otherwise it creates a new feed session.

The different types of feed (New, Recommended, Popular, Search, etc.) are identified using one of the `FeedService::FEED_TYPE_*` constants.

Via `FeedService::FEED_TYPE_CLASSES`, each feed type is mapped to a Feed class (under `App\Data\Feeds`). See [Feed System](#feed-system) for more info on how this works.

#### RatingsService

> The RatingsService is responsible for recording and aggregating user ratings.

- `getGlobalSubScores` - used for determining a user's sub ratings
- `getRatingMatrix` - used for calculating the matrix of user-sub scores used in the user profile and the content-based rec algorithms
- `getPopularPosts` - (naively) finds the most popular posts
- `getInfrequentlyRatedSubs`  - used by the Experiment feed

Later on we tried migrating this to use Redis. New ratings would call`RatingsService::recordRating`, which would record the ratings to Redis. Never got around to moving over the rest of the functionality.

#### ViewService

> The ViewService is responsible for recording user views

This was something we added in prep for the migration of ratings+views to Redis. It currently records views to Redis, but doesn't do anything with them.

#### RecommendationsService / RecommendationsServiceV3

> The RecommendationService handles talking to the AIFap Recommendation Service

- `recommendUser` - queries the recservice and populates new recommendation sets for the user.
- `getActiveRecommendationSets` - returns the latest RecommendationSets for a given users for the strategies currently enabled.
- `outputRecommendationsDataV2` - dumps data from the DB into S3 so it can be picked up by the recservice.

#### ReputationService

> The ReputationService implements the User Trust system.

The different `ReputationService::ACTION_*` constants and `ReputationService::ACTIONS` defines all of the possible Trust-related actions. `ReputationService::ABILITY_*` constants define permissions that can be unlocked.

- `getReputationConfig` - returns the different trust levels and the abilities they unlock.
- `getProfile` - returns a compiled profile of the user's current trust and unlocked abilities
- `record` - records an action that user has performed

#### UserSettingService

> Manages user preferences

#### UserSimilarityService

Some experimental stuff to figure out how similar users are. Didn't work very well.

#### SupporterService

This was intended to allow users to link up their Patreon accounts and get extra privileges for donating. Unfinished.

#### ProgressionService

This was intended to be something similar to the User Trust System. It would act as a tutorial system which would incentivize new users to rate items and explore features. Works in the same way as the ReputationService.

#### MetadataService

An experiment we actually completed. This probes MP4/WebM files to determine if they contain audio. It does this without needing to download the entire file.

Works pretty well, but still takes quite a lot of resources.

#### ImportService

Imports BF exports.

#### ExportService

Generates AIFap exports.



## UI

The web service contains two layouts in `resources/views`:

- `welcome.blade.php` - Renders the main web UI
- `prerender.blade.php` - The same as welcome, but contains a pre-rendered version of the homepage for indexing by Google.

These include the compiled version of the frontend app.

All URLs except for those used by the API will serve the frontend app. Routing is handled by Vue.

### Frontend app

The frontend is a Vue app which is compiled using Webpack/Laravel Mix. The entry point is `resources/assets/js/app.js`.

The general structure of the frontend app should be familiar to developers that have worked with Vue SPAs previously. 

Structure:

- `components` - UI components
  - `components/base` - Reusable global components - buttons, text boxes, cards, popups, etc.
  - `components/icons` - Icons - gender types, artstyles, 404 icons, etc.
- `http` 
  - `http/api` - Axios wrapper for interacting with the backend API
  - `http/session` - Keeps track of the current user/settings/trust/blacklists.
- `router` - Application routes
  - `router/index.js` - Main routes definition
  - Each routes subdirectory contains an `index.js` and a set of routes. Each `index.js` defines the routes for a certain part of the site ('my stuff', feeds, profile, about, etc.) The root `index.js` includes all of these.
- `services` - General-purpose UI helpers - alerts, analytics, view tracking, etc.
- `versions` - Version history and changelogs

#### Styling

Styling is handled entirely through Vue. Each component defines it's own styling in it's `<style>` block. 

#### Developing and building

The app can be worked on by running `npm run dev` - this will recompile the frontend app every time a change is made.

To deploy for production you can run `npm run prod`.

## Feed System

### V2

The V2 feed system uses the concept of a 'feed session'. For each type of feed the user can browse, the user can have a 'feed session', which keeps track of various parameters of the feed and the user's position in it. Typically a feed sessions's params are reset whenever the feed is refreshed.

**Creating feed sessions**

On the frontend, feed sessions are created by calling one of the methods in `resources/assets/js/services/feedSession.js#FeedSessions`, i.e. `getSavedListSession(listId)`.  This returns a `feedSession.js#FeedSession`

On the backend, this creates a new `App\FeedSession` associated with the `App\Data\Feeds\SavedListFeed` feed type. The list ID is saved to the session params.

**Using feed sessions**

To view a feed, one of the `feedSession.js#FeedSession` methods is called, i.e. `index(page, position)`. 

On the backend this causes a call to `FeedService::getFeed($session)`, `Feed::getItems()` is called on the returned feed type (in this case an instance of SavedListFeed.) The feed type uses the parameters saved in the session to determine the posts to return (in this case, the posts in the associated list.)

#### Feed types

All feed types are located in `app/Data/Feeds/` and inherit from `App\Data\Feeds\Feed`.

##### Global feeds

*NewPostsFeed* is the 'New' page. It's the simplest feed type and just returns posts in order.

*PopularFeed* is the 'Popular' page. It returns posts ordered by their popular_feed_score (which is regularly updated by the CalculatePopularPosts commands.) It's super naive and no longer efficient enough to really be useful.

*RandomFeed* is the 'Random' page. It returns posts in a random order (seeded by the timestamp when it was last reloaded.)

##### Recommendation feeds

*AggregateRecommendationFeed* is the feed behind the V2 recommendations page. It aggregates posts across the user's latest RecommendationSets. It allows filtering by the recommendation strategy.

*RecommendationFeed* is the feed behind the V1 recommendations page. It returns the user's associated Recommendations.

*LabsRecommendedFeed* was intended to be used for a planned 'Labs' page, where new recommendation methods would be trialed.

##### Feeds for searching and content

*SearchAdvancedFeed* is the feed behind the user-facing search functionality on the site. It interprets the query passed into it as a SearchQuery and uses Sphinx to look up results. See [Search](#Search]) for more info.

*SearchFeed* was used for the old 'quick search' box. It also uses Sphinx but is more limited than the advanced search.

*SubredditFeed* is used when you click on a subreddit badge. It returns all of the posts inside belonging to a subreddit.

*TagFeed* is used when you click on a tag badge. It uses Sphinx to return all of the posts with a certain tag.

##### User-specific feeds

*RatingFeed* is the feed behind the 'Loved', 'Liked + Loved', and the 'Disliked/Hated' pages. It returns only the posts with ratings of one of the ratings types specified in the feed session parameters.

*ExperimentFeed* is the feed behind the Experiment page. It uses RatingsService to determine infrequently-rated subreddits, then scores posts by their position in this list. It stores some random seeds in the feed session when it reloads, then uses these to introduce Gauss noise into the result scores. This prevents posts from being repetitive too much but preserves the general order.

*PrivatePostsFeed* is the feed behind the 'Private Posts' page. It simply returns the private posts for the current user in order. Supports shuffling.

*ViewedFeed* is the feed behind the 'Viewed' page. It returns all of the posts the current user has viewed.

*WatchLaterFeed* is the feed behind the 'Watch Later' page. It returns all of the posts in the current users' watch later queue.

##### Feeds related to saved lists

*SavedListFeed* is the feed behind the 'Browse List' page. It returns all of the posts in the saved list that the current user can view.

##### Misc feeds

*SinglePostFeed* is used when a user opens a link to a feed session they don't own. The user is instead redirected to a new session of SinglePostFeed which contains only the post from the URL. This allows users to share posts with each other via URL.

*TestingFeed* is a debug feed used to test out new concepts.

### V1

The V1 feed system shouldn't be used anywhere anymore.

In the V1 system, controllers would directly create Recommendations and associate them with users.

## Recommendations

**V3**

V3 was a WIP update to the V2 system. As of the latest changes there isn't really much difference, but in some places the code might refer to V3 and V2 seemingly interchangeably.

### V2

V2 is the current version of the recommendation system. V2 moved most of the work of generating recommendations away from the DB and switched to python for the actual ML code.

The code for the actual recommendation system itself (the 'AIFap recservice') was moved to a different service, which has been released separately.

Although the recservice handles generating recommendation, the web service is still responsible for managing it and handling it's output.

#### Steps to generate recommendations

Training:

- The TrainRecommendations command:
  - Calls `RecommendationServiceV3::outputRecommendationsDataV2` - this dumps the data from the DB onto S3
  - Hits `http://recs.services.aifap.net/train` - this triggers the recservice to begin training

Recommending:

- The frontend creates an `AggregateRecommendationFeed` (see V2 feed system)
- `AggregateRecommendationFeed::refresh()`:
  - Calls `RecommendationServiceV3::needsRecommendations`, which indicates the user needs fresh recommendations
  - Calls `RecommendationServiceV3::recommendUser`, which:
    - Hits ``http://recs.services.aifap.net/recommend/{user_id}`, which returns the recommendation results
    - Excludes results which the user has already viewed
    - Creates a `RecommendtionSet` for each strategy in the results
    - Populates `RecommendationResult` into each of the new sets
  - Returns recommendations from the user's latest `RecommendationSet`s

### V1

V1 is the legacy version of the recommendation system. V1 is characterized by direct calls to the DB, which means it became extremely resource-intensive as the DB reached a certain size.

**Recommendation strategies**

V1 rec strategies are stored in `app\Data\Strategies`. This actually contains older versions of the V2 rec strategies as well, back when each strategy was run locally on the web server. These can all be ignored now.

## Indexing

### V2

The V2 indexer actually lives in it's own repo over here:  <https://gitlab.com/aifap/indexer> 

It would have been a much needed improvement over the V1 system.

It was intended to replace the V1 system, but had a few issues. We open-sourced it but got no real interest from the community. Never got around to making it work.

### V1

The V1 indexer is actually testing code that was written in the first few days of this project.

It can be found in it's entirety in  `app\Console\Commands\TestIndexPosts.php`.

#### Type guessers

The V1 indexer guesses the gender/art/media type of posts using a `App\Data\ArtstyleTypeGuesser`, `MediaTypeGuesser` and `GenderTypeGuesser`, together with a `App\TypeHint`.

A TypeHint is associate with every Subreddit in the system. The TypeHint contains various flags (i.e. `always straight`, `always includes men`, `never solo`, `bans hentai`, etc.). The guessers use the flags and post metadata to determine the post's type.

**GenderTypeGuesser** primarily guesses by looking for patterns in the post title, i.e. "(m25)" to indicate a male, or "threesome" to indicate that the post is non-solo.

**MediaTypeGuesser** looks at the URL of the media file and guesses based either on the file extension (".gif" -> animated, ".jpg" -> imge, etc.), or the domain ("gfycat.com" -> animated, "pornhub.com" -> video", etc.)

**ArtstyleTypeGuesser** is the weakest of the three. It assumes posts are real-life, unless the sub only allows hentai or 3d, in which case it guesses one of those.

#### Duplicate checker

The V1 indexer uses an `App\Data\DuplicateChecker` to avoid indexing duplicates. More precisely, it indexes everything, then doesn't save posts that the DuplicateChecker flags.

The DuplicateChecker is pretty dumb and just compares the media URL to those already in the database.

In the event of a duplicate, DuplicateChecker uses the gender guess from the new post to  update the gender of the existing one, if it's unknown.

## Search

The web service uses Sphinx for web indexing. The configuration for Sphinx is found in `sphinx.conf`. The Sphinx indexer is configured to run every 10 minutes.

The search is very fast and powerful, using Sphinx was one of the better choices we made on this project.

### Search feeds

Searching primarily happens in one of the search feeds:

- **SearchAdvancedFeed** - used for the user-facing search on the site
- **SearchFeed** - used for the old 'quick-search' feature
- **TagFeed** - used to find posts with a certain tag

### Search queries

When a user performs a search, a new SearchAdvancedFeed feed session is created and the search query is passed in as a feed session param named 'query'. This is parsed into an `App\Data\Search\SearchQuery`, using `SearchQuery::parse`

A `SearchQuery` breaks up the user's input into a series of `SearchQueryTerm`.

Each term has a certain type:

- **TYPE_TAG ('tag')** - Match posts with a certain tag
- **TYPE_SOURCE ('source')** - Match posts belonging to a certain subreddit
- **TYPE_TITLE ('title')** - Match posts with certain terms in the title
- **TYPE_META ('meta')** - Match posts that meet special conditions.

Valid values for terms depending on type:

- **TYPE_TAG** - `key` is the tag key. `ns` is the tag namespace's short or long key, or null to match all namespaces.
- **TYPE_SOURCE** - `key` is the subreddit slug.
- **TYPE_TITLE** - `key` is the terms to match in the title.
- **TYPE_META** - `key` is:
  - `"audio" or "sound"` - Match posts containing audio.

Examples:

```
Input: 
	breasts source:nsfw_gif m:cum title:"red$ dress" -bimbo

SearchQuery:
	Term {type: TYPE_TAG, ns: null, key: "breasts", negate: false}
	Term {type: TYPE_SOURCE, ns: null, key: "nsfw_gif", negate: false}
	Term {type: TYPE_TAG, ns: "m", key: "cum", negate: false}
	Term {type: TYPE_TITLE, ns: null, key: "red$ dress", negate: false}
	Term {type: TYPE_TAG, ns: null, key: "bimbo", negate: true}

```

This is transformed into a Sphinx query using `SearchQuery::createSphinxQuery`.



## Commands

Console commands are found under `app/Console/Commands`.

Many of the commands run on an automated schedule, which can be found in `App\Console\Kernel`.

- **BanPost** 
  `php artisan post:ban {id} {reason}`
  Bans a post in the DB.

- **CalculatePopularPosts**
  `php artisan test:calculate_popular_posts`
  Updates posts' popular_feed_score

- **CalculateTFIDF**
  `php artisan calculate:tfidf`
  An experiment into calculating TF-IDF for post titles.

- **CleanFeedSessions**
  `php artisan clean:feed_sessions`
  Deletes feed sessions older than 3 days

- **DeduplicatePosts**
  `php artisan post:deduplicate`
  Merges and deletes duplicate posts

- **FixGfycatPosts**
  `php artisan posts:fix_gfycat`
  Fixes an issue we had once to do with gfycat post ID sensitivity

- **GenerateRecommendations**
  `php artisan recommendations:generate`
  Old V1 rec system: populates new recommendations for all users

- **ImportMigrationToRedis**
  `php artisan app:import_migration_to_redis`
  Part of our migration of ratings+views to redis. Never completed.

- **MergeDuplicatePosts**
  `php artisan posts:merge_duplicates`
  Old version of DeduplicatePosts from before we had the DuplicateChecker

- **TagSub**
  `php artisan tag:sub {sub} {tag}`
  Add a new auto sub tag and adds it to all existing posts in that sub

- **TestIndexMetadata**
  `php artisan test:index_metadata`
  Runs the MetadataService over posts that need to be checked for audio.

- **TestIndexPosts**
  `php artisan test:index`
  The V1 indexer. See [Indexing](#Indexing)

- **TrainRecommendations**
  `php artisan recommendations:train`
  Train V2 recservice recommendations

  












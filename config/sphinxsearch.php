<?php

return [
	'host'    => '127.0.0.1',
	'port'    => 9312,
	'indexes' => [
		'test1' => [
		    'table' => 'posts', 
		    'column' => 'id'
		],
	]
];
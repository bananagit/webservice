<?php
return [
    'show_video_controls' => true,
    'colorblind_mode' => false,
    'use_new_viewer' => false,
];
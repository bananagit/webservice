<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Tag;

class TagAlias extends Model {
    protected $table = 'tag_aliases';
    
    public function tag() {
        return $this->belongsTo(Tag::class, 'tag_id');
    }
}

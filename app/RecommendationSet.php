<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\RecommendationResult;

class RecommendationSet extends Model {
    protected $table = 'recommendation_sets';
    
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function results() {
        return $this->hasMany(RecommendationResult::class, 'set_id');
    }
}

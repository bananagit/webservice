<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Post;

class UserImport extends Model {
    const STATUS_QUEUED = 'queued';
    const STATUS_RUNNING = 'running';
    const STATUS_COMPLETE = 'complete';
    
    const TYPE_BF_LIKES = 'bf-likes';
    const TYPE_BF_FAVORITES = 'bf-favorites';
    
    protected $table = 'user_imports';
    
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function posts() {
        return $this->hasMany(Post::class, 'user_import_id');
    }
}

<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Post;


class UserPostTagging extends Model {
    protected $table = 'user_post_taggings';
    
    public $casts = [
        'old_tag_ids' => 'array',
        'new_tag_ids' => 'array',
    ];
    
    public $appends = [
        'new_tags',
        'old_tags',
    ];
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function post() {
        return $this->belongsTo(Post::class, 'post_id');
    }
    
    public function getNewTagsAttribute() {
        return Tag::whereIn('id', $this->new_tag_ids)->get();
    }
    public function getOldTagsAttribute() {
        return Tag::whereIn('id', $this->old_tag_ids)->get();
    }
    
    public function revert() {
        $post = $this->post;
        $post->tags()->sync($this->old_tag_ids);
        
        $this->reverted = true;
        $this->save();
    }
}

<?php
namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Services\ExportService;
use App\UserExport;

class ProcessUserExportJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $export;

    public function __construct(UserExport $export) {
        $this->export = $export;
    }

    public function handle() {
        ExportService::processExport($this->export);
    }
}

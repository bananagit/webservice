<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Tag;

class AutoTitleTag extends Model {
    protected $table = 'auto_title_tags';
    
    public function tag() {
        return $this->belongsTo(Tag::class, 'tag_id');
    }
}

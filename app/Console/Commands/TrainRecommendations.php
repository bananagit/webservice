<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;

class TrainRecommendations extends Command {
    protected $signature = 'recommendations:train';
    protected $description = 'Command description';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $this->info('Outputting training data...');
        \App\Services\RecommendationsService::outputRecommendationsDataV2();
        
        $this->info('Training...');
        $client = new Client([
            'base_uri' => 'http://REPLACEME/',
            'timeout'  => 1200,
        ]);
        $res = $client->request('GET', 'train');
        $this->info((string)$res->getBody());
    }
    
    public function old_handle() {
        $contentNewV1 = new \App\Data\Strategies\ContentNewV1;
        $collabUserV1 = new \App\Data\Strategies\CollabUserV1;
        $collabUserV2 = new \App\Data\Strategies\CollabUserV2;
        $collabUserV3 = new \App\Data\Strategies\CollabUserV3;
        
        $this->info('Outputting training data');
        \App\Services\RecommendationsService::outputRecommendationsDataV2();
        $this->info('Training ContentNewV1');
        $contentNewV1->train();
//        $this->info('Training CollabUserV1');
 //       $collabUserV1->train();
  //      $this->info('Training CollabUserV2');
   //     $collabUserV2->train();
        $this->info('Training CollabUserV3');
        $collabUserV3->train();
        $this->info('Training complete');
    }
}

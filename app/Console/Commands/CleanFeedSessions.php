<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\FeedSession;


class CleanFeedSessions extends Command {
    protected $signature = 'clean:feed_sessions';
    protected $description = 'Deletes feed sessions that haven\'t been accessed for 3 days.';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        FeedSession::where('updated_at', '<', new \Carbon\Carbon('-3 days'))
            ->delete();
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestGenRecSample extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:gen_rec_sample';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userIds = [
            1,
        ];
        
        $content = '';
        
        foreach ($userIds as $userId) {
            $ids = \App\Post::select('id')
                ->where('is_private', false)
                ->whereDoesntHave('views', function($q) use ($userId) { 
                    $q->where('user_id', $userId); 
                })
                ->limit(10000)
                ->chunk(1000, function($posts) use ($userId, &$content) {
                    foreach ($posts as $post) { 
                        $content .= "$userId,$post->id,0\n"; 
                    }
                });
        }
        
        file_put_contents(storage_path() . '/user_rec_sample.csv', $content);
    }
}

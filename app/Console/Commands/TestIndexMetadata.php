<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Post;
use App\Services\MetadataService;

class TestIndexMetadata extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:index_metadata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $i = 0;
        Post::orderBy('id', 'desc')->where('is_private', false)->whereNull('metadata_updated_at')->chunk(100, function($chunk) use (&$i) {
            foreach ($chunk as $post) {
                MetadataService::refreshMetadata($post);
                $i++;
                $this->info($i . ' -- #' . $post->id . ' -- ' . $post->title);
            }
        });
    }
}

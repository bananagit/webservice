<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TagSub extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tag:sub {sub} {tag}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tagKey = $this->argument('tag');
        $subSlug = $this->argument('sub');
        
        $sub = \App\Subreddit::where('slug', $subSlug)->firstOrFail();
        $tag = \App\Tag::where('key', $tagKey)->firstOrFail();
        
        if ($sub->defaultTags()->where('tags.id', $tag->id)->first()) {
            $this->info('already tagged');
            return;
        }
        
        $sub->defaultTags()->attach($tag);
        
        $count = 0;
        foreach ($sub->posts as $post) {
            if ($post->tags()->where('tags.id', $tag->id)->first()) {
                continue;
            }
            $post->tags()->attach($tag);
            $count++;
        }
        $this->info("tagged $count posts");
    }
}

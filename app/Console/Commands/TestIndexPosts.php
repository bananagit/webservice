<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Data\FilterConstants;
use App\Data\GenderTypeGuesser;
use App\Data\MediaTypeGuesser;
use App\Data\ArtstyleTypeGuesser;
use App\Data\AutoTagger;
use App\Data\DuplicateChecker;
use App\Post;
use App\Subreddit;

class TestIndexPosts extends Command {
    protected $signature = 'test:index_posts';
    protected $description = 'Command description';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $genderTypeGuesser = new GenderTypeGuesser;
        $mediaTypeGuesser = new MediaTypeGuesser;
        $artstyleTypeGuesser = new ArtstyleTypeGuesser;
        
        $autoTagger = new AutoTagger;
        $duplicateChecker = new DuplicateChecker;
        
        $postsToBeSavedBySub = [];
        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', 'https://api.gfycat.com/v1/oauth/token', [
            'json' => [
                'grant_type' => 'client_credentials',
                'client_id' => 'REPLACEME',
                'client_secret' => 'REPLACEME',
            ],
        ]);
        $data = json_decode($res->getBody());
        
        $gfyToken = $data->access_token;
        
        foreach (Subreddit::where('should_index', true)->where('slug', '!=', '_unknown')->get() as $subreddit) {
            $postsToBeSavedBySub[$subreddit->id] = [];
            
            $postIndex = 0;
            $firstPost = null;
            if ($subreddit->posts()->exists()) {
                while(($postIndex == 0 || $firstPost) && $postIndex < 200) {
                    if ($subreddit->id == 1 || $subreddit->id == 82 || $subreddit->id == 86 || $subreddit->id == 175) { break; }
                    $firstPost = $subreddit->posts()->orderBy('id', 'desc')->skip($postIndex)->first();
                    if (!$firstPost) {
                        continue;
                    }
                    
                    try {
                        $res = $client->request('GET', 'https://www.reddit.com/r/' . $subreddit->slug . '/new.json', [
                            'headers' => [
                                'User-Agent' => 'AIF',
                            ],
                            'query' => [
                                'after' => $firstPost->fullname,
                                'limit' => 1,
                            ],
                        ]);
                        
                        $data = json_decode($res->getBody())->data;
                        $after = $data->after;
                        $posts = $data->children;
                        
                        if (count($posts) == 0) {
                            $this->info('post deleted, finding next - ' . $firstPost->fullname);
                            $postIndex++;
                        } else {
                            break;
                        }
                    } catch (\Exception $ex) {
                        $this->info('error, finding next - ' . $firstPost->fullname . ': '. $ex->getMessage());
                        $postIndex++;
                    }
                }
                
            }
            
            
            $res = $client->request('GET', 'https://www.reddit.com/r/' . $subreddit->slug . '/new.json', [
                'headers' => [
                    'User-Agent' => 'AIF',
                ],
                'query' => [
                    'before' => $firstPost ? $firstPost->fullname : null,
                    'limit' => 100,
                ],
            ]);
            
            $data = json_decode($res->getBody())->data;
            $after = $data->after;
            $posts = $data->children;
            
            
            foreach ($posts as $postData) {
                if (Post::where('fullname', 't3_' . $postData->data->id)->first()) {
                 //   $this->info('skip - ' . $postData->data->title);
                    continue;
                }
                
                $postData = $postData->data;
                if (!property_exists($postData, 'permalink')) {
                    continue;
                }
                
                $post = new Post;
                $post->subreddit()->associate($subreddit);
                
                $post->fullname = 't3_' . $postData->id;
                $post->permalink = $postData->permalink;
                $post->title = $postData->title;
                $post->author = $postData->author;
                if ($post->author != '[deleted]') {
                    $post->author_fullname = $postData->author_fullname;
                } else {
                    $post->author_fullname = '[deleted]';
                }
                $post->posted_at = \Carbon\Carbon::createFromTimestamp($postData->created_utc);
                
                $post->subreddit_title = $postData->subreddit;
                
                $post->media_url = $postData->url;
                $post->media_domain = $postData->domain;
                
                if ($postData->media) {
                    $post->thumbnail_width = $postData->media->oembed->thumbnail_width ?? null;
                    $post->thumbnail_height = $postData->media->oembed->thumbnail_height ?? null;
                    $post->width = $postData->media->oembed->width ?? null;
                    $post->height = $postData->media->oembed->height ?? null;
                    $post->thumbnail_url = $postData->media->oembed->thumbnail_url ?? null;
                    
                    // Fix gfycat id casing
                    if (strpos($post->thumbnail_url, 'https://thumbs.gfycat.com/') === 0) {
                        preg_match('/.*\/(.*)-.*\.gif/m', $post->thumbnail_url, $matches);
                        try {
                            $post->media_url = 'https://gfycat.com/' . $matches[1];
                        } catch (\Exception $ex) {
                        }
                    }
                }
                
                if (strpos($post->media_url, 'gfycat.com/') !== false) {
                    try {
                        preg_match('/^https?:\/\/(?:.*\.)?gfycat\.com\/(?:.*\/)?(.*?)(?:-.*)?(?:\..*)?$/m', $post->media_url, $matches);
                        $gfyId = $matches[1];
                        $res = $client->request('GET', 'https://api.gfycat.com/v1/gfycats/' . $gfyId, [
                            'headers' => [
                                'Authorization' => 'Bearer ' . $gfyToken,
                            ],
                        ]);
                        $body = $res->getBody();
                        $gfyPost = json_decode($body);
                        
                        $post->media_url = $gfyPost->gfyItem->webmUrl;
                        $post->thumbnail_url = $gfyPost->gfyItem->gif100px;
                    } catch (\Exception $ex) {
                        dump($ex);
                        dump($ex->getResponse()->getBody()->getContents());
                        $content = $ex->getResponse()->getBody()->getContents();
                        if (strpos($content, 'not exist') !== false) {
                            continue;
                        }
                        if (strpos($content, 'many requests') !== false) {
                            sleep(10);
                            continue;
                        }
                    }
                }
                
                if (strpos($post->media_url, 'reddit.com/r/')) {
                    continue;
                }
            
                
                $post->description = $postData->selftext;
                
                $post->type_gender = $genderTypeGuesser->guess($post, $subreddit, $postData);
                $post->type_media = $mediaTypeGuesser->guess($post, $subreddit, $postData);
                $post->type_artstyle = $artstyleTypeGuesser->guess($post, $subreddit, $postData);
                
                $postsToBeSavedBySub[$subreddit->id][] = $post;
            }
            
            $this->info('/r/' . $subreddit->slug . ' -- ' . count($postsToBeSavedBySub[$subreddit->id]) . ' posts');
        }
        
        
        // Mix up post IDs between all the subs
        for ($j = 99; $j >= 0; $j--) {
            foreach ($postsToBeSavedBySub as $postlist) {
                if (!isset($postlist[$j])) {
                    continue;
                }
                $post = $postlist[$j];
                
                if ($duplicateChecker->isDuplicate($post)) {
                    $this->info('Deduped: ' . $post->title);
                    $duplicateChecker->mergeNewPost($post);
                    continue;
                }
                
                $post->save();
                $autoTagger->tagPost($post);
            }
        }
        
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Post;

class CalculateTFIDF extends Command {
    protected $signature = 'calculate:tfidf';

    protected $description = 'Command description';
    
    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        // TF(t) = (Number of times term t appears in a document) / (Total number of terms in the document)
        // IDF(t) = log_e(Total number of documents / Number of documents with term t in it).
        // Value = TF * IDF
        
        $commonWords = \Storage::get('common_words.txt');
        $commonWords = array_map('trim', explode("\n", $commonWords));
        
        $allTitles = Post::select('title')->get()->pluck('title');
        $titlesCount = count($allTitles);
        
        $this->info('total titles: ' . $titlesCount);
        
        $allTermsTF = [];
        foreach ($allTitles as $title) {
            $terms = explode('-', str_slug($title));
            
            $freq = array_count_values($terms);
            $termsCount = count($terms);
            
            foreach ($freq as $term => $count) {
                if (is_numeric($term)) {
                    continue;
                }
        /*        if (in_array($term, $commonWords)) {
                    continue;
                }
         */       
                if (!isset($allTermsTF[$term])) {
                    $allTermsTF[$term] = [];
                }
                $allTermsTF[$term][] = $count / $termsCount;
            }
        }
        
        $this->info('total terms: ' . count($allTermsTF));
        
        $allTermsIdf = [];
        foreach ($allTermsTF as $term => $tfs) {
            $idf = log($titlesCount / count($tfs));
            $allTermsIdf[$term] = $idf;
        }
        
        arsort($allTermsIdf);
        
        /*
        $allTermsIdf = array_filter($allTermsIdf, function ($v) {
            return $v > 6;
        });
        */
        
        
        $this->info('top idf');
        
        dump(count($allTermsIdf));
        
        $last = 999;
        foreach ($allTermsIdf as $term => $idf) {
            if ($last - $idf < 0.3) {
                continue;
            }
            $last = $idf;
            $this->info($term . ': ' . $idf);
        }
        
        $testIds = [192,4928,4985,3812,12394,20492, 1923, 4912, 283,4943,7853,8223];
        foreach ($testIds as $id) {
            $title = $allTitles[$id];
            $terms = explode('-', str_slug($title));
            
            $this->info('title: ' . $title);
            foreach ($terms as $term) {
                $score = -1;
                if (isset($allTermsIdf[$term])) {
                    $score = $allTermsIdf[$term];
                }
                $this->info(' - ' . $term . ': ' . $score);
            }
        }
        
        
        
    }
}

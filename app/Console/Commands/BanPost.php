<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BanPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:ban {id} {reason}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $post = \App\Post::findOrFail($this->argument('id'));
        
        $post->removed = true;
        $post->removed_by = 'admin';
        $post->removed_message = $this->argument('reason');
        $post->save();
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Post;
use App\PostRating;


class CalculatePopularPosts extends Command {
    protected $signature = 'test:calculate_popular_posts';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $posts = Post::notRemoved()
                    ->where('is_private', false)
                    ->where('created_at', '>', new \Carbon\Carbon('-7 days'))
                    ->has('ratings')
                    ->get();
                    
        foreach ($posts as $post) {
            $score = $post->ratings()
                ->select('*', \DB::raw("SUM(CASE WHEN type IN ('like', 'love') THEN 1 ELSE -1 END) as score"))
                ->where('created_at', '>', new \Carbon\Carbon('-7 days'))
                ->first()
                ->score;
            
            $post->popular_feed_score = $score;
            $post->save();
        }
    }
}

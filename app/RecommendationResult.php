<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\RecommendationSet;
use App\Post;

class RecommendationResult extends Model {
    protected $table = 'recommendation_results';
    
    public function recommendationSet() {
        return $this->belongsTo(RecommendationSet::class, 'set_id');
    }
    
    public function post() {
        return $this->belongsTo(Post::class, 'post_id');
    }
}

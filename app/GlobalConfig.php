<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalConfig extends Model {
    public static function getValue($key) {
        $config = GlobalConfig::where('key', $key)->first();
        if (!$config) {
            return null;
        }
        return $config->value;
    }
    
    public static function setValue($key, $value) {
        $config = GlobalConfig::where('key', $key)->first();
        if (!$config) {
            $config = new GlobalConfig;
            $config->key = $key;
        }
        $config->value = $value;
        $config->save();
    }
}

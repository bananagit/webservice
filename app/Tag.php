<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Post;
use App\User;
use App\Subreddit;
use App\AutoTitleTag;
use App\TagNamespace;
use App\TagAlias;

class Tag extends Model {
    protected $table = 'tags';
    public $with = [
        'tagNamespace',
    ];
    
    public function tagNamespace() {
        return $this->belongsTo(TagNamespace::class, 'namespace_id');
    }
    
    public function aliases() {
        return $this->hasMany(TagAlias::class, 'tag_id');
    }
    
    public function posts() {
        return $this->belongsToMany(Post::class, 'post_tags', 'tag_id', 'post_id');
    }
    
    public function users() {
        return $this->belongsToMany(User::class, 'post_tags', 'tag_id', 'user_id');
    }
    
    public function defaultSubreddits() {
        return $this->belongsToMany(Subreddit::class, 'default_sub_tags', 'tag_id', 'subreddit_id');
    }
    
    public function autoTitleTags() {
        return $this->hasMany(AutoTitleTag::class, 'tag_id');
    }
    
    public function usersBlacklisted() {
        return $this->belongsToMany(User::class, 'blacklisted_tags', 'tag_id', 'user_id');
    }
    
    
    public static function findByKey($key) {
        return Tag::where('key', $key)->first();
    }
    
    
    public function scopeNamespaceString($q, $ns) {
        if (!$ns) {
            return;
        }
        $tagNamespace = TagNamespace::where('key', $ns)
            ->orWhere('shortkey', $ns)
            ->first();
            
        if ($tagNamespace) {
            $q->where('namespace_id', $tagNamespace->id);
        }
        return $q;
    }
}

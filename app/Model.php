<?php
namespace App;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel {
    public function scopeWithAndWhereHas($query, $relation, $constraint){
        return $query->whereHas($relation, $constraint)
                     ->with([$relation => $constraint]);
    }
}
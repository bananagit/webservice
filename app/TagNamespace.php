<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Tag;


class TagNamespace extends Model {
    protected $table = 'tag_namespaces';
    
    public function tags() {
        return $this->hasMany(Tag::class, 'namespace_id');
    }
}

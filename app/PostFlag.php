<?php

namespace App;

use App\Model;
use App\User;
use App\Post;

class PostFlag extends Model {
    protected $table = 'post_flags';
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function post() {
        return $this->belongsTo(Post::class, 'post_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Post;
use App\Recommendation;

class FeedSession extends Model {
    protected $table = 'feed_sessions';
    
    public $casts = [
        'filters' => 'array',
        'params' => 'array',
    ];
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function beforePost() {
        return $this->belongsTo(Post::class, 'before_post_id');
    }
    
    public function afterPost() {
        return $this->belongsTo(Post::class, 'after_post_id');
    }
    
    public function recommendations() {
        return $this->hasMany(Recommendation::class, 'session_id');
    }
    
    
    public function getFilters() {
        $r = $this->filters;
        if (!$r) {
            return [
                'gender_type' => [],
                'media_type' => [],
                'artstyle_type' => [],
            ];
        }
        return $r;
    }
    
    public function getParams() {
        $r = $this->params;
        if (!$r) {
            return [];
        }
        return $r;
    }
    
    public function setParam($key, $default, $format = null) {
        $params = $this->params;
        if (!$params) {
            $params = [];
        }
        $value = $default;
        if ($default instanceof \Closure) {
            $value = $default();
        }
        if ($format == 'array') {
            $params[$key] = json_encode($value);
        } else {
            $params[$key] = $value;
        }
        $this->params = $params;
        return $value;
    }
    
    /**
     * Retrieves a value from this session's params, or
     * sets it to a default if it's not provided. Default can be a callback.
     * Remember to call save() afterwards.
     */
    public function param($key, $default, $format = null) {
        $params = $this->params;
        if (!$params) {
            $params = [];
        }
        
        if (isset($params[$key])) {
            if ($format == 'array') {
                return json_decode($params[$key], true);
            }
            return $params[$key];
        }
        $value = $default;
        if ($default instanceof \Closure) {
            $value = $default();
        }
        if ($format == 'array') {
            $params[$key] = json_encode($value);
        } else {
            $params[$key] = $value;
        }
        $this->params = $params;
        return $value;
    }
    
    public function resetParams($keys) {
        $params = $this->params;
        if (!$params) {
            $params = [];
        }
        $this->params = array_diff_key($params, array_flip($keys));
    }
}

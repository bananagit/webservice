<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Post;

class UserPostAmend extends Model {
    protected $table = 'user_post_amends';
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function post() {
        return $this->belongsTo(Post::class, 'post_id');
    }
    
    
    public function revert() {
        $post = $this->post;
        $post->type_gender = $this->old_type_gender;
        $post->type_artstyle = $this->old_type_artstyle;
        $post->save();
        
        $this->reverted = true;
        $this->save();
    }
}

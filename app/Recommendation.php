<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\FeedSession;
use App\Post;

class Recommendation extends Model {
    protected $table = 'recommendations';
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function session() {
        return $this->belongsTo(FeedSession::class, 'session_id');
    }
    
    public function post() {
        return $this->belongsTo(Post::class, 'post_id');
    }
    
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Tag;

class TagController extends Controller {
    const MAX_RESULTS = 8;
    
    public function search(Request $request) {
        $query = $request->get('query');
        $query = str_replace('%', '', $query);
        
        $ns = null;
        $parts = explode(':', $query);
        if (count($parts) == 2) {
            $ns = $parts[0];
            $query = $parts[1];
        }
        
        // Exact matches
        $results = Tag::where('key', $query)
                      ->namespaceString($ns)
                      ->take(self::MAX_RESULTS)
                      ->with('tagNamespace')
                      ->get();
        
        if (count($results) < self::MAX_RESULTS) {
            $prefixMatches = Tag::where('key', 'like', $query . '%')
                ->namespaceString($ns)
                ->take(self::MAX_RESULTS)
                ->with('tagNamespace')
                ->get();
            $results = $results->merge($prefixMatches);
            if (count($results) < self::MAX_RESULTS) {
                // Infix matches
                $infixMatches = Tag::where('key', 'like', '%' . $query . '%')
                    ->namespaceString($ns)
                    ->take(self::MAX_RESULTS)
                    ->with('tagNamespace')
                    ->get();
                $results = $results->merge($infixMatches);
                if (count($results) < self::MAX_RESULTS) {
                    // Description matches
                    $descriptionMatches = Tag::where('description', 'like', '%' . $query . '%')
                        ->namespaceString($ns)
                        ->take(self::MAX_RESULTS)
                        ->with('tagNamespace')
                        ->get();
                    $results = $results->merge($descriptionMatches);
                }
            }
        }
        
        $results = $results->take(self::MAX_RESULTS);
        
        return response()->json([
            'success' => true,
            'results' => $results,
        ]);
    }
}

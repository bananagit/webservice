<?php
namespace App\Http\Controllers\API;

use League\Csv\Reader;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;

use App\Services\ReputationService;

use App\Subreddit;
use App\Tag;
use App\TagNamespace;
use App\UserPostAmend;
use App\UserPostTagging;

class AdminController extends Controller {
    public function indexAmends(ModeratorRequest $request) {
        $amends = UserPostAmend::query()
                ->with('user', 'post')
                ->orderBy('id', 'desc')
                ->paginate(50)
                ->items();
        
        return response()->json([
            'success' => true,
            'amends' => $amends,
        ]);
    }
    
    public function importSubreddits(AdminRequest $request) {
        $file = $request->file('import');
        $path = $file->store('uploads/subreddit_imports');
        
        
        $csv = Reader::createFromPath($path, 'r');
        
        $subs = [];
        
        foreach ($csv as $row) {
        }
    }
}

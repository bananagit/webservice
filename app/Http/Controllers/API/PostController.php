<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;


use App\Data\RatingConstants;
use App\Http\Requests\ListPostRequest;
use App\Http\Requests\RatePostRequest;
use App\Http\Requests\AmendPostRequest;
use App\WatchLaterEntry;
use App\User;
use App\Post;
use App\PostView;
use App\PostRating;
use App\PostFlag;
use App\Subreddit;
use App\Services\FeedService;
use App\Services\ReputationService;
use App\Services\ViewService;
use App\Services\RatingsService;
use App\UserPostAmend;
use App\UserPostTagging;
use App\Tag;

class PostController extends Controller {
    public function flag(Request $request, Post $post) {
        $user = $request->user();
        
        if ($post->is_private && $post->owner_user_id != $request->user()->id) {
            return response()->json([
                'success' => false,
                'message' => 'Cannot flag another users private post'
            ]);
        }
        
        $flag = new PostFlag;
        $flag->user()->associate($user);
        $flag->post()->associate($post);
        $flag->type = $request->get('type');
        $flag->message = $request->get('message');
        $flag->save();
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    public function rate(RatePostRequest $request, Post $post) {
        $user = $request->user();
        $ratingType = $request->get('rating');
        
        if ($post->is_private && $post->owner_user_id != $request->user()->id) {
            return response()->json([
                'success' => false,
                'message' => 'Cannot rate another users private post'
            ]);
        }
        
        $post->ratings()
            ->where('user_id', $user->id)
            ->delete();
            
        if ($ratingType == 'neutral') {
            return response()->json([
                'success' => true,
                'rating' => null,
            ]);
        }
        
        $rating = new PostRating;
        $rating->user()->associate($user);
        $rating->post()->associate($post);
        $rating->type = $ratingType;
        $rating->save();
        
        RatingsService::recordRating($user->id, $post->id, $ratingType);
        
        return response()->json([
            'success' => true,
            'rating' => $rating,
        ]);
    }
    
    public function tag(AmendPostRequest $request, Post $post) {
        $tags = $request->get('tags');
        
        if ($post->is_private && $post->owner_user_id != $request->user()->id) {
            return response()->json([
                'success' => false,
                'message' => 'Cannot tag another users private post.'
            ]);
        }
        
        if (!$post->is_private) {
            $reputation = ReputationService::getProfile($request->user());
            if (!$reputation->hasUnlocked(ReputationService::ABILITY_TAG_POSTS)) {
                return response()->json([
                    'success' => false,
                    'message' => 'You do not have enough Trust to tag posts.',
                ]);
            }
        }
        
        $oldTags = $post->tags()->pluck('tags.id');
        $newTags = collect($tags)->pluck('id');
        
        if (count($oldTags->diff($newTags)) == 0 && count($newTags->diff($oldTags)) == 0) {
            return response()->json([
                'success' => false,
                'message' => 'Post already has those tags.',
            ]);
        }
        
        $tagging = new UserPostTagging;
        $tagging->user_id = $request->user()->id;
        $tagging->post_id = $post->id;
        
        $tagging->old_tag_ids = $oldTags->toArray();
        $tagging->new_tag_ids = $newTags->toArray();
        
        $tagging->save();
        $post->tags()->sync($newTags);
        
        if (!$post->is_private) {
            ReputationService::record($request->user(), ReputationService::ACTION_TAG_POST, [
                'post_id' => $post->id,
                'tagging_id' => $tagging->id
            ]);
        }
        
        return response()->json([
            'success' => true,
            'tags' => $post->tags,
        ]);
    }
    
    
    public function amend(AmendPostRequest $request, Post $post) {
        $genderType = $request->get('type_gender');
        $artstyleType = $request->get('type_artstyle');
        
        if ($post->is_private && $post->owner_user_id != $request->user()->id) {
            return response()->json([
                'success' => false,
                'message' => 'Cannot amend another users private post.'
            ]);
        }
        
        if (!$post->is_private) {
            $reputation = ReputationService::getProfile($request->user());
            if (!$reputation->hasUnlocked(ReputationService::ABILITY_AMEND_POSTS)) {
                return response()->json([
                    'success' => false,
                    'message' => 'You do not have enough Trust to amend posts.',
                ]);
            }
        }
        
        $amend = new UserPostAmend;
        $amend->user_id = $request->user()->id;
        $amend->post_id = $post->id;
        if ($genderType) {
            $amend->old_type_gender = $post->type_gender;
            $amend->type_gender = $genderType;
            $post->type_gender = $genderType;
        }
        if ($artstyleType) {
            $amend->old_type_artstyle = $post->type_artstyle;
            $amend->type_artstyle = $artstyleType;
            $post->type_artstyle = $artstyleType;
        }
        $amend->save();
        $post->save();
        
        if (!$post->is_private) {
            ReputationService::record($request->user(), ReputationService::ACTION_AMEND_POST, [
                'post_id' => $post->id,
                'amend_id' => $amend->id
            ]);
        }
        
        return response()->json([
            'success' => true,
            'amend' => $amend,
        ]);
    }
    
    public function viewed(Request $request, Post $post) {
        $user = $request->user();
        
        $view = new PostView;
        $view->user()->associate($user);
        $view->post()->associate($post);
        $view->save();
        
        ViewService::recordView($user->id, $post->id);
        
        \App\RecommendationResult::query()
            ->whereHas('recommendationSet', function($q) use ($user) {
                $q->where('user_id', $user->id);
            })
            ->where('post_id', $post->id)
            ->update(['viewed_at', new \Carbon\Carbon]);
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    public function viewedBulk(Request $request) {
        $user = $request->user();
        
        $postIds = $request->get('ids');
        
        $now = \Carbon\Carbon::now()->toDateTimeString();
        $views = [];
        foreach ($postIds as $id) {
            $views[] = [
                'user_id' => $user->id,
                'post_id' => $id,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        $views = PostView::insert($views);
        
        ViewService::recordViewsBulk($user->id, $postIds);
        
        \App\RecommendationResult::query()
            ->whereHas('recommendationSet', function($q) use ($user) {
                $q->where('user_id', $user->id);
            })
            ->whereIn('post_id', $postIds)
            ->update(['viewed_at' => $now]);
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    public function watchLater(Request $request, Post $post) {
        $user = $request->user();
        $include = $request->get('include');
        
        if ($post->is_private && $post->owner_user_id != $request->user()->id) {
            return response()->json([
                'success' => false,
                'message' => 'Cannot watch later a private post'
            ]);
        }
        
        $post->watchLaterEntries()
            ->where('user_id', $user->id)
            ->delete();
            
        if (!$include) {
            return response()->json([
                'success' => true,
                'entry' => null,
            ]);
        }
        
        $entry = new WatchLaterEntry;
        $entry->user()->associate($user);
        $entry->post()->associate($post);
        $entry->save();
        
        return response()->json([
            'success' => true,
            'entry' => $entry,
        ]);
    }
    
    
    public function search(Request $request) {
        $results = [
            'posts' => [],
            'subreddits' => [],
        ];
        
        $query = $request->get('query');
        if (!$query) {
            return response()->json([
                'success' => true,
                'results' => $results,
            ]);
        }
        
        // Post search
        try {
            $sphinx = new \sngrl\SphinxSearch\SphinxSearch();
            $posts = $sphinx->search($query, 'test1')
                ->setFieldWeights(['title' => 2, 'subreddit_title' => 1])
                ->limit(5)
                ->get();
            
            $results['posts'] = $posts;
        } catch (\Exception $ex) {
            \Log::error($ex);
        }
        
        // Subreddit search
        $regexTermStart = '/^\/?(?:r\/)(.*)$/m';
        $regexTerm = '/^\/?(?:r\/)?(.*)$/m';
        $subQuery = Subreddit::query()
                        ->where('slug', '!=', '_unknown')
                        ->orderBy(\DB::raw('LENGTH(title)'))
                        ->take(5);
            
        if (preg_match($regexTermStart, $query)) {
            $subredditTermStart = preg_replace($regexTermStart, '$1', trim($query));
            $subs = $subQuery->where('title', 'like', $subredditTermStart . '%');
        } else {
            $subredditTerm = preg_replace($regexTerm, '$1', trim($query));
            $subs = $subQuery->where('title', 'like', '%' . $subredditTerm . '%');
        }
        
        $subs = $subQuery->get();
        $results['subreddits'] = $subs;
        
        
        // Tags search
        $tags = Tag::query()
            ->where('key', 'like', '%' . $query .'%')
            ->get();
            
        $results['tags'] = $tags;
        
        return response()->json([
            'success' => true,
            'results' => $results,
        ]);
    }
}

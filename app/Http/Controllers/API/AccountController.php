<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignupRequest;
use App\Http\Requests\SettingsRequest;
use App\Http\Requests\ChangePasswordRequest;

use App\Services\RatingsService;
use App\Services\UserSettingsService;
use App\Services\ImportService;
use App\Services\ExportService;
use App\Services\ProgressionService;
use App\Services\ReputationService;
use App\Services\SupporterService;

use App\User;
use App\UserImport;
use App\UserExport;
use App\Subreddit;
use App\Tag;

class AccountController extends Controller {
    public function index(Request $request) {
        return response()->json([
            'success' => true,
            'user' => $request->user()
        ]);
    }
    
    public function getSettings(Request $request) {
        return response()->json([
            'success' => true,
            'settings' => UserSettingsService::getAll($request->user()),
        ]);
    }
    
    public function setSettings(SettingsRequest $request) {
        $map = $request->get('settings');
        
        UserSettingsService::setAll($request->user(), $map);
        
        return response()->json([
            'success' => true,
            'settings' => UserSettingsService::getAll($request->user()),
        ]);
    }
    
    public function update(Request $request) {
        $user = $request->user();
        $user->gender_filter = $request->get('gender_filter');
        $user->media_filter = $request->get('media_filter');
        $user->artstyle_filter = $request->get('artstyle_filter');
        $user->save();
        
        return response()->json([
            'success' => true,
            'user' => $user,
        ]);
    }
    
    public function signup(SignupRequest $request) {
        return abort(404);
        
        $user = new User;
        $user->username = $request->get('username');
        $user->password = \Hash::make($request->get('password'));
        
        $user->gender_filter = $request->input('filters.gender');
        $user->media_filter = $request->input('filters.media');
        $user->artstyle_filter = $request->input('filters.artstyle');
                
        $user->save();
        
        \Auth::login($user);
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    public function login(LoginRequest $request) {
        if (\Auth::attempt($request->only(['username', 'password']), $request->get('remember'))) {
            return response()->json([
                'success' => true,
                'user' => \Auth::getUser(),
            ]);
        }
        return response()->json([
            'success' => false,
        ]);
    }
    
    public function changePassword(ChangePasswordRequest $request) {
        $user = $request->user();
        
        $oldPassword = $request->get('old_password');
        $newPassword = $request->get('new_password');
        
        if (!\Hash::check($oldPassword, $user->password)) {
            return response()->json([
                'success' => false,
                'message' => 'Current password is incorrect.',
            ]);
        }
        
        $user->password = \Hash::make($newPassword);
        $user->save();
        
        return response()->json([
            'success' => true,
            'message' => 'Your password has been changed.',
        ]);
    }
    
    
    public function accountStats(Request $request) {
        $user = $request->user();
        
        return response()->json([
            'stats' => [
                'ratings_count' => $user->ratings()->count(),
                'views_count' => $user->views()->count(),
            ],
        ]);
    }
    
    public function progression(Request $request) {
        $user = $request->user();
        
        $profile = ProgressionService::getProfile($user);
        return response()->json([
            'success' => true,
            'progression' => $profile,
        ]);
    }
    
    public function trust(Request $request) {
        $user = $request->user();
        
        $profile = ReputationService::getProfile($user);
        return response()->json([
            'success' => true,
            'trust' => $profile,
        ]);
    }
    
    public function ratingsSummary(Request $request) {
        $user = $request->user();
        
        $subredditMap = RatingsService::getSubredditRatingMatrix($user);
        $subreddits = [];
        arsort($subredditMap);
        
        foreach ($subredditMap as $subId => $score) {
            $sub = \App\Subreddit::find($subId);
            $sub->score = $score;
            $subreddits[] = $sub;
        }
        
        $tagMap = RatingsService::getTagRatingMatrix($user);
        $tags = [];
        arsort($tagMap);
        
        
        foreach ($tagMap as $tagId => $score) {
            $tag = \App\Tag::find($tagId);
            $tag->score = $score;
            $tags[] = $tag;
        }
        
        $typeMap = RatingsService::getTypeRatingMatrix($user);
        
        return response()->json([
            'success' => true,
            'subreddits' => $subreddits,
            'types' => $typeMap,
            'tags' => $tags,
        ]);
    }
    
    
    public function clearWatchLater(Request $request) {
        $user = $request->user();
        $user->watchLaterEntries()->delete();
        return response()->json([
            'success' => true,
        ]);
    }
    
    public function getExports(Request $request) {
        return response()->json([
            'success' => true,
            'exports' => ExportService::getExports($request->user()),
        ]);
    }
    
    public function startExport(Request $request) {
        $user = $request->user();
        ExportService::startExport($user);
    }
    
    public function downloadExport(Request $request, UserExport $export) {
        $user = $request->user();
        if ($user->id != $export->user->id) {
            return abort(403);
        }
        
        $url = ExportService::downloadExport($export);
        return response()->json([
            'success' => true,
            'url' => $url,
        ]);
    }
    
    public function getImports(Request $request) {
        return response()->json([
            'success' => true,
            'imports' => ImportService::getImports($request->user()),
        ]);
    }
    
    public function deleteImports(Request $request) {
        ImportService::deleteImports($request->user());
        return response()->json([
            'success' => true,
        ]);
    }
    
    public function uploadImport(Request $request) {
        $user = $request->user();
        
        $imports = [];
        if ($request->has('file_favorites')) {
            $import = ImportService::createImport($user, UserImport::TYPE_BF_FAVORITES, $request->file('file_favorites'));
            $imports[] = $import;
        }
        if ($request->has('file_likes')) {
            $import = ImportService::createImport($user, UserImport::TYPE_BF_LIKES, $request->file('file_likes'));
            $imports[] = $import;
        }
        
        if (count($imports) == 0) {
            return response(json_encode([
                'errors' => [
                    '_' => [
                        'Please specify at least one file.',
                    ],
                ],
            ]), 422);
        }
        
        
        return response()->json([
            'success' => true,
            'imports' => ImportService::getImports($request->user()), 
        ]);
    }
    
    
    public function getContentPreferences(Request $request) {
        $blacklistedSubs = $request->user()->blacklistedSubreddits;
        $blacklistedTags = $request->user()->blacklistedTags;
        
        return response()->json([
            'success' => true,
            'blacklisted_subreddits' => $blacklistedSubs,
            'blacklisted_tags' => $blacklistedTags,
        ]);
    }
    
    public function blacklistSubreddit(Request $request, Subreddit $subreddit) {
        if (!$request->user()->blacklistedSubreddits()->where('subreddits.id', $subreddit->id)->first()) {
            $request->user()->blacklistedSubreddits()->attach($subreddit->id);
        }
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    public function blacklistTag(Request $request, Tag $tag) {
        if (!$request->user()->blacklistedTags()->where('tags.id', $tag->id)->first()) {
            $request->user()->blacklistedTags()->attach($tag->id);
        }
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    public function unblacklistSubreddit(Request $request, Subreddit $subreddit) {
        if ($request->user()->blacklistedSubreddits()->where('subreddits.id', $subreddit->id)->first()) {
            $request->user()->blacklistedSubreddits()->detach($subreddit->id);
        }
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    public function unblacklistTag(Request $request, Tag $tag) {
        if ($request->user()->blacklistedTags()->where('tags.id', $tag->id)->first()) {
            $request->user()->blacklistedTags()->detach($tag->id);
        }
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    public function connectPatreon(Request $request) {
        $authUrl = SupporterService::getPatreonAuthURL($request->user(), 
            \URL::action('API\\AccountController@handlePatreonConnected'));
        
        return response()->json([
            'success' => true,
            'auth_url' => $authUrl,
        ]);
    }
    
    public function handlePatreonConnected(Request $request) {
        $code = $request->get('code');
        $redirectUrl = \URL::action('API\\AccountController@handlePatreonConnected');
        
        try {
            SupporterService::handlePatreonAuth($request->user(), $code, $redirectUrl);
            
            
            SupporterService::updateSupporterStatus();
        } catch (\Exception $ex) {
            return \Redirect::to('/profile/supporter');
        }
        return \Redirect::to('/profile/supporter');
    }
    
    public function disconnectPatreon(Request $request) {
        $user = $request->user();
        
        $user->patreon_access_token = null;
        $user->pareton_refresh_token = null;
        $user->patreon_user_id = null;
        $user->save();
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Subreddit;
use App\Tag;
use App\TagNamespace;
use App\UserPostAmend;

class InfoController extends Controller {
    public function indexTags(Request $request) {
        $tags = Tag::all();
        
        return response()->json([
            'success' => true,
            'tags' => $tags,
        ]);
    }
    
    public function indexSubreddits(Request $request) {
        $subs = Subreddit::where('slug', '!=', '_unknown')->get();
        
        return response()->json([
            'success' => true,
            'subreddits' => $subs,
        ]);
    }
    
    public function indexTagNamespaces(Request $request) {
        $ns = TagNamespace::all();
        
        return response()->json([
            'success' => true,
            'tag_namespaces' => $ns,
        ]);
    }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateSavedListRequest;
use App\Http\Requests\UpdateSavedListRequest;

use App\SavedList;
use App\Post;

class SavedListController extends Controller {
    public function index(Request $request) {
        $user = $request->user();
        
        $lists = $user->lists()
            ->orderBy('title', 'asc')
            ->get();
            
        foreach ($lists as $list) {
            $list->load('postsSample');
        }
            
        
        return response()->json([
            'success' => true,
            'saved_lists' => $lists,
        ]);
    }
    
    public function store(CreateSavedListRequest $request) {
        $user = $request->user();
        
        $list = new SavedList;
        $list->title = $request->get('title');
        $list->description = $request->get('description');
        $list->is_public = $request->get('is_public');
        $list->user()->associate($user);
        $list->save();
        
        return response()->json([
            'success' => true,
            'saved_list' => $list,
        ]);
    }
    
    public function view(Request $request, SavedList $list) {
        $user = $request->user();
        if (!$list->is_public && $list->user_id != $user->id) {
            return abort(403);
        }
        
        $list->load('postsSample'); 
        
        return response()->json([
            'success' => true,
            'saved_list' => $list,
        ]);
    }
    
    public function update(UpdateSavedListRequest $request, SavedList $list) {
        $user = $request->user();
        if ($list->user_id != $user->id) {
            return abort(403);
        }
        
        $list->title = $request->get('title');
        $list->description = $request->get('description');
        $list->is_public = $request->get('is_public');
        $list->save();
        
        $list->load('postsSample'); 
        
        return response()->json([
            'success' => true,
            'saved_list' => $list,
        ]);
    }
    
    public function destroy(Request $request, SavedList $list) {
        $user = $request->user();
        if ($list->user_id != $user->id) {
            return abort(403);
        }
        
        $list->delete();
        
        return response()->json([
            'success' => true,
        ]);
    }
    
    
    public function indexContext(Request $request, Post $post) {
        $user = $request->user();
        
        $lists = $user->lists()
            ->orderBy('title', 'asc')
            ->with(['posts' =>  function($q) use ($post) {
                $q->where('posts.id', $post->id);
            }])
            ->get();
            
        return response()->json([
            'success' => true,
            'saved_lists' => $lists,
        ]);
    }
    
    public function addToList(Request $request, SavedList $savedList, Post $post) {
        $user = $request->user();
        if ($savedList->user_id != $user->id) {
            return abort(403);
        }
        
        $exists = $savedList->posts()->where('post_id', $post->id)->exists();
        if ($exists) {
            return response()->json([
                'success' => true,
                'message' => 'Post is already in that list.',
            ]);
        }
        
        $savedList->posts()->attach($post->id);
        
        return response()->json([
            'success' => true,
            'message' => 'Post added to list',
        ]);
    }
    
    public function removeFromList(Request $request, SavedList $savedList, Post $post) {
        $user = $request->user();
        if ($savedList->user_id != $user->id) {
            return abort(403);
        }
        
        $savedList->posts()->detach($post->id);
        
        return response()->json([
            'success' => true,
            'message' => 'Post removed from list',
        ]);
    }
}

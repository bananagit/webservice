<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ModeratorRequest;

use App\Services\ReputationService;

use App\Subreddit;
use App\Tag;
use App\TagNamespace;
use App\UserPostAmend;
use App\UserPostTagging;

class ModerationController extends Controller {
    public function indexAmends(ModeratorRequest $request) {
        $amends = UserPostAmend::query()
                ->with('user', 'post')
                ->orderBy('id', 'desc')
                ->paginate(50)
                ->items();
        
        return response()->json([
            'success' => true,
            'amends' => $amends,
        ]);
    }
    
    public function revertAmend(ModeratorRequest $request, $amendId) {
        $amend = UserPostAmend::findOrFail($amendId);
        $amend->revert();
        
        ReputationService::record($amend->user, ReputationService::ACTION_AMEND_POST_REVERTED, [
            'post_id' => $amend->post_id,
            'amend_id' => $amend->id
        ]);
        
        return response()->json([
            'success' => true,
            'amend' => $amend,
        ]);
    }
    
    public function indexTaggings(ModeratorRequest $request) {
        $taggings = UserPostTagging::query()
                ->with('user', 'post', 'post.tags')
                ->orderBy('id', 'desc')
                ->paginate(50)
                ->items();
        
        return response()->json([
            'success' => true,
            'taggings' => $taggings,
        ]);
    }
    
    public function revertTagging(ModeratorRequest $request, $taggingId) {
        $tagging = UserPostTagging::findOrFail($taggingId);
        $tagging->revert();
        
        ReputationService::record($tagging->user, ReputationService::ACTION_TAG_POST_REVERTED, [
            'post_id' => $tagging->post_id,
            'tagging_id' => $tagging->id
        ]);
        
        return response()->json([
            'success' => true,
            'tagging' => $tagging,
        ]);
    }
    
    public function indexTags(ModeratorRequest $request) {
        $tags = Tag::query()
            ->with('defaultSubreddits')
            ->with('autoTitleTags')
            ->with('tagNamespace')
            ->with('aliases')
            ->orderBy('key', 'asc', 'namespace_id', 'asc')
            ->get();
            
        $namespaces = TagNamespace::all();
        
        return response()->json([
            'success' => true,
            'tags' => $tags,
            'namespaces' => $namespaces,
        ]);
    }
    
    public function indexSubreddits(ModeratorRequest $request) {
        $subs = Subreddit::where('slug', '!=', '_unknown')->with('typeHint')->get();
        
        return response()->json([
            'success' => true,
            'subreddits' => $subs,
        ]);
    }
    
    public function updateSubreddit(ModeratorRequest $request, Subreddit $subreddit) {
        return response()->json([
            'success' => false,
            'message' => 'Temporarily disabled.',
        ]);
        
        $subData = $request->get('subreddit');
        
        $subData = json_decode(json_encode($subData));
        
        $typeHint = $subreddit->typeHint;
        
        $typeHint->fixed_gender_type = $subData->type_hint->fixed_gender_type;
        $typeHint->fixed_artstyle_type = $subData->type_hint->fixed_artstyle_type;
        $typeHint->always_solo = $subData->type_hint->always_solo;
        $typeHint->never_solo = $subData->type_hint->never_solo;
        $typeHint->always_involve_women = $subData->type_hint->always_involve_women;
        $typeHint->never_involve_women = $subData->type_hint->never_involve_women;
        $typeHint->always_involve_men = $subData->type_hint->always_involve_men;
        $typeHint->never_involve_men = $subData->type_hint->never_involve_men;
        $typeHint->always_straight = $subData->type_hint->always_straight;
        $typeHint->never_straight = $subData->type_hint->never_straight;
        $typeHint->always_gay = $subData->type_hint->always_gay;
        $typeHint->never_gay = $subData->type_hint->never_gay;
        $typeHint->always_lesbian = $subData->type_hint->always_lesbian;
        $typeHint->never_lesbian = $subData->type_hint->never_lesbian;
        $typeHint->always_transexual = $subData->type_hint->always_transexual;
        $typeHint->never_transexual = $subData->type_hint->never_transexual;
        $typeHint->allows_reallife = $subData->type_hint->allows_reallife;
        $typeHint->bans_reallife = $subData->type_hint->bans_reallife;
        $typeHint->allows_hentai = $subData->type_hint->allows_hentai;
        $typeHint->bans_hentai = $subData->type_hint->bans_hentai;
        $typeHint->allows_3d = $subData->type_hint->allows_3d;
        $typeHint->bans_3d = $subData->type_hint->bans_3d;
        $typeHint->save();
        
        $subreddit = Subreddit::with('typeHint')->find($subreddit->id);
        
        return response()->json([
            'success' => true,
            'subreddit' => $subreddit,
        ]);
    }
}

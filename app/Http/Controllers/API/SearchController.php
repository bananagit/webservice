<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Exceptions;

use App\Tag;
use App\Subreddit;


class SearchController extends Controller {
    const MAX_RESULTS_TAG = 5;
    const MAX_RESULTS_SOURCE = 5;
    
    public function index(Request $request) {
        $query = $request->get('query');
        if (!$query) {
            throw new Exceptions\SearchEmptyException();
        }
    }
    
    
    public function autocomplete(Request $request) {
        $query = $request->get('q');
        $query = str_replace('%', '', $query);
        
        $negate = false;
        if ($query[0] == '-') {
            $negate = true;
            $query = substr($query, 1);
        }
        
        
        $ns = null;
        $parts = explode(':', $query);
        if (count($parts) == 2) {
            $ns = $parts[0];
            $query = $parts[1];
        }
        
        if (!$query) {
            return response()->json([
                'query' => $query,
                'suggestions' => [],
            ]);
        }
        
        $suggestions = [];
        
        
        if ($ns != 'source' && $ns != 'title' && $ns != 'meta') {
            $tags = $this->getTagSuggestions($query);
            foreach ($tags as $tag) {
                $term = $tag->key;
                if ($tag->tagNamespace) {
                    $term = $tag->tagNamespace->shortkey . ':' . $term;
                }
                
                $suggestions[] = [
                    'type' => 'tag',
                    'term' => $negate ? '-' . $term : $term,
                    'prefix' => $negate ? 'Not tagged with ' : 'Tagged with ',
                    'label' => $term,
                    'suffix' => '...',
                    'negate' => $negate,
                    
                    'tag' => $tag,
                ];
            }
        }
        
        if ($ns == 'source' || !$ns) {
            $sources = $this->getSourceSuggestions($query);
            foreach ($sources as $source) {
                $term = 'source:' . $source->slug;
                
                $suggestions[] = [
                    'type' => 'source',
                    'term' => $negate ? '-' . $term : $term,
                    'prefix' => $negate ? 'Not from ' : 'From ',
                    'label' => $source->slug,
                    'suffix' => '...',
                    'negate' => $negate,
                    
                    'subreddit' => $source,
                ];
            }
        }
            
        if ($ns == 'title' || !$ns) {
            $termquote = (strpos($query, '"') !== FALSE) ? '' : '"';
            
            $suggestions[] = [
                'type' => 'title',
                'term' => 'title:' . $termquote . ($negate ? '-' : '') . $query . $termquote,
                'prefix' => $negate ? 'Posts without "' : 'Posts with "',
                'label' => $query . '...',
                'suffix' => '" in the title...',
                'negate' => $negate,
            ];
            $suggestions[] = [
                'type' => 'title',
                'term' => 'title:' . $termquote . ($negate ? '-' : '') . $query . '$' . $termquote,
                'prefix' => $negate ? 'Posts without "' : 'Posts with "',
                'label' => $query,
                'suffix' => '" (exactly) in the title...',
                'negate' => $negate,
            ];
        }
        
        if ($ns == 'meta' || !$ns) {
            if (strpos($query, 'audio') === 0
                || strpos($query, 'sound') === 0
                || strpos($query, 'mute') === 0) {
                $suggestions[] = [
                    'type' => 'meta',
                    'term' => ($negate ? '-' : '') . 'meta:audio',
                    'prefix' => '',
                    'label' => 'Posts with' . ($negate ? 'out' : ''). ' audio',
                    'suffix' => '',
                    'negate' => $negate,
                ];
            }
        }
        
        return response()->json([
            'query' => $query,
            'suggestions' => $suggestions,
        ]);
    }
    
    private function getSourceSuggestions($query) {
        // Exact matches
        $results = Subreddit::where('slug', $query)
                      ->take(self::MAX_RESULTS_SOURCE)
                      ->get();
        
        if (count($results) < self::MAX_RESULTS_SOURCE) {
            $prefixMatches = Subreddit::where('slug', 'like', $query . '%')
                ->take(self::MAX_RESULTS_SOURCE)
                ->get();
            $results = $results->merge($prefixMatches);
            if (count($results) < self::MAX_RESULTS_SOURCE) {
                // Infix matches
                $infixMatches = Subreddit::where('slug', 'like', '%' . $query . '%')
                    ->take(self::MAX_RESULTS_SOURCE)
                    ->get();
            }
        }
        
        $results = $results->take(self::MAX_RESULTS_TAG);
        return $results;
    }
    
    private function getTagSuggestions($query) {
        $ns = null;
        $parts = explode(':', $query);
        if (count($parts) == 2) {
            $ns = $parts[0];
            $query = $parts[1];
        }
        
        // Exact matches
        $results = Tag::where('key', $query)
                      ->namespaceString($ns)
                      ->take(self::MAX_RESULTS_TAG)
                      ->with('tagNamespace')
                      ->get();
        
        if (count($results) < self::MAX_RESULTS_TAG) {
            $prefixMatches = Tag::where('key', 'like', $query . '%')
                ->namespaceString($ns)
                ->take(self::MAX_RESULTS_TAG)
                ->with('tagNamespace')
                ->get();
            $results = $results->merge($prefixMatches);
            if (count($results) < self::MAX_RESULTS_TAG) {
                // Infix matches
                $infixMatches = Tag::where('key', 'like', '%' . $query . '%')
                    ->namespaceString($ns)
                    ->take(self::MAX_RESULTS_TAG)
                    ->with('tagNamespace')
                    ->get();
                $results = $results->merge($infixMatches);
                if (count($results) < self::MAX_RESULTS_TAG) {
                    // Description matches
                    $descriptionMatches = Tag::where('description', 'like', '%' . $query . '%')
                        ->namespaceString($ns)
                        ->take(self::MAX_RESULTS_TAG)
                        ->with('tagNamespace')
                        ->get();
                    $results = $results->merge($descriptionMatches);
                }
            }
        }
        
        $results = $results->take(self::MAX_RESULTS_TAG);
        return $results;
    }
}

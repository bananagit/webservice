<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateTagSuggestionRequest;

use App\User;
use App\Tag;
use App\TagNamespace;
use App\TagAlias;
use App\TagSuggestion;
use App\TagSuggestionVote;

class TagSuggestionController extends Controller {
    public function indexTop(Request $request) {
        $user = $request->user();
        
        $suggestions = TagSuggestion::where('status', 'pending')
            ->orderBy('score', 'desc')
            ->with([
                'user' => function($q) {
                    $q->select('id', 'username');
                },
                'votes' => function($q) use ($user){
                    $q->where('user_id', $user->id);
                }
            ])
            ->paginate(50);          
            
        return response()->json([
            'success' => true,
            'tag_suggestions' => $suggestions,
        ]);
    }
    
    public function indexMine(Request $request) {
        $user = $request->user();
        
        $suggestions = TagSuggestion::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->with([
                'user' => function($q) {
                    $q->select('id', 'username');
                },
                'votes' => function($q) use ($user){
                    $q->where('user_id', $user->id);
                }
            ])
            ->paginate(50);
            
        return response()->json([
            'success' => true,
            'tag_suggestions' => $suggestions,
        ]);
    }
    
    public function indexRecent(Request $request) {
        $user = $request->user();
        
        $status = $request->get('status');
        
        $q = TagSuggestion::orderBy('updated_at', 'desc');
        
        if ($status) {
            $q->where('status', $status);
        }
        
        $suggestions = $q
            ->with([
                'user' => function($q) {
                    $q->select('id', 'username');
                },
                'votes' => function($q) use ($user){
                    $q->where('user_id', $user->id);
                }
            ])
            ->paginate(10);
            
        return response()->json([
            'success' => true,
            'tag_suggestions' => $suggestions,
        ]);
    }
    
    public function indexSearch(Request $request) {
        $user = $request->user();
        
        $query = $request->get('query');
        
        $ns = null;
        
        $parts = explode(':', $query);
        if (count($parts) > 1) {
            $nsKey = $parts[0];
            $ns = TagNamespace::where('key', $nsKey)
                ->orWhere('shortkey', $nsKey)
                ->first();
                
            $query = $parts[1];
        }
        
        $q = TagSuggestion::query();
        
        if ($ns) {
            $q->where('new_tag_ns_id', $ns->id);
        }
        
        $q->where(function($q2) use ($query) {
            $q2->where('new_tag_key', 'like', "%$query%")
                ->orWhere('new_tag_description', 'like', "%$query%");
        });
        
        $suggestions = $q->orderBy('score', 'desc')
            ->take(30)
            ->with([
                'user' => function($q) {
                    $q->select('id', 'username');
                },
                'votes' => function($q) use ($user){
                    $q->where('user_id', $user->id);
                }
            ])
            ->get();
            
        return response()->json([
            'success' => true,
            'tag_suggestions' => $suggestions,
            'query' => $query,
        ]);
    }
    
    public function vote(Request $request, TagSuggestion $suggestion) {
        $user = $request->user();
        $type = $request->get('type');
        
        $upvotes = $suggestion->votes()
            ->where('user_id', $user->id)
            ->where('type', 'upvote')
            ->count();
        $downvotes = $suggestion->votes()
            ->where('user_id', $user->id)
            ->where('type', 'downvote')
            ->count();
            
        $delta = $upvotes - $downvotes;
        
        $suggestion->votes()
            ->where('user_id', $user->id)
            ->delete();
            
        if ($type == 'upvote') {
            $vote = new TagSuggestionVote;
            $vote->user()->associate($user);
            $vote->tagSuggestion()->associate($suggestion);
            $vote->type = 'upvote';
            $vote->save();
            
            $delta--;
        } else if ($type == 'downvote') {
            $vote = new TagSuggestionVote;
            $vote->user()->associate($user);
            $vote->tagSuggestion()->associate($suggestion);
            $vote->type = 'downvote';
            $vote->save();
            
            $delta++;
        }
        
        TagSuggestion::where('id', $suggestion->id)
            ->update([
                'score' => \DB::raw("score - ($delta)"),
            ]);
        
        
        $suggestion = TagSuggestion::where('id', $suggestion->id)
            ->with([
                'user' => function($q) {
                    $q->select('id', 'username');
                },
                'votes' => function($q) use ($user){
                    $q->where('user_id', $user->id);
                }
            ])
            ->first();
            
        return response()->json([
            'success' => true,
            'tag_suggestion' => $suggestion,
        ]);
    }
    
    
    public function store(CreateTagSuggestionRequest $request) {
        $user = $request->user();
        
        $type = $request->get('type');
        $nsId = $request->get('new_tag_ns_id');
        $key = $request->get('new_tag_key');
        $description = $request->get('new_tag_description');
        
        
        if ($type != 'new-tag') {
            return response()->json([
                'success' => false,
            ]);
        }
        
        if (strpos($key, ':') !== FALSE
            || strpos($key, ' ') !== FALSE
            || strtolower($key) != $key) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid tag format. Please format all-lowercase using underscores to separate words.',
            ]);
        }
        
        $q = Tag::where('key', $key);
        if ($nsId) {
            $q->where('namespace_id', $nsId);
        }
        $tag = $q->first();
        
        if ($tag) {
            return response()->json([
                'success' => false,
                'message' => 'There is already a tag that matches your suggestion.',
            ]);
        }
        
        $suggestion = new TagSuggestion;
        $suggestion->user()->associate($user);
        $suggestion->new_tag_ns_id = $nsId;
        $suggestion->new_tag_key = $key;
        $suggestion->new_tag_description = $description;
        $suggestion->type = $type;
        $suggestion->score = 1;
        $suggestion->save();
        
        $vote = new TagSuggestionVote;
        $vote->user()->associate($user);
        $vote->tagSuggestion()->associate($suggestion);
        $vote->type = 'upvote';
        $vote->save();
        
        return response()->json([
            'success' => true,
            'tag_suggestion' => $suggestion,
        ]);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSavedListRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'title' => 'required',
            'description' => '',
            'is_public' => 'boolean',
        ];
    }
}

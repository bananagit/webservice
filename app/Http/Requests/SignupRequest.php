<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest {
    public function authorize() {
        return true;
    }
    
    public function rules() {
        return [
            'username' => 'required|unique:users,username',
            'password' => 'required|min:6',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Services\FeedService;

class FeedSessionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'session_id' => '',
            'type' => 'required',
            'filters' => 'required|array',
            'params' => '',
            'page' => 'integer',
            'after' => '',
            'viewing' => '',
            'page' => '',
            'position' => '',
        ];
    }
    
    public function session() {
        $sessionId = $this->get('session_id');
        $feedType = $this->get('type');
        $filters = $this->get('filters');
        $params = $this->get('params');
        $page = $this->get('page');
        
        if (!$filters) {
            $filters = [];
        }
        if (!isset($filters['gender_type'])) {
            $filters['gender_type'] = [];
        }
        if (!isset($filters['media_type'])) {
            $filters['media_type'] = [];
        }
        if (!isset($filters['artstyle_type'])) {
            $filters['artstyle_type'] = [];
        }
        if (is_string($filters['gender_type'])) {
            $filters['gender_type'] = [$filters['gender_type']];
        }
        if (is_string($filters['media_type'])) {
            $filters['media_type'] = [$filters['media_type']];
        }
        if (is_string($filters['artstyle_type'])) {
            $filters['artstyle_type'] = [$filters['artstyle_type']];
        }
        
        
        if (isset($params['ratings']) && is_string($params['ratings'])) {
            $params['ratings'] = [$params['ratings']];
        }
        
        $session = null;
        if ($sessionId) {
            $session = $this->user()->sessions()->where('id', $sessionId)->first();
        }
        if (!$session) {
            $session = FeedService::findOrCreateSession(
                $this->user(),
                $feedType,
                $filters,
                $params);
        }
        if ($session) {
            $session->filters = $filters;
            if ($params) {
                $session->params = $params;
            }
        }
        
        return $session;
    }
}

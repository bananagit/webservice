<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListPostRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'feed' => 'required|in:new,watch-later,loved,liked,disliked,hated,liked-loved,disliked-hated',
            'filter_gender_type' => 'required|array',
            'filter_media_type' => 'required|array',
            'filter_artstyle_type' => 'required|array',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FlagPostRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'type' => 'required|in:broken_link,banned_content,not_porn,other',
            'message' => 'required_if:type,other',
        ];
    }
}

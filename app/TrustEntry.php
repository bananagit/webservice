<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class TrustEntry extends Model {
    protected $table = 'trust_entries';
    
    public $casts = [
        'metadata' => 'array',
    ];
    
    public function user() {
        return $this->belongsto(User::class, 'user_id');
    }
}

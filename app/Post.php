<?php

namespace App;

use App\Model;
use App\Subreddit;
use App\PostRating;
use App\WatchLaterEntry;
use App\User;
use App\UserImport;
use App\PostView;
use App\PostFlag;
use App\Recommendation;
use App\Tag;
use App\RecommendationResult;
use App\SavedList;

class Post extends Model {
    const SOURCE_REDDIT = 'reddit';
    const SOURCE_IMPORT_BF = 'import-bf';
    
    protected $table = 'posts';
    protected $dates = [
        'posted_at',
    ];
    
    public function subreddit() {
        return $this->belongsTo(Subreddit::class, 'subreddit_id');
    }
    
    public function views() {
        return $this->hasMany(PostView::class, 'post_id');
    }
    
    public function ratings() {
        return $this->hasMany(PostRating::class, 'post_id');
    }
    
    public function flags() {
        return $this->hasMany(PostFlag::class, 'post_id');
    }
    
    public function watchLaterEntries() {
        return $this->hasMany(WatchLaterEntry::class, 'post_id');
    }
    
    public function recommendations() {
        return $this->hasMany(Recommendation::class, 'post_id');
    }
    
    public function recommendationResults() {
        return $this->hasMany(RecommendationResult::class, 'post_id');
    }
    
    public function owner() {
        return $this->belongsTo(User::class, 'owner_user_id');
    }
    
    public function userImport() {
        return $this->belongsTo(UserImport::class, 'user_import_id');
    }
    
    public function tags() {
        return $this->belongsToMany(Tag::class, 'post_tags', 'post_id', 'tag_id');
    }
    
    public function savedLists() {
        return $this->belongsToMany(SavedList::class, 'saved_list_posts', 'post_id', 'list_id');
    }
    
    
    public function scopeRatedByUser($q, User $user, $ratingType, $after = null) {
        $afterRatingId = null;
        if ($after) {
            $rating = $user->ratings()->where('post_id', $rating)->first();
            if ($rating) {
                $afterRatingId = $rating->id;
            }
        }
        
        $q->whereHas('ratings', function($q2) use ($user, $ratingType) {
            $q2->where('user_id', $user->id)
                ->where('type', $ratingType);
        });
    }
    
    public function scopeAboveFlagThreshold($q) {
        $q->doesntHave('flags');
    }
    
    public function scopeInWatchLater($q, User $user) {
        $q->whereHas('watchLaterEntries', function($q2) use ($user) {
            $q2->where('user_id', $user->id);
        });
    }
    
    public function scopeNotRemoved($q) {
        $q->where('removed', false);
    }
    
    
    public function scopeVisibleToUser($q, User $user) {
        $q->where(function($q2) use ($user) {
            $q2->where('is_private', false)
                ->orWhere('owner_user_id', $user->id);
        });
    }
    
    
    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;


class UserSetting extends Model {
    protected $table = 'user_settings';
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}

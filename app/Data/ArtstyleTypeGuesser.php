<?php
namespace App\Data;

use App\Post;
use App\Data\FilterConstants;

class ArtstyleTypeGuesser {
    public function guess(Post $post, $subreddit, $postData) {
        $hints = $subreddit->typeHint;
        if (!$hints) {
            return FilterConstants::ARTSTYLE_REALLIFE;
        }
        if ($hints->fixed_artstyle_type) {
            return $hints->fixed_artstyle_type;
        }
        if ($hints->allows_reallife) {
            return FilterConstants::ARTSTYLE_REALLIFE;
        }
        if ($hints->allows_hentai) {
            return FilterConstants::ARTSTYLE_HENTAI;
        }
        if ($hints->allows_3d) {
            return FilterConstants::ARTSTYLE_3D;
        }
        return FilterConstants::ARTSTYLE_REALLIFE;
    }
}
<?php
namespace App\Data\Progression;

class ProgressionUnlock {
    public $key;
    public $name;
    
    public function __construct($key, $name) {
        $this->key = $key;
        $this->name = $name;
    }
}
<?php
namespace App\Data\Progression;

class ProgressionChannel {
    public $name;
    public $description;
    public $levels = [];
    
    public function __construct($name, $description, $levels) {
        $this->name = $name;
        $this->description = $description;
        $this->levels = $levels;
    }
}
<?php
namespace App\Data\Reputation;

class ReputationUnlock {
    public $key;
    public $name;
    
    public function __construct($key, $name) {
        $this->key = $key;
        $this->name = $name;
    }
}
<?php
namespace App\Data\Reputation;

class ReputationLevel {
    public $name;
    public $trustLevel;
    public $achieved;
    public $unlocks;
    
    private $unlockKeys = [];
    
    public function __construct($name, $trustLevel, $achieved, $unlocks) {
        $this->name = $name;
        $this->trustLevel = $trustLevel;
        $this->achieved = $achieved;
        $this->unlocks = $unlocks;
        
        $this->unlockKeys = [];
        foreach ($this->unlocks as $unlock) {
            $this->unlockKeys[] = $unlock->key;
        }
    }
    
    public function unlocks($unlockKey) {
        return in_array($unlockKey, $this->unlockKeys);
    }
}
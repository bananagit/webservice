<?php
namespace App\Data\Search;

use App\Data\Search\SearchQueryTerm;
use App\Tag;
use App\TagNamespace;
use App\Subreddit;

class SearchQuery {
    const MAX_TERMS = 10;
    const QUERY_REGEX = '/[^\s"\']+:(?:[^\s"\']+|[^\s"\']+|"(?:[^"]*)"|\'(?:[^\']*)\')|(?:[^\s"\']+|[^\s"\']+|"([^"]*)"|\'([^\']*)\')/m';
    
    public $terms = [];
    
    public function __construct($terms) {
        $this->terms = $terms;
    }
    
    public function canPerform() {
        $termsCount = count($this->terms);
        if ($termsCount < 1 || $termsCount > self::MAX_TERMS) {
            return false;
        }
        return true;
    }
    
    private function sphinxEscape($string) {
        $from = array ( '\\', '(',')','|','-','!','@','~','"','&', '/', '^', '$', '=' );
        $to = array ( '\\\\', '\(','\)','\|','\-','\!','\@','\~','\"', '\&', '\/', '\^', '\$', '\=' );
        return str_replace ( $from, $to, $string );
    }
    
    public function createSphinxQuery() {
        $includesTags = [];
        $includesSources = [];
        $includesTitles = [];
        $excludesTags = [];
        $excludesSources = [];
        $excludesTitles = [];
        $additional = [];
        
        $nsMap = [
        ];
        
        foreach (TagNamespace::all() as $ns) {
            $nsMap[$ns->key] = $ns->id;
            $nsMap[$ns->shortkey] = $ns->id;
        }
        
        foreach ($this->terms as $term) {
            if ($term->type == SearchQueryTerm::TYPE_TAG) {
                $q = Tag::where('key', $term->key);
                if ($term->ns) {
                    $q->where('namespace_id', $nsMap[$term->ns]);
                }
                $tags = $q->get();
                
                $tagGroupString = '(' . implode('|', $tags->pluck('id')->toArray()) . ')';
                if ($term->negate) {
                    $excludesTags[] = $tagGroupString;
                } else {
                    $includesTags[] = $tagGroupString;
                }
            } else if ($term->type == SearchQueryTerm::TYPE_SOURCE) {
                // TODO: source support
                $subreddit = Subreddit::where('slug', $term->key)->first();
                if ($subreddit) {
                    if ($term->negate) {
                        $excludesSources[] = $subreddit->id;
                    } else {
                        $includesSources[] = $subreddit->id;
                    }
                }
            } else if ($term->type == SearchQueryTerm::TYPE_TITLE) {
                if ($term->negate) {
                    $excludesTitles[] = $this->sphinxEscape($term->key) . '*';
                } else {
                    $includesTitles[] = $this->sphinxEscape($term->key) . '*';
                }
            } else if ($term->type == SearchQueryTerm::TYPE_META) {
                if ($term->key == 'audio' || $term->key == 'sound') {
                    if ($term->negate) {
                        $additional[] = '(@has_audio_track 0)';
                    } else {
                        $additional[] = '(@has_audio_track 1)';
                    }
                }
            }
        }
        
        $query = [];
        if (count($includesTags) > 0) {
            $query[] = '@tags (' . implode(' ', $includesTags) .')';
        }
        if (count($includesSources) > 0) {
            $query[] = '@subreddit_id (' . implode('|', $includesSources) .')';
        }
        if (count($includesTitles) > 0) {
            $query[] = '@title (' . implode('|', $includesTitles) .')';
        }
        if (count($excludesTags) > 0) {
            $query[] = '@tags -(' . implode('|', $excludesTags) .')';
        }
        if (count($excludesSources) > 0) {
            $query[] = '@subreddit_id -(' . implode('|', $excludesSources) .')';
        }
        if (count($excludesTitles) > 0) {
            $query[] = '@title -(' . implode('|', $excludesTitles) .')';
        }
        
        $query[] = implode(' ', $additional);
        
        $query =  implode(' ', $query);
        \Log::info('searching: ' . $query);
        return $query;
    }
    
    
    public static function parse($str) {
        $terms = [];
        
        preg_match_all(self::QUERY_REGEX, $str, $sections, PREG_SET_ORDER, 0);
        foreach ($sections as $section) {
            $parts = explode(':', $section[0]);
            
            $ns = null;
            $key = $parts[0];
            
            $negate = false;
            
            if (count($parts) > 1) {
                $ns = $parts[0];
                $key = str_replace(['"', '\''], '', $parts[1]);
            }
            
            if ($ns) {
                if ($ns[0] == '-') {
                    $negate = true;
                    $ns = substr($ns, 1);
                }
            } else {
                if ($key[0] == '-') {
                    $negate = true;
                    $key = substr($key, 1);
                }
            }
            
            $type = SearchQueryTerm::TYPE_TAG;
            if ($ns == 'source') {
                $type = SearchQueryTerm::TYPE_SOURCE;
            } else if ($ns == 'title') {
                $type = SearchQueryTerm::TYPE_TITLE;
            } else if ($ns == 'meta') {
                $type = SearchQueryTerm::TYPE_META;
            }
                
            $term = new SearchQueryTerm($type, $key, $ns, $negate);
            $terms[] = $term;
        }
        
        return new SearchQuery($terms);
    }
}

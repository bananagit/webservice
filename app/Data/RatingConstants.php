<?php
namespace App\Data;

class RatingConstants {
    const RATING_LOVE = 'love';
    const RATING_LIKE = 'like';
    const RATING_DISLIKE = 'dislike';
    const RATING_HATE = 'hate';
    
    const SCORE_LOVE = 10;
    const SCORE_LIKE = 1;
    const SCORE_DISLIKE = -1;
    const SCORE_HATE = -10;
}
<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Services\RatingsService;

use App\Post;
use App\Recommendation;

class TestingFeed extends BatchedFeed {
    const ERROR_NOT_ENOUGH_RATINGS = 'not_enough_ratings';
    
    public function preparePrevPage() {
        // ignore
    }
    public function prepareNextPage() {
      //  $this->generateRecommendations();
    }
    
    public function setup() {
        $now = new \Carbon\Carbon;
        $this->session->save();
    }
    
    public function refresh() {
        $this->session->page = 1;
        $this->session->page_size = 50;
        $this->session->position = 0;
        $this->session->beforePost()->associate(null);
        $this->session->afterPost()->associate(null);
        $this->session->refreshed_at = new \Carbon\Carbon;
        
        $this->session->error = null;
        $this->session->resetParams([
        ]);
        $this->setup();
        
        $this->cleanRecommendations();
        $this->generateRecommendations();
    }
    
    
    private function cleanRecommendations() {
        $this->session->recommendations()->delete();
    }
    
    private function generateRecommendations() {
        $contents = file_get_contents(storage_path() . '/data.json');
        $userRecs = json_decode($contents);
        
        $recs = $userRecs->{$this->session->user->id};
        
        $i = 0;
        foreach ($recs as $rec) {
            if ($this->session->user->views()->where('post_id', $rec[0])->exists()) {
                continue;
            }
            if ($this->session->user->ratings()->where('post_id', $rec[0])->exists()) {
                continue;
            }
            $i++;
            if ($i > 500) {
                break;
            }
            $recommendation = new Recommendation;
            $recommendation->user()->associate($this->session->user);
            $recommendation->session()->associate($this->session);
            $recommendation->post_id = $rec[0];
            $recommendation->score = $rec[1];
            $recommendation->save();
        }
    }
    
    
    private function baseQuery() {
        return $this->session->recommendations()
            ->whereHas('post', function($q2) {
                Feed::basePostsQueryTopLevel($q2, null, $this->session->getFilters(), null, $this->session->user);
            })
            ->with(['post.ratings' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.watchLaterEntries' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.recommendations' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.views' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with('post.tags');
    }
    
    public function getItems() {
        return $this->baseQuery()
            ->orderBy('score', 'desc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get()
            ->pluck('post');
    }
    
    public function getFirstItem() {
        return $this->baseQuery()
            ->orderBy('score', 'asc')
            ->first()
            ->post;
    }
    
    public function getLastItem() {
        return $this->baseQuery()
            ->orderBy('score', 'desc')
            ->first()
            ->post;
    }
}
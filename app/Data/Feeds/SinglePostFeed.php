<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Post;

class SinglePostFeed extends LiveFeed {
    protected $postId;
    
    public function setup() {
        $this->postId = $this->session->param('post_id', -1);
        $this->session->save();
    }
    
    public function refresh() {
        parent::refresh();
    }
    
    private function baseQuery() {
        $q = Feed::basePostsQuery(
            $this->session->user,
            $this->session->after_post_id, 
            $this->session->getFilters(),
            null,
            true,
            false);
            
        $q->where('id', $this->postId);
        
        return $q;
    }
    
    public function getItems() {
        return $this->baseQuery()
            ->orderBy('id', 'desc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get();
    }
    
    public function getFirstItem() {
        return $this->baseQuery()
            ->orderBy('id', 'asc')
            ->first();
    }
    
    public function getLastItem() {
        return $this->baseQuery()
            ->orderBy('id', 'desc')
            ->first();
    }
}
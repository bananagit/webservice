<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Services\RatingsService;

use App\Post;
use App\Recommendation;

class RecommendationFeed extends BatchedFeed {
    const ERROR_NOT_ENOUGH_RATINGS = 'not_enough_ratings';
    const DEFAULT_REC_LEVEL = 1;
    const SEED2_OFFSET = 84821234;
    
    protected $recommendationLevel;
    protected $seed1;
    protected $seed2;
    protected $subRatingMatrix;
    protected $typeRatingMatrix;
    protected $startViewId;
    protected $startPostId;
    protected $endPostId;
    
    public function setup() {
        $now = new \Carbon\Carbon;
        
        // User feed settings
        $this->recommendationLevel = $this->session->param('recommendation_level', self::DEFAULT_REC_LEVEL);
        
        // Random seeds
        $this->seed1 = $this->session->param('seed1', $now->timestamp);
        $this->seed2 = $this->session->param('seed2', $now->timestamp + self::SEED2_OFFSET);
        
        // Cached matrices
        $this->typeRatingMatrix = $this->session->param('type_rating_matrix', function() {
            return RatingsService::getTypeRatingMatrix($this->session->user);
        }, 'array');
        $this->subRatingMatrix = $this->session->param('sub_rating_matrix', function() {
            return RatingsService::getSubredditRatingMatrix($this->session->user);
        }, 'array');
        
        // Offsets
        $this->startViewId = $this->session->param('start_view_id', function() {
            $latestView = $this->session->user->views()->orderBy('id', 'desc')->first();
            return $latestView ? $latestView->id : null;
        });
        
        $positiveRatedSubs = [];
        foreach ($this->subRatingMatrix as $subId => $rating) {
            if ($rating > 0) {
                $positiveRatedSubs[] = $subId;
            }
        }
        $this->startPostId = $this->session->param('start_post_id', function() use ($positiveRatedSubs) {
            $latestPost = Post::where('is_private', false)
                ->whereIn('subreddit_id', $positiveRatedSubs)
                ->orderBy('id', 'desc')
                ->first();
            return $latestPost ? $latestPost->id : null;
        });
        $this->endPostId = $this->session->param('end_post_id', function() use ($positiveRatedSubs) {
            $latestPost = Post::where('is_private', false)
                ->whereIn('subreddit_id', $positiveRatedSubs)
                ->orderBy('id', 'desc')
                ->skip(5000)
                ->first();
            return $latestPost ? $latestPost->id : null;
        });
        
        $this->session->save();
    }
    
    public function preparePrevPage() {
        // ignore
    }
    public function prepareNextPage() {
        $this->generateRecommendations();
    }
    
    public function refresh() {
        $this->session->page = 1;
        $this->session->page_size = 50;
        $this->session->position = 0;
        $this->session->beforePost()->associate(null);
        $this->session->afterPost()->associate(null);
        $this->session->refreshed_at = new \Carbon\Carbon;
        
        $this->session->error = null;
        $this->session->resetParams([
            'seed1',
            'seed2',
            'start_view_id',
            'start_post_id',
            'end_post_id',
            'type_rating_matrix',
            'sub_rating_matrix',
        ]);
        $this->setup();
        
        $this->cleanRecommendations();
        $this->generateRecommendations();
    }
    
    private function getUnviewedPostsCount() {
        $cacheKey = 'RecommendationFeed:UnviewedPostCount:' . $this->session->user->id;
        return \Cache::remember($cacheKey, 5, function() {
            $q = Post::query();
            Feed::basePostsQueryTopLevel(
                $q,
                null,
                $this->session->getFilters(),
                null,
                $this->session->user,
                false);
                
            $q->whereDoesntHave('views', function($q2) {
                $q2->where('user_id', $this->session->user->id);
            });
            
            return $q->count();
        });
    }
    
    
    private function cleanRecommendations() {
        $this->session->recommendations()->delete();
    }
    
    private function buildSubredditCaseQuery() {
        $s = 'CASE';
        foreach ($this->subRatingMatrix as $subId => $score) {
            if ($score > 0) {
                $score = log($score);
            }
            
            $s .= " WHEN subreddit_id = $subId THEN $score";
        }
        $s .= ' ELSE 0 END';
        
        return '(' . $s . ')';
    }
    
    private function buildTypeCaseQueries() {
        $queries = [];
        foreach ($this->typeRatingMatrix as $type => $values) {
            $fieldName = [
                'gender_type' => 'type_gender',
                'media_type' => 'type_media',
                'artstyle_type' => 'type_artstyle',
            ][$type];
            
            $s = 'CASE';
            foreach ($values as $value => $score) {
                $s .= " WHEN $fieldName = '$value' THEN $score";
            }
            $s .= ' ELSE -0.5 END';
            
            $queries[$type] = '(' . $s . ')';
        }
        
        return $queries;
    }
    
    private function makeGaussStatement($mean, $stdDev, $seed1, $seed2) {
        return "((SQRT(-2*log(RAND($seed1 * id))) * COS(2*PI()* RAND($seed2 * id))) *$stdDev)+$mean";
    }
    
    private function generateRecommendations() {
        if (!$this->startPostId) {
            $this->session->error = self::ERROR_NOT_ENOUGH_RATINGS;
            $this->session->save();
            return [];
        }
        
        
        $subredditCase = $this->buildSubredditCaseQuery();
        $typeCases = $this->buildTypeCaseQueries();
        
        $q = Post::query();
        Feed::basePostsQueryTopLevel(
            $q,
            null,
            $this->session->getFilters(),
            null,
            $this->session->user,
            false);
            
        $wSub = 100;
        $wGender = 100;
        $wMedia = 100;
        $wArtstyle = 100;
        $stdDev = $this->recommendationLevel;
        
        $genderCase = $typeCases['gender_type'];
        $mediaCase = $typeCases['media_type'];
        $artstyleCase = $typeCases['artstyle_type'];
        
        $q->addSelect('*')
            ->whereDoesntHave('views', function($q2) {
                $q2->where('user_id', $this->session->user->id)
                    ->where('id', '<=', $this->startViewId);
            })
            ->addSelect(\DB::raw("@trueScore := LOG(($subredditCase * $wSub + $genderCase * $wGender + $mediaCase * $wMedia + $artstyleCase * $wArtstyle))  as true_score"))
            ->addSelect(\DB::raw("@trueScore + " . $this->makeGaussStatement(0, $stdDev, $this->seed1, $this->seed2) . " as score"));
            
            
        $q->where('id', '<', $this->startPostId);
        if ($this->endPostId) {
            $q->where('id', '>', $this->endPostId);
        }
            
            
        $skip = (($this->session->page - 1) * $this->session->page_size);
        
        $recommended = $q
            ->orderBy('score', 'desc')
            ->skip($skip)
            ->take($this->session->page_size)
            ->get();
            
        foreach ($recommended as $post) {
            $recommendation = new Recommendation;
            $recommendation->user()->associate($this->session->user);
            $recommendation->session()->associate($this->session);
            $recommendation->post()->associate($post);
            $recommendation->score = $post->true_score;// / ($stdDev + 1);
            $recommendation->save();
        }
        
        return $recommended;
    }
    
    
    private function baseQuery() {
        return $this->session->recommendations()
            ->whereHas('post', function($q2) {
                Feed::basePostsQueryTopLevel($q2, null, $this->session->getFilters(), null,
                    $this->session->user, true, false);
                $q2->where('is_private', false);
            })
            ->with(['post.ratings' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.watchLaterEntries' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.recommendations' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.views' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with('post.tags');
    }
    
    public function getItems() {
        return $this->baseQuery()
            ->orderBy('id', 'asc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get()
            ->pluck('post');
    }
    
    public function getFirstItem() {
        return $this->baseQuery()
            ->orderBy('id', 'desc')
            ->first()
            ->post;
    }
    
    public function getLastItem() {
        return $this->baseQuery()
            ->orderBy('id', 'asc')
            ->first()
            ->post;
    }
}
<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use sngrl\SphinxSearch\SphinxSearch;

use App\Post;

class SearchFeed extends LiveFeed {
    public function refresh() {
        parent::refresh();
    }
        
    private function sphinxEscape($string) {
        $from = array ( '\\', '(',')','|','-','!','@','~','"','&', '/', '^', '$', '=' );
        $to = array ( '\\\\', '\(','\)','\|','\-','\!','\@','\~','\"', '\&', '\/', '\^', '\$', '\=' );
        return str_replace ( $from, $to, $string );
    }

    
    public function getItems() {
        $sphinx = new SphinxSearch();
        
        $userQuery = $this->session->getParams()['query'];
        $userQuery = $this->sphinxEscape($userQuery);
        
        $cleanup = function($arr) {
            return array_map(function($v) {
                return '"' . $v . '"';
            }, $arr);
        };
        
        $filters = $this->session->getFilters();
        $genderTypes = $cleanup($filters['gender_type']);
        $mediaTypes = $cleanup($filters['media_type']);
        $artstyleTypes = $cleanup($filters['artstyle_type']);
        
        $query = '@title ' . $userQuery;
        $query .= '@type_gender (' . implode(' | ', $genderTypes) . ')'; 
        $query .= ' @type_media (' . implode(' | ', $mediaTypes) . ')';
        $query .= ' @type_artstyle (' . implode(' | ', $artstyleTypes) . ')';
       
        $postIds = collect($sphinx->search($query, 'test1')
        	->setMatchMode(\Sphinx\SphinxClient::SPH_MATCH_EXTENDED)
        	->setFieldWeights(['title' => 1, 'type_gender' => 0, 'type_media' => 0, 'type_artstyle' => 0])
        	->setSortMode(\Sphinx\SphinxClient::SPH_SORT_EXTENDED, "@id DESC")
            ->limit($this->session->page_size, ($this->session->page - 1) * $this->session->page_size)
            ->get())
            ->pluck('id');
            
        $q = Post::orderBy('id', 'desc');
        $q->aboveFlagThreshold();
        Feed::basePostsQueryRelations($q, $this->session->user);
        $q->whereIn('id', $postIds);
        
        $posts = $q->get();
        return $posts;
    }
    
    public function getFirstItem() {
        return null;
    }
    
    public function getLastItem() {
        return null;
    }
}
<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use sngrl\SphinxSearch\SphinxSearch;

use App\Post;

use App\Data\Search\SearchQuery;

class SearchAdvancedFeed extends LiveFeed {
    protected $query;
    
    public function refresh() {
        parent::refresh();
    }
        
    
    public function setup() {
        $this->query = SearchQuery::parse($this->session->param('query', ''));
    }

    
    public function getItems() {
        $sphinx = new SphinxSearch();
        
        
        if (!$this->query->canPerform()) {
            if (count($this->query->terms) < 1) {
                $this->session->error = 'empty';
                $this->session->save();
                return collect([]);
            }
            if (count($this->query->terms) > SearchQuery::MAX_TERMS) {
                $this->session->error('too-many-search-terms');
                $this->session->save();
                return collect([]);
            }
        }
        
        $this->session->error = null;
        $this->session->save();
        
        $userQuery = $this->query->createSphinxQuery();
        
        if (!$userQuery) {
            $this->session->error = 'no-matching-terms';
            $this->session->save();
            return collect([]);
        }
        
        $cleanup = function($arr) {
            return array_map(function($v) {
                return '"' . $v . '"';
            }, $arr);
        };
        
        $filters = $this->session->getFilters();
        $genderTypes = $cleanup($filters['gender_type']);
        $mediaTypes = $cleanup($filters['media_type']);
        $artstyleTypes = $cleanup($filters['artstyle_type']);
        
        $query = $userQuery;
        $query .= ' @type_gender (' . implode(' | ', $genderTypes) . ')'; 
        $query .= ' @type_media (' . implode(' | ', $mediaTypes) . ')';
        $query .= ' @type_artstyle (' . implode(' | ', $artstyleTypes) . ')';
       
        $postIds = collect($sphinx->search($query, 'test1')
        	->setMatchMode(\Sphinx\SphinxClient::SPH_MATCH_EXTENDED)
        	->setFieldWeights(['title' => 1, 'type_gender' => 0, 'type_media' => 0, 'type_artstyle' => 0])
        	->setSortMode(\Sphinx\SphinxClient::SPH_SORT_EXTENDED, "@id DESC")
            ->limit($this->session->page_size, ($this->session->page - 1) * $this->session->page_size)
            ->get())
            ->pluck('id');
            
        $q = Post::orderBy('id', 'desc');
        Feed::basePostsQueryRelations($q, $this->session->user);
        $q->whereIn('id', $postIds);
        
        $posts = $q->get();
        
        return $posts;
    }
    
    public function getFirstItem() {
        return null;
    }
    
    public function getLastItem() {
        return null;
    }
}
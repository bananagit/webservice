<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Post;

class SubredditFeed extends LiveFeed {
    public function refresh() {
        parent::refresh();
        $this->session->afterPost()->associate($this->getLastItem());
        
        $params = $this->session->getParams();
        if (!isset($params['show_viewed'])) {
            $params['show_viewed'] = true;
        }
        
        $params['start_view_id'] = null;
        $latestView = $this->session->user->views()->orderBy('id', 'desc')->first();
        if ($latestView) {
            $params['start_view_id'] = $latestView->id;
        }
        $this->session->params = $params;
        $this->session->save();
    }
    
    private function baseQuery() {
        $q = Feed::basePostsQuery(
            $this->session->user,
            $this->session->after_post_id, 
            $this->session->getFilters(),
            $this->session->getParams()['subreddit_id']);
            
        if (!isset($this->session->getParams()['show_viewed'])
            || !$this->session->getParams()['show_viewed']) {
            $q->whereDoesntHave('views', function($q2) {
                $q2->where('user_id', $this->session->user->id);
                if (isset($this->session->getParams()['start_view_id'])) {
                    $q2->where('id', '<=', $this->session->getParams()['start_view_id']);
                }
            });
        }
        
        return $q;
    }
    
    public function getItems() {
        return $this->baseQuery()
            ->orderBy('id', 'desc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get();
    }
    
    public function getFirstItem() {
        return $this->baseQuery()
            ->orderBy('id', 'asc')
            ->first();
    }
    
    public function getLastItem() {
        return $this->baseQuery()
            ->orderBy('id', 'desc')
            ->first();
    }
}
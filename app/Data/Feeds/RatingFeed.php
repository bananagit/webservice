<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Post;

class RatingFeed extends LiveFeed {
    const SORT_DEFAULT = 'default';
    const SORT_SHUFFLE = 'shuffle';
    
    
    protected $sort;
    protected $seed1;
    
    public function setup() {
        $now = new \Carbon\Carbon;
        
        $this->seed1 = intval($this->session->param('seed1', $now->timestamp));
        $this->sort = $this->session->param('sort', self::SORT_DEFAULT);
        
        $this->session->save();
    }
    
    public function refresh() {
        parent::refresh();
        
        $this->session->error = null;
        $this->session->resetParams([
            'seed1',
        ]);
        $this->setup();
        
        $afterEntry = $this->baseQuery()
            ->orderBy('id', 'desc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->first();
            
        if ($afterEntry) {
            $this->session->afterPost()->associate($afterEntry->post);
        } else {
            $this->session->error = 'empty';
        }
    }
    
    private function baseQuery() {
        $afterRating = null;
        if ($this->session->after_post_id) {
            $afterRating = $this->session->user->ratings()
                ->where('post_id', $this->session->after_post_id)
                ->first();
        }
        
        $ratingTypes = $this->session->getParams()['ratings'];
            
        $q = $this->session->user->ratings()
            ->whereIn('type', $ratingTypes)
            ->withAndWhereHas('post', function ($q2)  {
                Feed::basePostsQueryTopLevel($q2, null, $this->session->getFilters(), null, $this->session->user, true);
            })
            ->with(['post.ratings' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.watchLaterEntries' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.views' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with('post.tags')
            ->with('post.tags.tagNamespace');
            
        if ($afterRating) {
            $q->where('id', '<=', $afterRating->id);
        }
        
        return $q;
    }
    
    public function getItems() {
        $q = $this->baseQuery();
    
        if ($this->sort == self::SORT_SHUFFLE) {
            $q->orderBy(\DB::raw('RAND(' . $this->seed1 . ')', 'desc'));
        } else {
            $q->orderBy('id', 'desc');
        }
        
        return $q
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get()
            ->pluck('post');
    }
    
    public function getFirstItem() {
        $q = $this->baseQuery();
        if ($this->sort == self::SORT_SHUFFLE) {
            $q->orderBy(\DB::raw('RAND(' . $this->seed1 . ')', 'asc'));
        } else {
            $q->orderBy('id', 'asc');
        }
        
        return $q
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->first()
            ->post;
    }
    
    public function getLastItem() {
        $q = $this->baseQuery();
        if ($this->sort == self::SORT_SHUFFLE) {
            $q->orderBy(\DB::raw('RAND(' . $this->seed1 . ')', 'desc'));
        } else {
            $q->orderBy('id', 'desc');
        }
        
        return $q
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->first()
            ->post;
    }
}
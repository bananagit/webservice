<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Services\RatingsService;

use App\Post;
use App\Recommendation;

class ExperimentFeed extends BatchedFeed {
    const ERROR_NOT_ENOUGH_RATINGS = 'not_enough_ratings';
    const DEFAULT_REC_LEVEL = 50;
    const SEED2_OFFSET = 84821234;
    
    protected $recommendationLevel;
    protected $seed1;
    protected $seed2;
    protected $infrequentSubs;
    protected $startViewId;
    protected $startPostId;
    protected $endPostId;
    
    public function preparePrevPage() {
        // ignore
    }
    public function prepareNextPage() {
        $this->generateRecommendations();
    }
    
    public function setup() {
        $now = new \Carbon\Carbon;
        
        // User feed settings
        $this->recommendationLevel = $this->session->param('recommendation_level', self::DEFAULT_REC_LEVEL);
        
        // Random seeds
        $this->seed1 = $this->session->param('seed1', $now->timestamp);
        $this->seed2 = $this->session->param('seed2', $now->timestamp + self::SEED2_OFFSET);
        
        // Cached matrices
        $this->infrequentSubs = $this->session->param('subs', function() {
            return RatingsService::getInfrequentlyRatedSubs($this->session->user);
        }, 'array');
        
        // Offsets
        $this->startViewId = $this->session->param('start_view_id', function() {
            $latestView = $this->session->user->views()->orderBy('id', 'desc')->first();
            return $latestView ? $latestView->id : null;
        });
        
        $this->startPostId = $this->session->param('start_post_id', function() {
            $latestPost = Post::where('is_private', false)
                ->orderBy('id', 'desc')
                ->first();
            return $latestPost ? $latestPost->id : null;
        });
        $this->endPostId = $this->session->param('end_post_id', function() {
            $latestPost = Post::where('is_private', false)
                ->orderBy('id', 'desc')
                ->skip(5000)
                ->first();
            return $latestPost ? $latestPost->id : null;
        });
        $this->startTimestamp = $this->session->param('start_timestamp', function() {
            return new \Carbon\Carbon('-3 days');
        });
        
        $this->session->save();
    }
    
    public function refresh() {
        $this->session->page = 1;
        $this->session->page_size = 50;
        $this->session->position = 0;
        $this->session->beforePost()->associate(null);
        $this->session->afterPost()->associate(null);
        $this->session->refreshed_at = new \Carbon\Carbon;
        
        $this->session->error = null;
        $this->session->resetParams([
            'seed1',
            'seed2',
            'start_view_id',
            'start_post_id',
            'end_post_id',
            'subs',
            'start_timestamp',
        ]);
        $this->setup();
        
        $this->cleanRecommendations();
        $this->generateRecommendations();
    }
    
    
    private function cleanRecommendations() {
        $this->session->recommendations()->delete();
    }
    
    private function buildCaseQuery() {
        $s = 'CASE';
        foreach ($this->infrequentSubs as $rank => $subId) {
            $score = count($this->infrequentSubs) - $rank;
            $s .= " WHEN subreddit_id = $subId THEN $score";
        }
        $s .= ' ELSE 0 END';
        
        return '(' . $s . ')';
    }
    
    private function makeGaussStatement($mean, $stdDev, $seed1, $seed2) {
        return "((SQRT(-2*log(RAND($seed1 * id))) * COS(2*PI()* RAND($seed2 * id))) *$stdDev)+$mean";
    }
    
    private function generateRecommendations() {
        if (!$this->startPostId || !$this->startViewId) {
            $this->session->error = self::ERROR_NOT_ENOUGH_RATINGS;
            $this->session->save();
            return [];
        }
        
        $cases = $this->buildCaseQuery();
        
        $q = Post::query();
        Feed::basePostsQueryTopLevel(
            $q,
            null,
            $this->session->getFilters(),
            null,
            $this->session->user);
            
        $stdDev = $this->recommendationLevel;
        
        $timestamp = $this->startTimestamp;
        if (is_array($timestamp)) {
            $timestamp = $timestamp['date'];
        }
        
        $q->select('*')
            ->whereDoesntHave('views', function($q2) {
                $q2->where('user_id', $this->session->user->id)
                    ->where('id', '<=', $this->startViewId);
            })
            ->where('is_private', false)
            ->addSelect(\DB::raw("@trueScore := LOG($cases) as true_score"))
            ->addSelect(\DB::raw("@trueScore + " . $this->makeGaussStatement(0, $stdDev, $this->seed1, $this->seed2) . " as score"));
            
        $q->where('id', '<', $this->startPostId);
        if ($this->session->params['end_post_id']) {
            $q->where('id', '>', $this->endPostId);
        }
            
        $skip = (($this->session->page - 1) * $this->session->page_size);
            
        $recommended = $q
            ->orderBy('score', 'desc')
            ->skip($skip)
            ->take($this->session->page_size)
            ->get();
            
        foreach ($recommended as $post) {
            $recommendation = new Recommendation;
            $recommendation->user()->associate($this->session->user);
            $recommendation->session()->associate($this->session);
            $recommendation->post()->associate($post);
            $recommendation->score = $post->true_score;
            $recommendation->save();
        }
        
        return $recommended;
    }
    
    
    private function baseQuery() {
        return $this->session->recommendations()
            ->whereHas('post', function($q2) {
                Feed::basePostsQueryTopLevel($q2, null, $this->session->getFilters(), null, $this->session->user);
            })
            ->with(['post.ratings' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.watchLaterEntries' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.recommendations' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.views' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with('post.tags')
            ->with('post.tags.tagNamespace');
    }
    
    public function getItems() {
        return $this->baseQuery()
            ->orderBy('id', 'asc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get()
            ->pluck('post');
    }
    
    public function getFirstItem() {
        return $this->baseQuery()
            ->orderBy('id', 'desc')
            ->first()
            ->post;
    }
    
    public function getLastItem() {
        return $this->baseQuery()
            ->orderBy('id', 'asc')
            ->first()
            ->post;
    }
}
<?php
namespace App\Data\Feeds;

use App\Data\Feeds\Feed;
use App\Data\Feeds\LiveFeed;

use App\Post;

class ViewedFeed extends LiveFeed {
    public function refresh() {
        parent::refresh();
        $this->session->afterPost()->associate($this->getLastItem());
    }
    
    private function baseQuery() {
        $afterView = null;
        if ($this->session->after_post_id) {
            $afterView = $this->session->user->views()
                ->where('post_id', $this->session->after_post_id)
                ->first();
        }
        
        $q = $this->session->user->views()
            ->groupBy('post_id')
            ->withAndWhereHas('post', function ($q2)  {
                Feed::basePostsQueryTopLevel($q2, null, $this->session->getFilters(), null, $this->session->user);
            })
            ->with(['post.ratings' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.watchLaterEntries' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with(['post.views' => function ($q) {
                $q->where('user_id', $this->session->user_id);
            }])
            ->with('post.tags');
            
        if ($afterView) {
            $q->where('id', '<=', $afterView->id);
        }
        
        return $q;
    }
    
    public function getItems() {
        return $this->baseQuery()
            ->orderBy('id', 'desc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->get()
            ->pluck('post');
    }
    
    public function getFirstItem() {
        $entry = $this->baseQuery()
            ->orderBy('id', 'asc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->first();
        return $entry ? $entry->post : null;
    }
    
    public function getLastItem() {
        $entry = $this->baseQuery()
            ->orderBy('id', 'desc')
            ->skip(($this->session->page - 1) * $this->session->page_size)
            ->take($this->session->page_size)
            ->first();
        return $entry ? $entry->post : null;
    }
}
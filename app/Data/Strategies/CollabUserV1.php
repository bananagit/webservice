<?php
namespace App\Data\Strategies;

use App\Data\Strategies\Strategy;

use App\Services\RatingsService;
use App\Services\RecommendationsService;

use App\User;
use App\Post;
use App\PostRating;
use App\PostView;

class CollabUserV1 extends Strategy {
    protected static $name = 'CollabUserV1';
    
    public function train() {
        // Train model
        exec('cd ' . dirname(__FILE__) . '/collabuserv1 && ' . dirname(__FILE__) . '/collabuserv1/env/bin/python3 ' . dirname(__FILE__) . '/collabuserv1/train.py');
    }
    
    public function recommend(User $user) {
        exec('cd ' . dirname(__FILE__) . '/collabuserv1 && ' . dirname(__FILE__) . '/collabuserv1/env/bin/python3 ' . dirname(__FILE__) . '/collabuserv1/predict.py ' . $user->id,
            $contents,
            $returnVal);
        $recData = json_decode($contents[0]);
        
        $recs = [];
        foreach ($recData as $rec) {
            $recs[$rec[0]] = $rec[1];
        }
        
        $scores = array_values($recs);
        if (count($scores) < 1) {
            return [];
        }
        $minScore = min($scores);
        $maxScore = max($scores);
        
        $scoringInfo = RecommendationsService::getStrategyScoringInfo()[static::$name];
        
        foreach ($recs as $post => $score) {
            if ($maxScore == $minScore) {
                $normalized = $score;
            } else {
                $normalized = ($score - $minScore) / ($maxScore - $minScore);
            }
            $adjustedScore = $normalized  * $scoringInfo['weight'];
            $finalScore = $adjustedScore;
            if ($scoringInfo['noise'] > 0) {
                $gauss = $this->nrand(1, $scoringInfo['noise']);
                $finalScore *= $gauss;
            }
            $recs[$post] = $finalScore * $scoringInfo['scale'];
        }
        
        return $recs;
    }
    
    private function nrand($mean, $sd){
        $x = mt_rand()/mt_getrandmax();
        $y = mt_rand()/mt_getrandmax();
        return sqrt(-2*log($x))*cos(2*pi()*$y)*$sd + $mean;
    }
}
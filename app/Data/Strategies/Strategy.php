<?php
namespace App\Data\Strategies;

use App\User;

abstract class Strategy {
    protected static $name = 'Unnamed strat';
    
    abstract function recommend(User $user);
    
    public function getName() {
        return static::$name;
    }
}
<?php
namespace App\Data;

use App\Post;
use App\Data\FilterConstants;

class MediaTypeGuesser {
    public function guess(Post $post, $subreddit, $postData) {
        if (strpos($post->media_url, 'gfycat.com/') !== false) {
            return FilterConstants::MEDIA_ANIMATED;
        }
        if (strpos($post->media_url, '.gif') !== false) {
            return FilterConstants::MEDIA_ANIMATED;
        }
        if (strpos($post->media_url, '.mp4') !== false) {
            return FilterConstants::MEDIA_ANIMATED;
        }
        if (strpos($post->media_url, 'https://imgur.com/a/') === 0
            || strpos($post->media_url, 'http://imgur.com/a/') === 0) {
            // TODO: imgur album support
            return FilterConstants::MEDIA_TEXT;
        }
        if (strpos($post->media_url, 'https://i.imgur.com/') === 0
            || strpos($post->media_url, 'http://i.imgur.com/') === 0) {
            return FilterConstants::MEDIA_IMAGE;
        }
        if (strpos($post->media_url, 'https://imgur.com/') === 0
            || strpos($post->media_url, 'http://imgur.com/') === 0) {
            return FilterConstants::MEDIA_IMAGE;
        }
        if (strpos($post->media_url, '.png') !== false
            || strpos($post->media_url, '.jpg') !== false
            || strpos($post->media_url, '.jpeg') !== false) {
            return FilterConstants::MEDIA_IMAGE;
        }
        
        if (strpos($post->media_url, 'pornhub.com/') !== false) {
            return FilterConstants::MEDIA_VIDEO;
        }
        if (strpos($post->media_url, 'youtube.com/') !== false) {
            return FilterConstants::MEDIA_VIDEO;
        }
        if (strpos($post->media_url, 'youtu.be') !== false) {
            return FilterConstants::MEDIA_VIDEO;
        }
        if (strpos($post->media_url, 'xvideos.com') !== false) {
            return FilterConstants::MEDIA_VIDEO;
        }
        if (strpos($post->media_url, 'xhamster.com') !== false) {
            return FilterConstants::MEDIA_VIDEO;
        }
        
        $postHint = 'unknown';
        if ($postData && property_exists($postData, 'post_hint')) {
            $postHint = $postData->post_hint;
        }
        if ($postData && !$postData->media && $postHint != 'image') {
            return FilterConstants::MEDIA_TEXT;
        }
        if (!$postHint == 'image') {
            return FilterConstants::MEDIA_IMAGE;
        }
        if ($postHint == 'rich:video') {
            return FilterConstants::MEDIA_VIDEO;
        }
        
        return FilterConstants::MEDIA_TEXT;
    }
}
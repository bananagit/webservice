<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Tag;
use App\TagAlias;
use App\TagNamespace;

class TagSuggestion extends Model {
    const STATUS_PENDING = 'pending';
    const STATUS_ON_HOLD = 'on-hold';
    const STATUS_ADDED = 'added';
    const STATUS_REJECTED = 'rejected';
    
    protected $table = 'tag_suggestions';
    
    public $with = [
        'oldTag',
        'newTag',
        'newTagAlias',
        'newTagNamespace',
    ];
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function oldTag() {
        return $this->belongsTo(Tag::class, 'old_tag_id');
    }
    
    public function newTag() {
        return $this->belongsTo(Tag::class, 'new_tag_id');
    }
    
    public function newTagAlias() {
        return $this->belongsTo(TagAlias::class, 'new_tag_alias_id');
    }
    
    public function newTagNamespace() {
        return $this->belongsTo(TagNamespace::class, 'new_tag_ns_id');
    }
    
    public function votes() {
        return $this->hasMany(TagSuggestionVote::class, 'tag_suggestion_id');
    }
    
    
    
    public function add() {
        $tag = new Tag;
        $tag->key = $this->new_tag_key;
        $tag->description = $this->new_tag_description;
        $tag->namespace_id = $this->new_tag_ns_id;
        $tag->active = true;
        $tag->approved = true;
        $tag->save();
        
        $this->status = self::STATUS_ADDED;
        $this->new_tag_id = $tag->id;
        $this->save();
    }
    
    public function hold($message) {
        $this->status = self::STATUS_ON_HOLD;
        $this->rejection_reason = $message;
        $this->save();
    }
    
    public function holdSwitchToGendered() {
        $this->hold('Switching non-gendered to gendered tag.');
    }
    
    public function reject($message) {
        $this->status = self::STATUS_REJECTED;
        $this->rejection_reason = $message;
        $this->save();
    }
    
    public function rejectWrongFormat() {
        $this->reject('Incorrect tag format.');
    }
    
    public function rejectTooGeneral() {
        $this->reject('Too general - consider splitting into multiple specific tags, if possible.');
    }
    
    public function rejectDuplicate($tag) {
        $this->reject('Tag with the same meaning already exists (' . $tag . ')');
    }
    
    public function rejectTooSimilar($tag) {
        $this->reject('Too similar to a tag that already exists (' . $tag . ')');
    }
    
    public function regjectAmbig($tag) {
        $this->reject('Tag is too ambiguous or does not align with description.');
    }
    
    public function rejectSubmissionGuidelines() {
        $this->reject('Please refer to submission guidelines on the create-suggestion page.');
    }
    
    public function rejectInvalidated() {
        $this->reject('Invalidated by another suggestion which was approved.');
    }
}

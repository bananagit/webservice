<?php
namespace App\Services;

use App\Services\RatingsService;
use App\Data\CosineDistance;
use App\Data\MatrixFactorization;

use App\User;
use App\PostRating;

class UserSimilarityService {
    public static function getSimilarityMatrix() {
        // u = (u1, u2, ...., un) and v = (v1, v2, ...., vn)
        //            u1*v1 + u2*v2 + .... + un*vn
        // cos_sim =  _____________________________
        //            len(u) * len(v) 
        
        $ratingsMatrix = RatingsService::getRatingsMatrix();
        
        $indexedRatingsMatrix = [];
        foreach ($ratingsMatrix as $ratings) {
            $indexedRatingsMatrix[] = array_values($ratings);
        }
        
        $factorizer = new MatrixFactorization;
        
        dump('factorizing: ' . count($indexedRatingsMatrix) . ' x ' . count($indexedRatingsMatrix[0]));
        
        return $factorizer->process($indexedRatingsMatrix);
    }
    
}
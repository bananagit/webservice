<?php
namespace App\Services;

use App\WatchLaterEntry;
use App\User;
use App\Post;
use App\PostView;
use App\PostRating;
use App\PostFlag;
use App\Subreddit;
use App\FeedSession;
use App\Data\Feeds;

/**
 * Generates feeds.
 */
class FeedService {
    const FEED_TYPE_NEW = 'new';
    const FEED_TYPE_SUBREDDIT = 'subreddit';
    const FEED_TYPE_RATING = 'rating';
    const FEED_TYPE_WATCH_LATER = 'watch-later';
    const FEED_TYPE_RECOMMENDATION = 'recommendation';
    const FEED_TYPE_RANDOM = 'random';
    const FEED_TYPE_EXPERIMENT = 'experiment';
    const FEED_TYPE_POPULAR = 'popular';
    const FEED_TYPE_PRIVATE_POSTS = 'private-posts';
    const FEED_TYPE_VIEWED = 'viewed';
    const FEED_TYPE_SEARCH = 'search';
    const FEED_TYPE_SEARCH_ADVANCED = 'search-advanced';
    const FEED_TYPE_TAG = 'tag';
    const FEED_TYPE_SINGLE_POST = 'single-post';
    const FEED_TYPE_TESTING = 'testing';
    const FEED_TYPE_RECOMMENDATION_AGGREGATED = 'recommendation-aggregated';
    const FEED_TYPE_RECOMMENDATION_LAB = 'recommendation-lab';
    const FEED_TYPE_SAVED_LIST = 'saved-list';
    
    const DEFAULT_PAGE_SIZE = 20;
    
    const FEED_TYPE_CLASSES = [
        self::FEED_TYPE_NEW         => Feeds\NewPostFeed::class,
        self::FEED_TYPE_SUBREDDIT   => Feeds\SubredditFeed::class,
        self::FEED_TYPE_RATING      => Feeds\RatingFeed::class,
        self::FEED_TYPE_WATCH_LATER => Feeds\WatchLaterFeed::class,
        self::FEED_TYPE_RECOMMENDATION => Feeds\RecommendationFeed::class,
        self::FEED_TYPE_RANDOM      => Feeds\RandomFeed::class,
        self::FEED_TYPE_EXPERIMENT  => Feeds\ExperimentFeed::class,
        self::FEED_TYPE_POPULAR     => Feeds\PopularFeed::class,
        self::FEED_TYPE_PRIVATE_POSTS => Feeds\PrivatePostsFeed::class,
        self::FEED_TYPE_VIEWED      => Feeds\ViewedFeed::class,
        self::FEED_TYPE_SEARCH      => Feeds\SearchFeed::class,
        self::FEED_TYPE_SEARCH_ADVANCED => Feeds\SearchAdvancedFeed::class,
        self::FEED_TYPE_TAG         => Feeds\TagFeed::class,
        self::FEED_TYPE_SINGLE_POST => Feeds\SinglePostFeed::class,
        self::FEED_TYPE_TESTING     => Feeds\TestingFeed::class,
        self::FEED_TYPE_RECOMMENDATION_AGGREGATED => Feeds\AggregateRecommendationFeed::class,
        self::FEED_TYPE_RECOMMENDATION_LAB => Feeds\LabsRecommendationFeed::class,
        self::FEED_TYPE_SAVED_LIST  => Feeds\SavedListFeed::class,
    ];
    
    public static function newSession($user, $type) {
        $session = new FeedSession;
        $session->user()->associate($user);
        $session->feed_type = $type;
        $session->session_type = 'live';
        $session->page_size = self::DEFAULT_PAGE_SIZE;
        $session->page = 1;
        $session->position = 0;
        $session->save();
        return $session;
    }
    
    public static function findSession($user, $type, $filters, $params) {
        if ($type == self::FEED_TYPE_NEW) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_NEW)->first();
        }
        if ($type == self::FEED_TYPE_RECOMMENDATION) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_RECOMMENDATION)->first();
        }
        if ($type == self::FEED_TYPE_SEARCH) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_SEARCH)->first();
        }
        if ($type == self::FEED_TYPE_TESTING) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_TESTING)->first();
        }
        if ($type == self::FEED_TYPE_EXPERIMENT) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_EXPERIMENT)->first();
        }
        if ($type == self::FEED_TYPE_SAVED_LIST) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_SAVED_LIST)->first();
        }
        if ($type == self::FEED_TYPE_RANDOM) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_RANDOM)->first();
        }
        if ($type == self::FEED_TYPE_POPULAR) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_POPULAR)->first();
        }
        if ($type == self::FEED_TYPE_WATCH_LATER) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_WATCH_LATER)->first();
        }
        if ($type == self::FEED_TYPE_RECOMMENDATION_AGGREGATED) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_RECOMMENDATION_AGGREGATED)->first();
        }
        if ($type == self::FEED_TYPE_RECOMMENDATION_LAB) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_RECOMMENDATION_LAB)->first();
        }
        if ($type == self::FEED_TYPE_TAG) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_TAG)->first();
        }
        if ($type == self::FEED_TYPE_VIEWED) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_VIEWED)->first();
        }
        if ($type == self::FEED_TYPE_SINGLE_POST) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_SINGLE_POST)->first();
        }
        if ($type == self::FEED_TYPE_PRIVATE_POSTS) {
            return $user->sessions()->where('feed_type', self::FEED_TYPE_PRIVATE_POSTS)->first();
        }
        if ($type == self::FEED_TYPE_RATING) {
            $ratings = $params['ratings'];
            $sessions = $user->sessions()->where('feed_type', self::FEED_TYPE_RATING)->get();
            foreach ($sessions as $session) {
                $sessionRatings = $session->getParams()['ratings'];
                if (count($ratings) == count($sessionRatings) && !array_diff($ratings, $sessionRatings)) {
                    return $session;
                }
            }
        }
    }
    
    public static function findOrCreateSession($user, $type, $filters, $params) {
        $session = static::findSession($user, $type, $filters, $params);
        if ($session) {
            return $session;
        }
        
        $session = static::newSession($user, $type);
        if ($filters) {
            $session->filters = $filters;
        }
        if ($params) {
            $session->params = $params;
        }
        $session->save();
        return $session;
    }
    
    public static function getFeed(FeedSession $session) {
        $typeClass = self::FEED_TYPE_CLASSES[$session->feed_type];
        return new $typeClass($session);
    }
    
    
    public static function basePostsQuery($user, $after, $filters, $subreddit) {
        $q = Post::query();
        self::basePostsQueryTopLevel($q, $after, $filters, $subreddit);
        self::basePostsQueryRelations($q, $user);
        return $q;
    }
    
    public static function newWatchLaterEntriesQuery($user, $after, $filters) {
        $afterEntry = $user->watchLaterEntries()
            ->where('post_id', $after)
            ->first();
            
        $q = $user->watchLaterEntries()
            ->orderBy('id', 'desc')
            ->withAndWhereHas('post', function ($q2) use ($user, $filters) {
                self::basePostsQueryTopLevel($q2, null, $filters, null);
            })
            ->with(['post.ratings' => function ($q) use ($user) {
                $q->where('user_id', $user->id);
            }])
            ->with(['post.watchLaterEntries' => function ($q) use ($user) {
                $q->where('user_id', $user->id);
            }]);
            
        if ($afterEntry) {
            $q->where('id', '<=', $afterEntry->id);
        }
        
        return $q; 
    }
    public static function basePostsQueryRelations($q, $user) {
        $q->with(['ratings' => function ($q) use ($user) {
                $q->where('user_id', $user->id);
        }])
        ->with(['watchLaterEntries' => function ($q) use ($user) {
            $q->where('user_id', $user->id);
        }]);
    }
    
    public static function basePostsQueryTopLevel($q, $after, $filters, $subreddit) {
        $filterGenderType = $filters['gender_type'];
        $filterMediaType = $filters['media_type'];
        $filterArtstyleType = $filters['artstyle_type'];
        
        $q->notRemoved()
            ->aboveFlagThreshold()
            ->whereIn('type_gender', $filterGenderType)
            ->whereIn('type_media', $filterMediaType)
            ->whereIn('type_artstyle', $filterArtstyleType);
            
        if ($after) {
            $q->where('id', '<=', $after);
        }
        
        if ($subreddit) {
            $q->where('subreddit_id', $subreddit);
        }
    }
    
    
}
<?php
namespace App\Services;

use League\Csv\Reader;

use App\Data\FilterConstants;
use App\Data\MediaTypeGuesser;
use App\User;
use App\UserImport;
use App\Post;
use App\PostRating;
use App\Subreddit;

use App\Jobs\ProcessUserImportJob;

class ImportService {
    public static function getImports(User $user) {
        return $user->imports()->withCount('posts')->get();
    }
    
    public static function deleteImports(User $user) {
        $user->privatePosts()->delete();
        $user->imports()->delete();
    }
    
    public static function createImport(User $user, $type, $requestFile) {
        $filepath = \Storage::disk('s3')->putFile('imports/' . $user->id, $requestFile);
        
        $import = new UserImport;
        $import->user()->associate($user);
        $import->type = $type;
        $import->status = UserImport::STATUS_QUEUED;
        $import->s3_filepath = $filepath;
        $import->save();
        
        dispatch(new ProcessUserImportJob($import));
   
        return $import;
    }
    
    public static function processImport(UserImport $import) {
        if ($import->status != UserImport::STATUS_QUEUED) {
            return;
        }
        $import->status = UserImport::STATUS_RUNNING;
        $import->processing_started_at = new \Carbon\Carbon;
        $import->save();
        
        $unknownSub = Subreddit::where('slug', '_unknown')->first();
        if (!$unknownSub) {
            $unknownSub = new Subreddit;
            $unknownSub->title = 'Unknown sub';
            $unknownSub->slug = '_unknown';
            $unknownSub->save();
        }
        
        $mediaTypeGuesser = new MediaTypeGuesser;
        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', 'https://api.gfycat.com/v1/oauth/token', [
            'json' => [
                'grant_type' => 'client_credentials',
                'client_id' => 'REPLACEME',
                'client_secret' => 'REPLACEME',
            ],
        ]);
        $data = json_decode($res->getBody());
        
        $gfyToken = $data->access_token;
        
        $content = \Storage::disk('s3')->get($import->s3_filepath);
        
        $csv = Reader::createFromString($content);
        foreach ($csv->getRecords() as $row) {
            $src = $row[1];
            
            $post = new Post;
            $post->owner()->associate($import->user);
            $post->userImport()->associate($import);
            $post->is_private = true;
            $post->fullname = '_unknown';
            $post->permalink = '_unknown';
            $post->title = 'Imported post';
            $post->author = '_unknown';
            $post->author_fullname = '_unknown';
            $post->subreddit_title = '_unknown';
            $post->description = null;
            $post->subreddit_id = $unknownSub->id;
            $post->media_url = $src;
            $post->type_gender = FilterConstants::GENDER_UNKNOWN;
            $post->type_artstyle = FilterConstants::ARTSTYLE_REALLIFE;
            $post->type_media = $mediaTypeGuesser->guess($post, $unknownSub, null);
            
            if (strpos($post->media_url, 'gfycat.com/') !== false) {
                try {
                    preg_match('/^https?:\/\/(?:.*\.)?gfycat\.com\/(?:.*\/)?(.*?)(?:-.*)?(?:\..*)?$/m', $post->media_url, $matches);
                    $gfyId = $matches[1];
                    $res = $client->request('GET', 'https://api.gfycat.com/v1/gfycats/' . $gfyId, [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $gfyToken,
                        ],
                    ]);
                    $body = $res->getBody();
                    $gfyPost = json_decode($body);
                    
                    $post->media_url = $gfyPost->gfyItem->webmUrl;
                    $post->thumbnail_url = $gfyPost->gfyItem->gif100px;
                } catch (\Exception $ex) {
                    dump($ex);
                    dump($ex->getResponse()->getBody()->getContents());
                    $content = $ex->getResponse()->getBody()->getContents();
                    if (strpos($content, 'not exist') !== false) {
                        continue;
                    }
                    if (strpos($content, 'many requests') !== false) {
                        sleep(10);
                        continue;
                    }
                }
            }
            
            $post->save();
            
            if ($import->type == UserImport::TYPE_BF_LIKES) {
                $rating = new PostRating;
                $rating->post()->associate($post);
                $rating->user()->associate($import->user);
                $rating->type = 'like';
                $rating->save();
            }
            if ($import->type == UserImport::TYPE_BF_FAVORITES) {
                $rating = new PostRating;
                $rating->post()->associate($post);
                $rating->user()->associate($import->user);
                $rating->type = 'love';
                $rating->save();
            }
        }
        
        $import->status = UserImport::STATUS_COMPLETE;
        $import->processing_ended_at = new \Carbon\Carbon;
        $import->save();
    }
}
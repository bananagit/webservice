<?php
namespace App\Services;

use League\Flysystem\MountManager;

use App\Services\RatingsService;

use App\User;
use App\RecommendationSet;
use App\RecommendationResult;
use App\Post;
use App\PostRating;
use App\PostView;

class RecommendationsService {
    const SCORING_SCALE = 1000000;
    
    public static function getActiveRecommendationSets(User $user) {
//        $collabUserV1 = $user->recommendationSets()->where('strategy_name', 'CollabUserV1')->orderBy('id', 'desc')->first();
        $collabUserV2 = $user->recommendationSets()->where('strategy_name', 'CollabUserV2')->orderBy('id', 'desc')->first();
        $collabUserV3 = $user->recommendationSets()->where('strategy_name', 'CollabUserV3')->orderBy('id', 'desc')->first();
        $contentNewV1 = $user->recommendationSets()->where('strategy_name', 'ContentNewV1')->orderBy('id', 'desc')->first();
        
        $strats = [
        ];
        
        /*
        if ($collabUserV1) {
            $strats[$collabUserV1->id] = 1;
        }
        if ($collabUserV2) {
            $strats[$collabUserV2->id] = 1;
        }
        */
        
        if ($contentNewV1) {
            $strats[$contentNewV1->id] = 1;
        }
        if ($collabUserV3) {
            $strats[$collabUserV3->id] = 1;
        }
        
        return $strats;
    }
    
    public static function needsRecommendations(User $user) {
        $latestSet = $user->recommendationSets()->orderBy('id', 'desc')->first();
        return !$latestSet || $latestSet->created_at->lt(new \Carbon\Carbon('-16 minutes'));
    }
    
    public static function recommendUser(User $user) {
        $runner = new \App\Data\Strategies\StrategyRunner;
//        $runner->addStrategy(new \App\Data\Strategies\CollabUserV1);
//        $runner->addStrategy(new \App\Data\Strategies\CollabUserV2);
        $runner->addStrategy(new \App\Data\Strategies\CollabUserV3);
        $runner->addStrategy(new \App\Data\Strategies\ContentNewV1);
        
        $runner->recommend($user);
    }
    
    private static function outputRatingsData() {
        dump('outputting ratings');
        $file = fopen(dirname(__FILE__) . '/../Data/Strategies/data/user_ratings.csv', 'w');
        $ratings = PostRating::selectRaw("user_id, post_id, (CASE type WHEN 'love' THEN 10 WHEN 'like' THEN 1 WHEN 'dislike' THEN -1 WHEN 'hate' THEN -10 ELSE 0 END) as score")
            ->whereHas('post', function($q) {
                $q->where('is_private', false);
            })
            ->get();
        foreach ($ratings as $rating) {
            fputcsv($file, [
                $rating->user_id,
                $rating->post_id,
                $rating->score,
            ]);
        }
        fclose($file);
    }
    
    private static function outputPostsData() {
        dump('outputting posts');
        $file = fopen(dirname(__FILE__) . '/../Data/Strategies/data/posts.csv', 'w');
        $posts = Post::selectRaw("posts.id, title, type_gender, type_artstyle, type_media, subreddit_id, GROUP_CONCAT(tag_id separator '|') AS tags")
            ->leftJoin('post_tags', 'posts.id', 'post_tags.post_id')
            ->where('is_private', false)
            ->groupBy('posts.id')
            ->get();
        foreach ($posts as $post) {
            fputcsv($file, [
                $post->id,
                $post->title,
                $post->type_gender,
                $post->type_artstyle,
                $post->type_media,
                $post->subreddit_id,
                $post->tags
            ]);
        }
        fclose($file);
    }
    
    private static function outputUsersData() {
        dump('outputting users');
        $file = fopen(dirname(__FILE__) . '/../Data/Strategies/data/users.csv', 'w');
        $users = User::selectRaw("id, gender_filter as _gender_filter, media_filter as _media_filter, artstyle_filter as _artstyle_filter")->get();
        foreach ($users as $user) {
            fputcsv($file, [
                $user->id,
                $user->_gender_filter,
                $user->_media_filter,
                $user->_artstyle_filter,
            ]);
        }
        fclose($file);
    }
    
    private static function outputViewsData() {
        dump('outputting views');
        $file = fopen(dirname(__FILE__) . '/../Data/Strategies/data/views.csv', 'w');
        PostView::selectRaw("user_id, post_id")->chunk(250000, function($views) use ($file) {
            foreach ($views as $view) {
                fputcsv($file, [
                    $view->user_id,
                    $view->post_id,
                ]);
            }
        });
        fclose($file);
    }
    
    private static function outputUserProfilesData() {
        dump('outputting profiles');
        $file = fopen(dirname(__FILE__) . '/../Data/Strategies/data/user_ratings_profile.csv', 'w');
        $users = User::all();
        foreach($users as $user) {
            $typeRatingMatrix = RatingsService::getTypeRatingMatrix($user);
            $subRatingMatrix = RatingsService::getSubredditRatingMatrix($user);
            $tagRatingMatrix = RatingsService::getTagRatingMatrix($user);
            fputcsv($file, [
                $user->id,
                json_encode($typeRatingMatrix),
                json_encode($subRatingMatrix),
                json_encode($tagRatingMatrix),
            ]);
        }
        fclose($file);
    }
    
    public static function outputRecommendationsData() {
        static::outputRatingsData();
        static::outputPostsData();
        static::outputUsersData();
        static::outputViewsData();
        static::outputUserProfilesData();
    }
    
    public static function outputRecommendationsDataV2() {
        static::outputRatingsDataV2();
        static::outputPostsDataV2();
        static::outputUsersDataV2();
        static::outputViewsDataV2();
        
        static::outputUserProfilesData();
    }
    
    private static function outputRatingsDataV2() {
        dump('outputting ratings v2');
        // Export to S3
        \DB::statement("
            SELECT 
                user_id, 
                post_id, 
                (CASE type WHEN 'love' THEN 10 WHEN 'like' THEN 1 WHEN 'dislike' THEN -1 WHEN 'hate' THEN -10 ELSE 0 END) as score,
                UNIX_TIMESTAMP(post_ratings.created_at) as created_at
            FROM post_ratings
            JOIN posts ON post_ratings.post_id = posts.id
            WHERE posts.is_private = 0
            LIMIT 999999999
            INTO OUTFILE S3 's3-us-east-1://REPLACEME/ratings'
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            OVERWRITE ON
        ");
        
        // Copy S3 to local
        $mountManager = new MountManager([
            's3' => \Storage::disk('s3_2')->getDriver(),
            'recdata' => \Storage::disk('recdata')->getDriver(),
        ]);
        \Storage::disk('recdata')->delete('user_ratings.csv');
        $mountManager->copy('s3://ratings.part_00000', 'recdata://user_ratings.csv');
    }
    
    private static function outputPostsDataV2() {
        dump('outputting posts v2');
        // Export to S3
        \DB::statement("
            SELECT 
                posts.id, 
                title, 
                type_gender, 
                type_artstyle, 
                type_media, 
                subreddit_id, 
                GROUP_CONCAT(tag_id separator '|') AS tags,
                UNIX_TIMESTAMP(posts.created_at) as created_at
            FROM posts
            LEFT JOIN post_tags ON posts.id = post_tags.post_id
            WHERE posts.is_private = 0
            GROUP BY posts.id
            LIMIT 999999999
            INTO OUTFILE S3 's3-us-east-1://REPLACEME/posts'
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            OVERWRITE ON
        ");
        
        // Copy S3 to local
        $mountManager = new MountManager([
            's3' => \Storage::disk('s3_2')->getDriver(),
            'recdata' => \Storage::disk('recdata')->getDriver(),
        ]);
        \Storage::disk('recdata')->delete('posts.csv');
        $mountManager->copy('s3://posts.part_00000', 'recdata://posts.csv');
    }
    
    private static function outputUsersDataV2() {
        dump('outputting users v2');
        // Export to S3
        \DB::statement("
            SELECT 
                id, 
                gender_filter as _gender_filter, 
                media_filter as _media_filter, 
                artstyle_filter as _artstyle_filter,
                UNIX_TIMESTAMP(users.created_at) as created_at
            FROM users
            LIMIT 999999999
            INTO OUTFILE S3 's3-us-east-1://REPLACEME/users'
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            OVERWRITE ON
        ");
        
        // Copy S3 to local
        $mountManager = new MountManager([
            's3' => \Storage::disk('s3_2')->getDriver(),
            'recdata' => \Storage::disk('recdata')->getDriver(),
        ]);
        \Storage::disk('recdata')->delete('users.csv');
        $mountManager->copy('s3://users.part_00000', 'recdata://users.csv');
    }
    
    private static function outputViewsDataV2() {
        dump('outputting views v2');
        // Export to S3
        \DB::statement("
            SELECT 
                user_id,
                post_id,
                UNIX_TIMESTAMP(created_at) as created_at
            FROM post_views
            LIMIT 999999999
            INTO OUTFILE S3 's3-us-east-1://REPLACEME/views'
            FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            LINES TERMINATED BY '\n'
            OVERWRITE ON
        ");
        
        // Copy S3 to local
        $mountManager = new MountManager([
            's3' => \Storage::disk('s3_2')->getDriver(),
            'recdata' => \Storage::disk('recdata')->getDriver(),
        ]);
        \Storage::disk('recdata')->delete('views.csv');
        $mountManager->copy('s3://views.part_00000', 'recdata://views.csv');
    }
    
    public static function getStrategyScoringInfo() {
        return [
            'CollabUserV1' => [
                'scale' => self::SCORING_SCALE,
                'weight' => 1,
                'noise' => 0,
            ],
            'CollabUserV2' => [
                'scale' => self::SCORING_SCALE,
                'weight' => 1,
                'noise' => 0,
            ],
            'CollabUserV3' => [
                'scale' => self::SCORING_SCALE,
                'weight' => 1,
                'noise' => 0,
            ],
            'ContentNewV1' => [
                'scale' => self::SCORING_SCALE,
                'weight' => 0.3,
                'noise' => 0.1,
            ],
        ];
    }
}
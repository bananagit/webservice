<?php
namespace App\Services;

use Patreon\API;
use Patreon\OAuth;
use GuzzleHttp\Client;
use App\User;
use App\GlobalConfig;

class SupporterService {
    public static function getPatreonMembers() {
        $campaignId = env('PATREON_CAMPAIGN_ID');
        $url = "campaigns/{$campaignId}/members?include=user&page[count]=100&fields[member]=full_name,is_follower,email,last_charge_date,last_charge_status,lifetime_support_cents,patron_status,currently_entitled_amount_cents,pledge_relationship_start,will_pay_amount_cents,patron_status&fields[user]=full_name,hide_pledges";
        $cursor = null;
        
        $oauth = new OAuth(env('PATREON_CLIENT_ID'), env('PATREON_CLIENT_SECRET'));
        
        $refreshToken = GlobalConfig::getValue('patreon_creator_refresh_token');
        $res = $oauth->refresh_token($refreshToken, 'http://example.com');
        
        $accessToken = $res['access_token'];
        $refreshToken = $res['refresh_token'];
        
        GlobalConfig::setValue('patreon_creator_refresh_token', $refreshToken);

        $client = new Client([
            'base_uri' => 'https://www.patreon.com/api/oauth2/v2/',
            'timeout'  => 30,
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);
        
        $allMembers = [];
        
        while (true) {
            $res = $client->request('GET', $url);
            $res = json_decode($res->getBody());
            
            foreach ($res->data as $member) {
                if ($member->type != 'member') {
                    continue;
                }
                $allMembers[] = $member;
            }
            
            if (property_exists($res, 'links')) {
                $url = $res->links->next;
            } else {
                break;
            }
        }
        return $allMembers;
    }
    
    public static function updateSupporterStatus() {
        $members = static::getPatreonMembers();
        
        foreach ($members as $member) {
            $user = User::where('patreon_user_id', $member->relationships->user->data->id)->first();
            if (!$user) {
                continue;
            }
            
            if ($member->patron_status == 'active_patron') {
                $user->is_supporter = true;
                $user->past_supporter = true;
                $user->support_amount = $member->currently_entitled_amount_cents;
            } else {
                $user->is_supporter = false;
                $user->support_amount = null;
            }
            $user->save();
        }
    }
    
    public static function getPatreonAuthURL(User $user, $redirectUrl) {
        $authUrl = 'https://www.patreon.com/oauth2/become-patron?response_type=code&min_cents=100';
        $authUrl .= '&redirect_uri=' . $redirectUrl;
        $authUrl .= '&client_id=' . env('PATREON_CLIENT_ID');
        return $authUrl;
    }
    
    public static function handlePatreonAuth(User $user, $code, $redirectUrl) {
        $client = new Client([
            'base_uri' => 'https://www.patreon.com/api/oauth2/',
            'timeout'  => 30,
            'headers' => [
            ],
        ]);
        
        $res = $client->request('POST', 'token', [
            'form_params' => [
                'code' => $code,
                'grant_type' => 'authorization_code',
                'client_id' => env('PATREON_CLIENT_ID'),
                'client_secret' => env('PATREON_CLIENT_SECRET'),
                'redirect_uri' => $redirectUrl,
            ],
        ]);
        $res = json_decode($res->getBody());
        
        $accessToken = $res->access_token;
        $refreshToken = $res->refresh_token;
        
        $user->patreon_access_token = $accessToken;
        $user->pareton_refresh_token = $refreshToken;
        
        $client = new Client([
            'base_uri' => 'https://www.patreon.com/api/oauth2/v2/',
            'timeout'  => 30,
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);
        
        $res = $client->request('GET', 'identity');
        $res = json_decode($res->getBody());
        
        $user->patreon_user_id = $res->data->id;
        $user->save();
        
        return true;
    }
}

<?php
namespace App\Services;

use App\Data\Reputation\ReputationProfile;
use App\Data\Reputation\ReputationLevel;
use App\Data\Reputation\ReputationUnlock;
use App\User;
use App\TrustEntry;

class ReputationService {
    const ACTION_AMEND_POST = 'amend-post';
    const ACTION_AMEND_POST_REVERTED = 'amend-post-reverted';
    const ACTION_TAG_POST = 'tag-post';
    const ACTION_TAG_POST_REVERTED = 'tag-post-reverted';
    const ACTION_PENALIZED = 'moderator-penalized';
    
    const ABILITY_AMEND_POSTS = 'amend-posts';
    const ABILITY_TAG_POSTS = 'tag-posts';
    
    const ACTIONS = [
        self::ACTION_AMEND_POST => [
            'change' => +3,
            'title' => 'Amended post',
            'description' => 'Amended a post\'s types.',
        ],
        self::ACTION_AMEND_POST_REVERTED => [
            'change' => -15,
            'title' => 'Amend reverted',
            'description' => 'An admin reverted an amendment you made to a post.',
        ],
        self::ACTION_TAG_POST => [
            'change' => +1,
            'title' => 'Tagged a post',
            'description' => 'Added or removed tags from a post.',
        ],
        self::ACTION_TAG_POST_REVERTED => [
            'change' => -5,
            'title' => 'Tag post reverted',
            'description' => 'An admin reverted a tag you added to a post.',
        ],
        self::ACTION_PENALIZED => [
            'change' => -100,
            'title' => 'Penalized by admin',
            'description' => 'An admin penalized your account for bad behavior.',
        ],
    ];
    
    private static function getReputationConfig() {
        return [
            [
                'name' => '???',
                'trust_level' => -9999999,
                'unlocks' => [
                ],
            ],
            [
                'name' => 'Blacklisted',
                'trust_level' => -200,
                'unlocks' => [
                ],
            ],
            [
                'name' => 'It\'s Complicated',
                'trust_level' => -100,
                'unlocks' => [
                    self::ABILITY_AMEND_POSTS => 'Amend posts types.',
                ],
            ],
            [  
                'name' => 'Stranger',
                'trust_level' => 0,
                'unlocks' => [
                ],
            ],
            [  
                'name' => 'Acquaintance',
                'trust_level' => 50,
                'unlocks' => [
                    self::ABILITY_TAG_POSTS => 'Add and remove post tags',
                ],
            ],
            [  
                'name' => 'Friend',
                'trust_level' => 300,
                'unlocks' => [
                ],
            ],
            [
                'name' => 'Trusted Friend',
                'trust_level' => 1000,
                'unlocks' => [
                ],
            ],
            [
                'name' => '???',
                'trust_level' => 9999999,
                'unlocks' => [
                ],
            ],
        ];
    }
    
    public static function getProfile(User $user) {
        $levels = [];
        foreach (static::getReputationConfig() as $levelData) {
            $unlocks = [];
            foreach ($levelData['unlocks'] as $unlockKey => $unlockName) {
                $unlocks[] = new ReputationUnlock(
                    $unlockKey,
                    $unlockName
                );
            }
            $levels[] = new ReputationLevel(
                $levelData['name'],
                $levelData['trust_level'],
                $levelData['trust_level'] <= $user->trust,
                $unlocks
            );
        }
        $recentActions = $user->trustEntries()->orderBy('id', 'desc')->take(50)->get();
        $profile = new ReputationProfile($user->trust, $levels, $recentActions, self::ACTIONS);
        return $profile;
    }
    
    public static function record(User $user, $actionKey, $metadata = [], $overrideScore = null) {
        $action = self::ACTIONS[$actionKey];
        
        
        $score = $overrideScore;
        if ($overrideScore === null) {
            $score = $action['change'];
        }
        $newTrust = $user->trust + $score;
        
        $entry = new TrustEntry;
        $entry->user_id = $user->id;
        $entry->old_trust = $user->trust;
        $entry->new_trust = $newTrust;
        $entry->action = $actionKey;
        $entry->metadata = $metadata;
        $entry->trust_change = $score;
        $entry->save();
        
        $user->trust = $newTrust;
        $user->save();
        
        return $entry;
    }
}
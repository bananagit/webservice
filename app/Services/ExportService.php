<?php
namespace App\Services;

use League\Csv\Reader;

use App\Data\FilterConstants;
use App\Data\MediaTypeGuesser;
use App\User;
use App\UserImport;
use App\UserExport;
use App\Post;
use App\PostRating;
use App\Subreddit;

use App\Jobs\ProcessUserExportJob;

class ExportService {
    public static function getExports(User $user) {
        return $user->exports;
    }
    
    public static function startExport(User $user) {
        $export = new UserExport;
        $export->user()->associate($user);
        $export->save();
        
        dispatch(new ProcessUserExportJob($export));
   
        return $export;
    }
    
    public static function processExport(UserExport $export) {
        if ($export->finished) {
            return;
        }
        
        \Log::info('[ExportService]: Processing');
        
        $timestamp = (new \Carbon\Carbon)->toIso8601String();
        $filepath = 'exports/' . md5($export->user_id) . '-' . $timestamp . '.aifapexport.zip';
        
        $tempPath = storage_path() . '/' . md5($filepath) . '.tmp.zip';
        
        try {
            $zip = new \Chumper\Zipper\Zipper;
            
            $zip->make($tempPath);
            
            \Log::info('[ExportService]: Exporting user info');
            $userInfo = static::buildUserInfo($export->user);
            \Log::info('[ExportService]: Exporting rating info');
            $ratings = static::buildRatingsInfo($export->user);
            \Log::info('[ExportService]: Exporting views info');
            $views = static::buildViewsInfo($export->user);
            \Log::info('[ExportService]: Exporting settings info');
            $settings = static::buildSettingsInfo($export->user);
            \Log::info('[ExportService]: Exporting posts info');
            $privatePosts = static::buildPrivatePostsInfo($export->user);
            \Log::info('[ExportService]: Exporting trust info');
            $trustHistory = static::buildTrustHistoryInfo($export->user);
            \Log::info('[ExportService]: Exporting lists info');
            $lists = static::buildListsInfo($export->user);
            \Log::info('[ExportService]: Exporting blacklist info');
            $blacklist = static::buildBlacklistInfo($export->user);
            
            \Log::info('[ExportService]: Building zip');
            $zip->addString('user.json', json_encode($userInfo, JSON_PRETTY_PRINT));
            $zip->addString('ratings.json', json_encode($ratings, JSON_PRETTY_PRINT));
            $zip->addString('views.json', json_encode($views, JSON_PRETTY_PRINT));
            $zip->addString('settings.json', json_encode($settings, JSON_PRETTY_PRINT));
            $zip->addString('private_posts.json', json_encode($privatePosts, JSON_PRETTY_PRINT));
            $zip->addString('trust_history.json', json_encode($trustHistory, JSON_PRETTY_PRINT));
            $zip->addString('lists.json', json_encode($lists, JSON_PRETTY_PRINT));
            $zip->addString('blacklist.json', json_encode($blacklist, JSON_PRETTY_PRINT));
            
            $zip->addString('readme.md', file_get_contents(storage_path().'/app/export_readme.md'));
            $zip->addString('readme.pdf', file_get_contents(storage_path().'/app/export_readme.pdf'));
            
            // TODO
            
            \Log::info('[ExportService]: Writing');
            $zip->close();
            
            \Storage::disk('s3')->put($filepath, file_get_contents($tempPath));
            
            $export->s3_path = $filepath;
            
            $export->finished = true;
            $export->save();
        } catch (\Exception $ex) {
            \File::delete($tempPath);
            throw $ex;
        } finally {
            \File::delete($tempPath);
        }
    }
    
    private static function buildUserInfo($user) {
        return [
            'user' => array_intersect_key($user->toArray(), array_flip([
                'id',
                'username',
                'gender_filter',
                'media_filter',
                'artstyle_filter',
                'moderator',
                'trust',
                'is_supporter',
                'past_supporter',
            ]))
        ];
    }
    private static function buildViewsInfo($user) {
        return [
            'views' => $user->views()->select([
                'post_views.id',
                'created_at',
                'updated_at',
                'user_id',
                'post_id',
            ])->with([
                'post' => function($q) {
                    static::getPostInfo($q);
                },
            ])->get()->toArray(),
        ];
    }
    private static function buildRatingsInfo($user) {
        return [
            'ratings' => $user->ratings()->select([
                'post_ratings.id',
                'created_at',
                'updated_at',
                'user_id',
                'post_id',
                'type',
            ])->with([
                'post' => function($q) {
                    static::getPostInfo($q);
                },
            ])->get()->toArray(),
        ];
    }
    private static function buildSettingsInfo($user) {
        return [
            'settings' => $user->settings()->select([
                'updated_at',
                'user_id',
                'key',
                'value',
            ])->get()->toArray(),
        ];
    }
    private static function buildPrivatePostsInfo($user) {
        return [
            'private_posts' => static::getPostInfo($user->privatePosts()),
        ];
    }
    private static function buildTrustHistoryInfo($user) {
        return [
            'trust_history' => $user->trustEntries()->select([
                'trust_entries.id',
                'created_at',
                'updated_at',
                'user_id',
                'old_trust',
                'new_trust',
                'trust_change',
                'action',
                'metadata',
            ])->get()->toArray(),
        ];
    }
    private static function buildListsInfo($user) {
        return [
            'lists' => $user->lists()->select([
                'id',
                'created_at',
                'updated_at',
                'user_id',
                'title',
                'description',
                'is_public',
            ])->get()->map(function($list) {
                $posts = static::getPostInfo($list->posts());
                $list = $list->getOriginal();
                $list['posts'] = $posts;
                return $list;
            })->all(),
        ];
    }
    private static function getPostInfo($query) {
        return $query->select([
            'posts.id',
            'fullname',
            'permalink',
            'title',
            'author',
            'author_fullname',
            'posted_at',
            'subreddit_title',
            'subreddit_id',
            'media_url',
            'media_domain',
            'thumbnail_url',
            'thumbnail_width',
            'thumbnail_height',
            'width',
            'height',
            'description',
            'type_gender',
            'type_media',
            'type_artstyle',
            'removed',
            'source',
            'user_import_id',
            'is_private',
            'owner_user_id',
            'popular_feed_score',
            'has_audio_track',
        ])->withCount('flags')->get()->map(function($post) {
            if ($post->flags_count > 0 || $post->removed) {
                return [
                    'id' => $post->id,
                    'fullname' => $post->fullname,
                    'removed' => true
                ];
            }
            return $post->getOriginal();
        });
    }
    private static function buildBlacklistInfo($user) {
        return [
            'blacklist' => [
                'tags' => $user->blacklistedTags()->select([
                    'tags.id',
                    'key',
                    'namespace_id',
                ])->get()->map(function($tag) {
                    return $tag->getOriginal();
                }),
                'subreddits' => $user->blacklistedSubreddits()->select([
                    'subreddits.id',
                    'title',
                    'slug',
                    'fullname',
                ])->get()->map(function($sub) {
                    return $sub->getOriginal();
                }),
            ],
        ];
    }
    
    
    public static function downloadExport(UserExport $export) {
        $s3 = \Storage::disk('s3');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $expiry = "+30 minutes";
        
        $command = $client->getCommand('GetObject', [
            'Bucket' => \Config::get('filesystems.disks.s3.bucket'),
            'Key'    => $export->s3_path,
        ]);
        
        $request = $client->createPresignedRequest($command, $expiry);
        return (string)$request->getUri();
    }
}
<?php

Route::get('/logout', function() {
    \Auth::logout();
    return redirect()->to('/');
});

Route::get('/', function ($any = null) {
    return view('prerender');
})->where('any', '.*');

Route::get('{any?}', function ($any = null) {
    return view('welcome');
})->where('any', '.*');


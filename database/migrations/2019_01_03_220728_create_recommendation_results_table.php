<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecommendationResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendation_results', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            $table->integer('set_id')->unsigned();
            $table->foreign('set_id')
                ->references('id')
                ->on('recommendation_sets')
                ->onDelete('cascade');
            
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');
                
            $table->integer('score');
            $table->index('score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendation_results');
    }
}

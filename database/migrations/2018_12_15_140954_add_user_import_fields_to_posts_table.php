<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserImportFieldsToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('posts', function($table) {
            $table->string('source')->default('reddit');
            $table->index('source');
            
            $table->integer('user_import_id')->unsigned()->nullable();
            $table->foreign('user_import_id')
                ->references('id')
                ->on('user_imports')
                ->onDelete('cascade');
            
            $table->boolean('is_private')->default(false);
            $table->integer('owner_user_id')->unsigned()->nullable();
            $table->foreign('owner_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        \Schema::table('posts', function($table) {
            $table->dropIndex([
                'source',
                'user_import_id',
                'owner_user_id',
            ]);
            $table->dropColumn([
                'source',
                'user_import_id',
                'is_private',
                'owner_user_id',
            ]);
        });
    }
}

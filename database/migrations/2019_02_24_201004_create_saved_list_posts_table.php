<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedListPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saved_list_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            $table->integer('list_id')->unsigned();
            $table->foreign('list_id')
                ->references('id')
                ->on('saved_lists')
                ->onDelete('cascade');
                
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saved_list_posts');
    }
}

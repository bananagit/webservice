<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Data\FilterConstants;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            // Metadata
            $table->string('fullname');
            $table->string('permalink');
            $table->string('title');
            $table->string('author');
            $table->string('author_fullname');
            $table->timestamp('posted_at')->nullable();
            
            $table->string('subreddit_title');
            $table->integer('subreddit_id')->unsigned();
            $table->foreign('subreddit_id')
                ->references('id')
                ->on('subreddits');
                
            // Media
            $table->string('media_url')->nullable();
            $table->string('media_domain')->nullable();
            $table->string('thumbnail_url')->nullable();
            
            // Image media
            $table->integer('thumbnail_width')->nullable();;
            $table->integer('thumbnail_height')->nullable();;
            $table->integer('width')->nullable();;
            $table->integer('height')->nullable();;
            
            // Text media
            $table->text('description')->nullable();;
            
            
            $table->enum('type_gender', [
                FilterConstants::GENDER_SOLO_WOMEN,
                FilterConstants::GENDER_SOLO_MEN,
                FilterConstants::GENDER_STRAIGHT,
                FilterConstants::GENDER_GAY,
                FilterConstants::GENDER_LESBIAN,
                FilterConstants::GENDER_TRANSEXUAL,
                FilterConstants::GENDER_UNKNOWN,
            ]);
            
            $table->enum('type_media', [
                FilterConstants::MEDIA_IMAGE,
                FilterConstants::MEDIA_ANIMATED,
                FilterConstants::MEDIA_VIDEO,
                FilterConstants::MEDIA_AUDIO,
                FilterConstants::MEDIA_TEXT,
            ]);
            
            $table->enum('type_artstyle', [
                FilterConstants::ARTSTYLE_REALLIFE,
                FilterConstants::ARTSTYLE_HENTAI,
                FilterConstants::ARTSTYLE_3D,
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

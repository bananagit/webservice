<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMetadataToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('posts', function($table) {
            $table->timestamp('metadata_updated_at')->nullable();
            $table->index('metadata_updated_at');
            
            $table->boolean('has_audio_track')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('posts', function($table) {
            $table->dropIndex('metadata_updated_at');
            $table->dropColumn([
                'metadata_updated_at',
                'has_audio_track',
            ]);
        });
    }
}

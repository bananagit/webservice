<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagSuggestionVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_suggestion_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            $table->integer('tag_suggestion_id')->unsigned();
            $table->foreign('tag_suggestion_id')
                ->references('id')
                ->on('tag_suggestions')
                ->onDelete('cascade');
                
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
                
            $table->enum('type', [
                'upvote',
                'downvote',
            ]);
            
            $table->index(['tag_suggestion_id', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_suggestion_votes');
    }
}

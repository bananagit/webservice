<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Data\FilterConstants;

class CreateTypeHintsTable extends Migration {
    public function up() {
        Schema::create('type_hints', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            $table->enum('fixed_gender_type', [
                FilterConstants::GENDER_SOLO_WOMEN,
                FilterConstants::GENDER_SOLO_MEN,
                FilterConstants::GENDER_STRAIGHT,
                FilterConstants::GENDER_GAY,
                FilterConstants::GENDER_LESBIAN,
                FilterConstants::GENDER_TRANSEXUAL,
                FilterConstants::GENDER_UNKNOWN,
            ])->nullable();
            
            $table->boolean('always_solo')->default(false);
            $table->boolean('never_solo')->default(false);
            
            $table->boolean('always_involve_women')->default(false);
            $table->boolean('never_involve_women')->default(false);
            $table->boolean('always_involve_men')->default(false);
            $table->boolean('never_involve_men')->default(false);
            
            $table->boolean('always_straight')->default(false);
            $table->boolean('never_straight')->default(false);
            
            $table->boolean('always_gay')->default(false);
            $table->boolean('never_gay')->default(false);
            
            $table->boolean('always_lesbian')->default(false);
            $table->boolean('never_lesbian')->default(false);
            
            $table->boolean('always_transexual')->default(false);
            $table->boolean('never_transexual')->default(false);
            
            
            $table->enum('fixed_artstyle_type', [
                FilterConstants::ARTSTYLE_REALLIFE,
                FilterConstants::ARTSTYLE_HENTAI,
                FilterConstants::ARTSTYLE_3D,
            ])->nullable();
            
            $table->boolean('allows_reallife')->default(false);
            $table->boolean('bans_reallife')->default(false);
            
            $table->boolean('allows_hentai')->default(false);
            $table->boolean('bans_hentai')->default(false);
            
            $table->boolean('allows_3d')->default(false);
            $table->boolean('bans_3d')->default(false);
        });
    }

    public function down() {
        Schema::dropIfExists('type_hints');
    }
}

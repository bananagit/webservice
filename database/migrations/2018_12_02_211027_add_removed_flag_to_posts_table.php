<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemovedFlagToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('posts', function($table) {
            $table->boolean('removed');
            $table->enum('removed_by', [
                'admin',
                'request',
                'reddit',
            ])->nullable();
            $table->string('removed_message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('posts', function($table) {
            $table->dropColumn([
                'removed',
                'removed_by',
                'removed_message',
            ]);
        });
    }
}

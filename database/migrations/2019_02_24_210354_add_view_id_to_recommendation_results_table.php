<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewIdToRecommendationResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('recommendation_results', function($table) {
            $table->timestamp('viewed_at')->nullable();
            $table->index('viewed_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('recommendation_results', function($table) {
            $table->dropColumn('viewed_at');
        });
    }
}

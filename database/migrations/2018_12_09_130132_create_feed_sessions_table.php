<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
                
            $table->string('feed_type');
            $table->index('feed_type');
            
            $table->enum('session_type', [
                'live',
                'batched',
            ]);
            
            $table->integer('page_size')->default(20);
            $table->integer('page')->default(1);
            $table->integer('position')->default(0);
            
            $table->integer('before_post_id')->unsigned()->nullable();
            $table->foreign('before_post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('set null');
                
            $table->integer('after_post_id')->unsigned()->nullable();
            $table->foreign('after_post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('set null');
                
            $table->text('filters');
            $table->text('params');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_sessions');
    }
}

<?php
use Illuminate\Database\Seeder;

use App\Tag;
use App\AutoTitleTag;

class AutoTitleTagsSeeder extends Seeder {
    public $values = [
        'anal' => [
            'anal',
        ],
        'ass' => [
            'ass',
            'arse',
            'butt',
            'butts',
        ],
        '60fps' => [
            '60fps',
        ],
        'ahegao' => [
            'ahegao',
        ],
        'asian' => [
            'asian',
            'asians',
            'jav',
            'japan',
            'japanese',
            'korean',
            'korea',
            'china',
            'chinese',
        ],
        'breasts' => [
            'boobs',
            'tits',
            'boob',
            'tit',
            'breasts',
            'breast',
            'boobjob',
            'titjob',
            'boobjob',
            'titjob',
            'paizuri',
            'nip',
            'nipple',
        ],
        'bimbo' => [
            'bimbo',
            'bimbos',
        ],
        'boobjob' => [
            'boobjob',
            'titjob',
            'paizuri',
        ],
        'black' => [
            'blacked',
        ],
        'blowjob' => [
            'blowjob',
            'deepthroat',
            'throat',
            'blows',
            'blowing',
        ],
        'bulge' => [
            'bulge',
        ],
        'buttplug' => [
            'buttplug',
            'plug',
            'buttplugs',
        ],
        'celeb' => [
            'celeb',
        ],
        'comic' => [
            'comic',
            'manga',
            'doujinshi',
            'comics',
        ],
        'condom' => [
            'condom',
            'condoms',
        ],
        'cosplay' => [
            'cosplay',
            'cosplaying',
        ],
        'costume' => [
            'costume',
            'nurse',
            'policewomen',
        ],
        'creampie' => [
            'creampie',
            'creampies',
        ],
        'cum' => [
            'cum',
            'cumming',
            'jizz',
            'cumslut',
            'jizzing',
        ],
        'cuntboy' => [
            'cuntboy',
        ],
        'curvy' => [
            'curvy',
        ],
        'cute' => [
            'cute',
            'adorable',
            'kawaii',
        ],
        'deepthroat' => [
            'deepthroat',
            'throat',
        ],
        'dildo' => [
            'dildo',
            'toys',
        ],
        'distension' => [
            'distension',
        ],
        'facefuck' => [
            'facefuck',
        ],
        'facial' => [
            'facial',
        ],
        'fat' => [
            'fat',
            'chubby',
            'bbw',
            'bbm',
        ],
        'feet' => [
            'feet',
            'footjob',
            'foot',
        ],
        'femboy' => [
            'femboy',
        ],
        'fisting' => [
            'fisting',
            'fists',
        ],
        'femdom '=> [
            'femdom',
            'cbt',
            'mistress',
        ],
        'freeuse' => [
            'freeuse',
        ],
        'furry' => [
            'furry',
            'fur',
        ],
        'futanari' => [
            'futanari',
            'futa',
            'futas',
        ],
        'gape' => [
            'gape',
            'gaping',
            'gapes',
        ],
        'ginger' => [
            'ginger',
            'redhead',
            'gingers',
            'redheads',
        ],
        'glasses' => [
            'glasses',
        ],
        'goth' => [
            'goth',
            'emo',
            'scene girl',
            'scene chick',
        ],
        'group' => [
            '3some',
            'threesome',
            'threesomes',
            '4some',
            'foursomes',
        ],
        'hairy' => [
            'hairy',
        ],
        'handjob' => [
            'handjob',
        ],
        'happy' => [
            'happy',
        ],
        'ignored' => [
            'ignored',
            'bored',
        ],
        'inflation' => [
            'inflation',
        ],
        'insertion' => [
            'insertion',
            'dildo',
        ],
        'interracial' => [
            'interracial',
            'blacked',
        ],
        'japanese' => [
            'japanese',
            'jav',
            'japan',
        ],
        'korean' => [
            'korean',
            'korea',
        ],
        'latex' => [
            'latex',
        ],
        'mario' => [
            'mario',
        ],
        'masturbation' => [
            'masturbation',
            'masturbating',
            'jilling',
            'jacking',
        ],
        'mature' => [
            'mature',
        ],
        'milf' => [
            'milf',
            'mom',
        ],
        'monstergirl' => [
            'monstergirl',
            'slimegirl',
            'sharkgirl',
            'spidergirl',
            'lamia',
            'harpy',
        ],
        'neko' => [
            'neko',
            'nekos',
            'nekogirl',
            'catgirl',
        ],
        'oiled' => [
            'oiled',
            'oil',
        ],
        'orgasm' => [
            'orgasm',
        ],
        'orgasm_denial' => [
            'orgasm denial',
        ],
        'outercourse' => [
            'outercourse',
            'handjob',
            'boobjob',
            'titjob',
            'paizuri',
            'footjob',
        ],
        'my_little_pony' => [
            'my little pony',
            'mlp',
        ],
        'overwatch' => [
            'overwatch',
            'sombra',
            'd.va',
        ],
        'oviposition' => [
            'oviposition',
            'egg laying',
            'egglaying',
            'laying eggs',
        ],
        'pale' => [
            'pale',
        ],
        'panties' => [
            'panties',
            'pantsu',
        ],
        'party' => [
            'party',
            'festival',
            'concert',
        ],
        'pawg' => [
            'pawg',
        ],
        'petite' => [
            'petite',
        ],
        'pokemon' => [
            'pokemon',
        ],
        'pov' => [
            'pov',
        ],
        'pregnancy' => [
            'pregnancy',
            'pregnant',
        ],
        'public' => [
            'public',
        ],
        'pussy' => [
            'pussy',
            'vagina',
        ],
        'reveal' => [
            'reveal',
            'boobdrop',
            'titdrop',
            'boob drop',
            'tit drop',
            'undressing',
        ],
        'ruined_orgasm' => [
            'ruined',
        ],
        'rwby' => [
            'rwby',
        ],
        'smell' => [
            'smell',
        ],
        'spanking' => [
            'spanking',
            'spank',
        ],
        'swimwear' => [
            'swimwear',
            'bikini',
            'swimsuit',
        ],
        'teen' => [
            'teen',
        ],
        'tentacles' => [
            'tentacles',
        ],
        'tgirl' => [
            'tgirl',
        ],
        'thick' => [
            'thick',
        ],
        'transformation' => [
            'transformation',
            'tf',
        ],
        'trashy' => [
            'trashy',
        ],
        'twink' => [
            'twink',
        ],
        'yoga_pants' => [
            'yoga pants',
            'yogapants',
        ],
        
    ];
    
    public function run() {
        foreach ($this->values as $key => $patterns) {
            $tag = Tag::where('key', $key)->firstOrFail();
            
            $tag->autoTitleTags()->delete();
            foreach ($patterns as $pattern) {
                if (strpos('/', $pattern) === FALSE) {
                    $pattern = '/\b' . $pattern . 's?\b/i';
                }
                $autoTag = new AutoTitleTag;
                $autoTag->tag()->associate($tag);
                $autoTag->pattern = $pattern;
                $autoTag->save();
                
                dump("$pattern -> $key");
            }
        }
    }
}

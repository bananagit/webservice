import Vue from 'vue';
import VueShortkey from 'vue-shortkey';
import Vue2TouchEvents from 'vue2-touch-events';
import VueHighlightJS from 'vue-highlightjs'
import PrettyCheckbox from 'pretty-checkbox-vue';
import router from '@/router/index';
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import 'vue2-animate/dist/vue2-animate.min.css';
import 'pretty-checkbox/src/pretty-checkbox.scss';
//import 'highlightjs/styles/paraiso.dark.css';

import App from './App';
import session from '@/http/session';

import './filters';

Vue.use(PrettyCheckbox);
Vue.use(VueShortkey, {
    prevent: ["input", "textarea"],
});
Vue.use(Vue2TouchEvents);
Vue.use(VueHighlightJS);

/**
 * Auto-register base components starting with "App.." or "Icon..."
 */
const requireComponent = require.context('./components/base', false, /App[A-Z]\w+\.(vue|js)$/);
requireComponent.keys().forEach(filename => {
    const componentConfig = requireComponent(filename);
    const componentName = upperFirst(camelCase(filename.replace(/^\.\/(.*)\.\w+$/, '$1')));
    
    Vue.component(componentName, componentConfig.default || componentConfig);
});
const requireComponentIcons = require.context('./components/icons', false, /Icon[A-Z]\w+\.(vue|js)$/);
requireComponentIcons.keys().forEach(filename => {
    const componentConfig = requireComponentIcons(filename);
    let componentName = upperFirst(camelCase(filename.replace(/^\.\/(.*)\.\w+$/, '$1')));
    
    Vue.component(componentName, componentConfig.default || componentConfig);
});

const app = new Vue({
    el: '#app',
    template: '<app/>',
    components: { App },
    router,
});


session.on('update', async () => {
    var user = await session.getUser();
    console.log('session onchange', {
        user,
    });
    
    if (window.ga) {
        window.ga('set', 'userId', user.id);
        window.ga('send', 'event', 'authentication', 'user-id available');
    }
});
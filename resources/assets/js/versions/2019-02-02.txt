Tagging improvements:
    Improved the 'manage tags' search to be more intutive. It will now return
        infix matches (i.e. 'breasts' will return 'big_breasts'), and
        tags with matches in the description.
    Opening the 'manage tags' popup will now auto-focus the search field.
    Hitting enter in the 'manage tags' search will now add the top result.

Tag namespaces:
    We've started working on introducing tag namespaces. Some tags will have
        a prefix which indicate what type of tag it is. Some namespaces
        include 'female', 'male', and 'trans' for specifying the gender a 
        tag applies to, or 'series' and 'artist' for specifying a media's
        source.
    A full guide on tagging including namespaces will be coming in the future.
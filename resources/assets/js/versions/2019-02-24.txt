UI improvements:
    We've made some general improvements to the look and feel of the site.
    Let us know what you think on Discord!
Performance optimizations:
    We've made more optimizations to the database and feed session
    system, which should improve general site performance.
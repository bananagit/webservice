import Evee from 'evee';
import api from './api';

class Session extends Evee {
    constructor() {
        super();
        this._user = null;
        this._settings = null;
        this._trust = null;
        this._contentPrefs = null;
        
        this._waitingPromise = null
        this._waitingPromiseSettings = null
        this._waitingPromiseTrust = null;
        this._waitingPromiseContentPrefs;
    }
    
    async getUser() {
        if (this._user) {
            return this._user;
        }
        if (this._waitingPromise) {
            return await this._waitingPromise;
        }
        var promise = this._queueGetUser();
        this._waitingPromise = promise;
        return await promise;
    }
    
    async _queueGetUser() {
        var res = await api.get('/account');
        var old = this._user;
        this._user = res.data.user;
        if (old != this._user) {
            this.emit('update');
        }
        return this._user;
    }
    
    async getTrust() {
        if (this._trust) {
            return this._trust;
        }
        if (this._waitingPromiseTrust) {
            return await this._waitingPromiseTrust;
        }
        var promise = this._queueGetTrust();
        this._waitingPromiseTrust = promise;
        return await promise;
    }
    
    async _queueGetTrust() {
        var res = await api.get('/account/trust');
        var old = this._trust;
        this._trust = res.data.trust;
        if (old != this._trust) {
            this.emit('update');
        }
        return this._trust;
    }
    
    async refreshContentPrefs() {
        var promise = this._queueGetContentPrefs();
        this._waitingPromiseContentPrefs = promise;
        return await promise;
    }
    
    async getContentPrefs() {
        if (this._contentPrefs) {
            return this._contentPrefs;
        }
        if (this._waitingPromiseContentPrefs) {
            return await this._waitingPromiseContentPrefs;
        }
        var promise = this._queueGetContentPrefs();
        this._waitingPromiseContentPrefs = promise;
        return await promise;
    }
    
    async _queueGetContentPrefs() {
        var res = await api.get('/account/content_preferences');
        var old = this._contentPrefs;
        this._contentPrefs = {
            blacklisted_subreddits: res.data.blacklisted_subreddits,
            blacklisted_tags: res.data.blacklisted_tags,
        };
        if (old != this._contentPrefs) {
            this.emit('update');
        }
        return this._contentPrefs;
    }
    
    async refreshUser() {
        var promise = this._queueGetUser();
        this._waitingPromise = promise;
        this.refreshSettings();
        return await promise;
    }
    
    async getSettings() {
        if (this._settings) {
            return this._settings;
        }
        if (this._waitingPromiseSettings) {
            return await this._waitingPromiseSettings;
        }
        var promise = this._queueGetSettings();
        this._waitingPromiseSettings = promise;
        return await promise;
    }
    
    async _queueGetSettings() {
        var res = await api.get('/account/settings');
        var old = this._settings;
        this._settings = res.data.settings;
        if (old != this._settings) {
            this.emit('update');
        }
        return this._settings;
    }
    
    async refreshSettings() {
        var promise = this._queueGetSettings();
        this._waitingPromiseSettings = promise;
        return await promise;
    }
}


export default new Session;
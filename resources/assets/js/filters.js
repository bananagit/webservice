import Vue from 'vue';
import moment from 'moment';

Vue.filter('fromNow', (value) => {
    if (!value) {
        return 'never';
    }
    return moment.utc(value).fromNow();
});

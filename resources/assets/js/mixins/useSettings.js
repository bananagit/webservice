import session from '@/http/session';

export default {
    data() {
        return {
            '_settings': 'testing',
        };
    },
    mounted() {
        console.log('usesettings mounted 2');
        this.$settings = 'testing 2';
        session.on('update', () => {
            this._loadSettings();
        });
    },
    methods: {
        async _loadSettings() {
            var settings = await session.getSettings();
            this.$settings = settings;
            console.log('got settings', {settings, t: this});
        },
    },
};
        
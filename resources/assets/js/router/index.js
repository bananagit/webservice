import Vue from 'vue'
import VueRouter from 'vue-router';

import Homepage from './Homepage';
import NotFound from './NotFound';

import Signup from './Signup';
import Login from './Login';

import Viewer from './Viewer';

import HelpDevelopment from './HelpDevelopment';

import feedRoutes from './feeds/index';
import myStuffRoutes from './mystuff/index';
import moderationRoutes from './moderation/index';
import aboutRoutes from './about/index';
import legalRoutes from './legal/index';
import contentRoutes from './content/index';
import profileRoutes from './profile/index';
import listRoutes from './lists/index';
import dataRoutes from './data/index';
import labsRoutes from './labs/index';

import session from '@/http/session';

Vue.use(VueRouter);

var router = new VueRouter({
    mode: 'history',
    routes: [
        {path: '/', name: 'Homepage', component: Homepage, meta: {title: 'Home'}},
        
        {path: '/account/signup', name: 'Signup', component: Signup, meta: {title: 'Signup'}},
        {path: '/account/login', name: 'Login', component: Login, meta: {title: 'Login'}},
        
        
        {path: '/view/:session/:page/:position/:post', name: 'Viewer', component: Viewer, meta: {title: 'View post', requiresAuth: true}},
//        {path: '/view/:feed/:page/:after/:id', name: 'Viewer', component: Viewer},
        
        
        ...myStuffRoutes,
        ...profileRoutes,
        ...feedRoutes,        
        ...listRoutes,
        ...contentRoutes,
        ...aboutRoutes,
        ...legalRoutes,
        ...moderationRoutes,
        ...dataRoutes,
        ...labsRoutes,
        
        {path: '*', name: 'NotFound', component: NotFound, meta: {title: '404'}},
    ]
});

router.beforeEach((to, from, next) => {
    document.title = to.meta.title + ' - AIFap';
    next();
});

router.beforeEach(async (to, from, next) => {
    if (to.meta.requiresAuth) {
        var user = await session.getUser();
        if (!user) {
            return next({
                name: 'Login',
            });
        }
    }
    next();
});

export default router;
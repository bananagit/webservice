import DataIndex from './DataIndex';
import BfImporter from './BfImporter';
import DataExport from './DataExport';

export default [
    {path: '/data', name: 'DataIndex', component: DataIndex, children: [
        {path: 'import/bf', name: 'BfImporter', component: BfImporter, meta: {title: 'Import from BF'}},
        {path: 'export', name: 'DataExport', component: DataExport, meta: {title: 'Export'}},
    ]},
];

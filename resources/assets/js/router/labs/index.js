import Labs from './Labs';
import FeedLabRecommendations from './FeedLabRecommendations';
import LabsMixer from './LabsMixer';


export default [
    {path: '/labs', component: Labs, children: [
        {path: '', name: 'Labs', redirect: {name: 'FeedLabRecommendations'}},
        
        {path: 'recommendations', name: 'FeedLabRecommendations', component: FeedLabRecommendations, meta: {title: 'Recommended', requiresAuth: true}},
        {path: 'mixer', name: 'LabsMixer', component: LabsMixer, meta: {title: 'Mixer', requiresAuth: true}},
    ], meta: {requiresAuth: true}},
];
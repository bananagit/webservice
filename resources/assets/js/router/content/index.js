import ContentIndex from './ContentIndex';
import ContentTags from './ContentTags';
import ContentSuggestTags from './ContentSuggestTags';
import ContentSuggestTagsTop from './ContentSuggestTagsTop';
import ContentSuggestTagsRecent from './ContentSuggestTagsRecent';
import ContentSuggestTagsSearch from './ContentSuggestTagsSearch';
import ContentSuggestTagsNew from './ContentSuggestTagsNew';
import ContentSuggestTagsInfo from './ContentSuggestTagsInfo';
import ContentSuggestTagsMine from './ContentSuggestTagsMine';
import ContentSubreddits from './ContentSubreddits';

export default [
    {path: '/content', component: ContentIndex, children: [
        {path: '/', name: 'ContentIndex', redirect: {name: 'ContentTags'}},
        
        {path: 'tags', name: 'ContentTags', component: ContentTags, meta: {title: 'Tags'}},
        {path: 'tags/suggest', component: ContentSuggestTags, meta: {title: 'Suggest tags'}, children: [
            {path: '/', name: 'ContentSuggestTagsTop', component: ContentSuggestTagsTop, meta: {title: 'Suggest tags'}},
            {path: 'recent', name: 'ContentSuggestTagsRecent', component: ContentSuggestTagsRecent, meta: {title: 'Suggest tags'}},
            {path: 'search', name: 'ContentSuggestTagsSearch', component: ContentSuggestTagsSearch, meta: {title: 'Suggest tags'}},
            {path: 'new', name: 'ContentSuggestTagsNew', component: ContentSuggestTagsNew, meta: {title: 'Suggest tags'}},
            {path: 'info', name: 'ContentSuggestTagsInfo', component: ContentSuggestTagsInfo, meta: {title: 'Suggest tags'}},
            {path: 'my-suggestions', name: 'ContentSuggestTagsMine', component: ContentSuggestTagsMine, meta: {title: 'Suggest tags'}},
        ]},
        {path: 'subreddits', name: 'ContentSubreddits', component: ContentSubreddits, meta: {title: 'Subreddits'}},
    ]},
];

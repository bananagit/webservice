import LegalIndex from './LegalIndex';
import LegalTerms from './LegalTerms';
import LegalPrivacy from './LegalPrivacy';
import LegalDmca from './LegalDmca';

export default [
    {path: '/legal', name: 'LegalIndex', component: LegalIndex, children: [
        {path: 'terms', name: 'LegalTerms', component: LegalTerms, meta: {title: 'Terms'}},
        {path: 'privacy', name: 'LegalPrivacy', component: LegalPrivacy, meta: {title: 'Privacy'}},
        {path: 'dmca', name: 'LegalDmca', component: LegalDmca, meta: {title: 'DMCA'}},
    ]},
];

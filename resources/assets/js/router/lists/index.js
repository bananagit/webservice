import ListFeeds from './ListFeeds';
import FeedSavedList from './FeedSavedList';
import MyLists from './MyLists';

export default [
    {path: '/lists', name: 'ListFeeds', component: ListFeeds, children: [
        {path: '', name: 'Lists', redirect: {name: 'MyLists'}},
        
        {path: 'my-lists', name: 'MyLists', component: MyLists, meta: {title: 'Lists', requiresAuth: true}},
        {path: 'list/:id', name: 'FeedSavedList', component: FeedSavedList, meta: {title: 'Saved list', requiresAuth: true}},
    ], meta: {requiresAuth: true}},
];
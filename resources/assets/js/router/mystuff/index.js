import MyStuff from './MyStuff';
import WatchLater from './WatchLater';
import Loved from './Loved';
import LikedLoved from './LikedLoved';
import DislikedHated from './DislikedHated';
import PrivatePosts from './PrivatePosts';
import Viewed from './Viewed';

export default [
    {path: '/my-stuff', component: MyStuff, children: [
        {path: '/', name: 'MyStuff', redirect: {name: 'LikedLoved'}},
        
        {path: 'watch-later', name: 'WatchLater', component: WatchLater, meta: {title: 'Watch later', requiresAuth: true}},
        {path: 'loved', name: 'Loved', component: Loved, meta: {title: 'Loved', requiresAuth: true}},
        {path: 'liked-loved', name: 'LikedLoved', component: LikedLoved, meta: {title: 'Liked & Loved', requiresAuth: true}},
        {path: 'disliked-hated', name: 'DislikedHated', component: DislikedHated, meta: {title: 'Disliked & Hated', requiresAuth: true}},
        {path: 'private-posts', name: 'PrivatePosts', component: PrivatePosts, meta: {title: 'Private posts', requiresAuth: true}},
        {path: 'viewed', name: 'Viewed', component: Viewed, meta: {title: 'Viewed', requiresAuth: true}},
    ], meta: {requiresAuth: true}},
];
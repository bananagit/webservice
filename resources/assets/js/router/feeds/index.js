import Feeds from './Feeds';

import recommendedRoutes from './recommended/index';
import browseRoutes from './browse/index';
import searchRoutes from './search/index';

export default [
    {path: '/feeds', name: 'Feeds', component: Feeds, children: [
        ...recommendedRoutes,
        ...browseRoutes,
        ...searchRoutes,
    ], meta: {requiresAuth: true}},
];
import SearchFeeds from './SearchFeeds';
import FeedSearch from './FeedSearch';
import FeedSearchAdvanced from './FeedSearchAdvanced';

export default [
    {path: '', name: 'SearchFeeds', component: SearchFeeds, children: [
        {path: 'search', name: 'FeedSearch', component: FeedSearch, meta: {title: 'Search results', requiresAuth: true}},
        {path: 'search/advanced', name: 'FeedSearchAdvanced', component: FeedSearchAdvanced, meta: {title: 'Search results', requiresAuth: true}},
    ], meta: {requiresAuth: true}},
];
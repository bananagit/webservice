import RecommendedFeeds from './RecommendedFeeds';
import FeedRecommendations from './FeedRecommendations';
import FeedRecommendationsAggregate from './FeedRecommendationsAggregate';


export default [
    {path: 'recommended', component: RecommendedFeeds, children: [
        {path: '', name: 'RecommendedFeeds', redirect: {name: 'FeedRecommendationsAggregate'}},
        
        {path: 'legacy', name: 'FeedRecommendations', component: FeedRecommendations, meta: {title: 'Recommended', requiresAuth: true}},
        {path: 'v2', name: 'FeedRecommendationsAggregate', component: FeedRecommendationsAggregate, meta: {title: 'Recommended', requiresAuth: true}},
    ], meta: {requiresAuth: true}},
];
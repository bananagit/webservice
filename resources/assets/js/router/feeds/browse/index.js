import BrowseFeeds from './BrowseFeeds';
import FeedNew from './FeedNew';
import FeedPopular from './FeedPopular';
import FeedExperiment from './FeedExperiment';
import FeedRandom from './FeedRandom';
import FeedSubreddit from './FeedSubreddit';
import FeedTag2 from './FeedTag2';
import FeedTesting from './FeedTesting';
import FeedSinglePost from './FeedSinglePost';

export default [
    {path: '/', name: 'BrowseFeeds', component: BrowseFeeds, children: [
        {path: 'new', name: 'FeedNew', component: FeedNew, meta: {title: 'New', requiresAuth: true}},
        {path: 'popular', name: 'FeedPopular', component: FeedPopular, meta: {title: 'Popular', requiresAuth: true}},
        {path: 'experiment', name: 'FeedExperiment', component: FeedExperiment, meta: {title: 'Experiment', requiresAuth: true}},
        {path: 'random', name: 'FeedRandom', component: FeedRandom, meta: {title: 'Random', requiresAuth: true}},
        {path: 'testing', name: 'FeedTesting', component: FeedTesting, meta: {title: 'Testing', requiresAuth: true}},
        {path: 'subreddit/:id/:slug', name: 'FeedSubreddit', component: FeedSubreddit , meta: {title: 'Subreddit', requiresAuth: true}},
        {path: 'tag/:id/:key', name: 'FeedTag', component: FeedTag2 , meta: {title: 'Tagged', requiresAuth: true}},
        {path: 'single', name: 'FeedSinglePost', component: FeedSinglePost , meta: {title: 'Single', requiresAuth: true}},
    ], meta: {requiresAuth: true}},
];
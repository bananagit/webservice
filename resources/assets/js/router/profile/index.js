import Profile from './Profile';
import ProfileAccount from './ProfileAccount';
import ProfileRatingsSummary from './ProfileRatingsSummary';
import ProfileProgression from './ProfileProgression';
import ProfileSettings from './ProfileSettings';
import ProfileContentPrefs from './ProfileContentPrefs';
import ProfileSupporter from './ProfileSupporter';
import ProfileSecurity from './ProfileSecurity';

export default [
    {path: '/profile', component: Profile, children: [
        {path: '/', name: 'Profile', redirect: {name: 'ProfileAccount'}},
        
        {path: 'account', name: 'ProfileAccount', component: ProfileAccount, meta: {title: 'Account', requiresAuth: true}},
        {path: 'ratings', name: 'ProfileRatingsSummary', component: ProfileRatingsSummary, meta: {title: 'Ratings', requiresAuth: true}},
        {path: 'supporter', name: 'ProfileSupporter', component: ProfileSupporter, meta: {title: 'Supporter', requiresAuth: true}},
        {path: 'progression', name: 'ProfileProgression', component: ProfileProgression, meta: {title: 'Progression', requiresAuth: true}},
        {path: 'security', name: 'ProfileSecurity', component: ProfileSecurity, meta: {title: 'Security', requiresAuth: true}},
        {path: 'content-preferences', name: 'ProfileContentPrefs', component: ProfileContentPrefs, meta: {title: 'Content Preferences', requiresAuth: true}},
        {path: 'settings', name: 'ProfileSettings', component: ProfileSettings, meta: {title: 'Settings', requiresAuth: true}},
    ], meta: {requiresAuth: true}},
];
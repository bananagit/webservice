class VideoEmbeds {
    getIframeSrc(mediaUrl) {
        if (mediaUrl.indexOf('youtube.com') > -1 || mediaUrl.indexOf('youtu.be') > -1) {
            return this._getYoutubeSrc(mediaUrl);
        }
        if (mediaUrl.indexOf('vimeo.com') > -1) {
            return this._getVimeoSrc(mediaUrl);
        }
        if (mediaUrl.indexOf('pornhub.com') > -1) {
            return this._getPornhubSrc(mediaUrl);
        }
        if (mediaUrl.indexOf('xvideos.com') > -1) {
            return this._getXvideosSrc(mediaUrl);
        }
        if (mediaUrl.indexOf('xhamster.com') > -1) {
            return this._getXhamsterSrc(mediaUrl);
        }
        return null;
    }
    
    _getXhamsterSrc(url) {
        var regExp = /xhamster\.com\/movies\/([0-9]*)/;
        var match = url.match(regExp);
    
        if (!match) {
            return null;
        }
        var id = match[1];
        return 'https://xhamster.com/embed/' + id + '/?autoplay=1';
    }
    
    _getXvideosSrc(url) {
        var regExp = /xvideos\.com\/video([0-9]*)/;
        var match = url.match(regExp);
    
        if (!match) {
            return null;
        }
        var id = match[1];
        return 'https://www.xvideos.com/embedframe/' + id + '/?autoplay=1';
    }
    
    _getPornhubSrc(url) {
        var regExp = /pornhub\.com\/view_video\.php\?viewkey=([a-zA-Z0-9]*)/;
        var match = url.match(regExp);
    
        if (!match) {
            return null;
        }
        var id = match[1];
        return 'https://www.pornhub.com/embed/' + id + '?autoplay=1';
    }
    
    
    _getYoutubeSrc(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
    
        if (!match || match[2].length != 11) {
            return null;
        }
        var id = match[2];
        return '//www.youtube.com/embed/' + id + '?rel=0&autoplay=1'; //&mute=1';
    }
    
    _getVimeoSrc(url) {
        var regExp = /vimeo.com\/(.*)\/?/;
        var match = url.match(regExp);
    
        if (!match) {
            return null;
        }
        var id = match[1];
        return 'https://player.vimeo.com/video/' + id + '?background=1';
    }
}

export default new VideoEmbeds;
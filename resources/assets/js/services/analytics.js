class Analytics {
    constructor() {
        this._ga = window.ga;
    }
    
    record(eventCategory, eventAction, eventLabel) {
        if (!this._ga) {
            return;
        }
        
        this._ga('send', {
          hitType: 'event',
          eventCategory,
          eventAction,
          eventLabel
        });
    }
    
    recordSearch(query) {
        this.record('search', 'query', query);
    }
    recordSearchNavigate(query) {
        this.record('search', 'results-page', query);
    }
    
    recordSearchResult(result) {
        this.record('search', 'result-found', result);
    }
}

export default new Analytics;
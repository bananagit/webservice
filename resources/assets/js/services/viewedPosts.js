import localStorage from 'local-storage-fallback';
import api from '@/http/api';

const LS_KEY = 'aif_viewed_posts';

class ViewedPost {
    constructor() {
        try {
            this._viewed = JSON.parse(localStorage.getItem(LS_KEY));
        } catch (e) {
            this._viewed = {};
        }
        if (!this._viewed) {
            this._viewed = {}
        }
        
        this._viewsWaitingToSync = [];
        
        setInterval(() => {
            this._syncViews();
        }, 5000);
    }
    
    isViewed(id) {
        return !!this._viewed[id];
    }
    
    setViewed(id, viewed) {
        this._viewed[id] = viewed;
        this._serialize();
    }
    
    clearViewed() {
        this._viewed = {};
        this._serialize();
    }
    
    _serialize() {
        localStorage.setItem(LS_KEY, JSON.stringify(this._viewed));
    }
    
    
    markViewed(postId) {
        this._viewed[postId] = true;
        this._viewsWaitingToSync.push(postId);
    }
    
    _syncViews() {
        if (this._viewsWaitingToSync.length < 1) {
            return;
        }
        var postIds = this._viewsWaitingToSync;
        this._viewsWaitingToSync = [];
        
        api.post('/posts/mark_viewed_bulk', {
            ids: postIds,
        });
    }
}

export default new ViewedPost();
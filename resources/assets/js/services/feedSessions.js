import api from '@/http/api';
import userSession from '@/http/session';

class FeedSession {
    constructor(type, filters, params) {
        this.id = null;
        this.feedType = type;
        this.filters = filters || {};
        this.params = params || {};
        this.pageSize = 20;
        this.page = 1;
        this.position = 0;
        this.error = null;
        this.refreshedAt = null;
    }
    
    _populate(sessionData) {
        this.id = sessionData.id;
        this.feedType = sessionData.feed_type;
        this.filters = sessionData.filters || {};
        this.params = sessionData.params || {};
        this.pageSize = sessionData.page_size;
        this.page = sessionData.page;
        this.position = sessionData.position;
        this.error = sessionData.error;
        this.refreshedAt = sessionData.refreshed_at;
    }
    
    async index(page, position) {
        var res = await api.post('/feeds/index', {
            session_id: this.id,
            type: this.feedType,
            filters: this.filters,
            params: this.params,
            page,
            position,
        });
        
        if (res.data.success) {
            this._populate(res.data.session);
            return this._mapItems(res.data.posts);
        } else {
            throw 'Error';
        }
    }
    
    async refresh() {
        var res = await api.post('/feeds/refresh', {
            session_id: this.id,
            type: this.feedType,
            filters: this.filters,
            params: this.params,
        });
        
        if (res.data.success) {
            this._populate(res.data.session);
            return this._mapItems(res.data.posts);
        } else {
            throw 'Error';
        }
    }
    
    async nextPage() {
        var res = await api.post('/feeds/next', {
            session_id: this.id,
            type: this.feedType,
            filters: this.filters,
            params: this.params,
        });
        
        if (res.data.success) {
            this._populate(res.data.session);
            return this._mapItems(res.data.posts);
        } else {
            throw 'Error';
        }
    }
    
    async prevPage() {
        if (this.page <= 1) {
            return [];
        }
        
        var res = await api.post('/feeds/prev', {
            session_id: this.id,
            type: this.feedType,
            filters: this.filters,
            params: this.params,
        });
        
        if (res.data.success) {
            this._populate(res.data.session);
            return this._mapItems(res.data.posts);
        } else {
            throw 'Error';
        }
    }
    
    
    _mapItems(posts) {
        var r = [];
        for (let i in posts) {
            var post = posts[i];
            post._pagination = {
                page: this.page,
                position: i,
            };
            r.push(post);
        }
        console.log('mapitems', {r});
        return r;
    }
}

class FeedSessions {
    constructor() {
        this._sessions = {
            newPosts: null,
            viewed: null,
            search: {},
            searchAdvanced: {},
            privatePosts: null,
            recommendation: null,
            recommendation_aggregated: null,
            recommendation_lab: null,
            watchLater: null,
            experiment: null,
            random: null,
            testing: null,
            popular: null,
            subreddits: {},
            tags: {},
            ratings: {},
            singlePosts: {},
            savedLists: {},
        };
    }
    
    async getSinglePostSession(postId) {
        if (this._sessions.singlePosts[postId]) {
            return this._sessions.singlePosts[postId];
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('single-post', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {
            post_id: postId,
        });
        
        this._sessions.singlePosts[postId] = session;
        return session;
    }
    
    async getSavedListSession(listId) {
        if (this._sessions.savedLists[listId]) {
            return this._sessions.savedLists[listId];
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('saved-list', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {
            saved_list_id: listId,
        });
        
        this._sessions.savedLists[listId] = session;
        return session;
    }
    
    
    async getSearchAdvancedSession(query) {
        if (this._sessions.searchAdvanced[query]) {
            return this._sessions.searchAdvanced[query];
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('search-advanced', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {
            query,
        });
        
        this._sessions.searchAdvanced[query] = session;
        return session;
    }
    
    async getSearchSession(query) {
        if (this._sessions.search[query]) {
            return this._sessions.search[query];
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('search', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {
            query,
        });
        
        this._sessions.search[query] = session;
        return session;
    }
    
    async getViewedSession() {
        if (this._sessions.viewed) {
            return this._sessions.viewed;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('viewed', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.viewed = session;
        return session;
    }
    
    async getTestingSession() {
        if (this._sessions.testing) {
            return this._sessions.testing;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('testing', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.testing = session;
        return session;
    }
    
    async getPrivatePostsSession() {
        if (this._sessions.privatePOsts) {
            return this._sessions.privatePOsts;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('private-posts', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.privatePosts = session;
        return session;
    }
    
    async getPopularSession() {
        if (this._sessions.popular) {
            return this._sessions.popular;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('popular', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.popular = session;
        return session;
    }
    
    async getExperimentSession() {
        if (this._sessions.experiment) {
            return this._sessions.experiment;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('experiment', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.experiment = session;
        return session;
    }
    
    async getRandomSession() {
        if (this._sessions.random) {
            return this._sessions.random;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('random', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.random = session;
        return session;
    }
    
    async getNewPostsSession() {
        if (this._sessions.newPosts) {
            return this._sessions.newPosts;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('new', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.newPosts = session;
        return session;
    }
    
    async getRecommendationLabSession() {
        if (this._sessions.recommendation_lab) {
            return this._sessions.recommendation_lab;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('recommendation-lab', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.recommendation_lab = session;
        return session;
    }
    
    async getRecommendationAggregatedSession() {
        if (this._sessions.recommendation_aggregated) {
            return this._sessions.recommendation_aggregated;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('recommendation-aggregated', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.recommendation_aggregated = session;
        return session;
    }
    
    
    async getRecommendationSession() {
        if (this._sessions.recommendation) {
            return this._sessions.recommendation;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('recommendation', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.recommendation = session;
        return session;
    }
    
    async getWatchLaterSession() {
        if (this._sessions.watchLater) {
            return this._sessions.watchLater;
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('watch-later', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {});
        
        this._sessions.watchLater = session;
        return session;
    }
    
    async getTagSession(tagId) {
        if (this._sessions.tags[tagId]) {
            return this._sessions.tags[tagId];
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('tag', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {
            tag_id: tagId,
        });
        
        this._sessions.tags[tagId] = session;
        return session;
    }
    
    async getSubredditSession(subredditId) {
        if (this._sessions.subreddits[subredditId]) {
            return this._sessions.subreddits[subredditId];
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('subreddit', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {
            subreddit_id: subredditId,
        });
        
        this._sessions.subreddits[subredditId] = session;
        return session;
    }
    
    async getRatingSession(ratings) {
        if (this._sessions.ratings[ratings.join(',')]) {
            return this._sessions.ratings[ratings.join(',')];
        }
        
        var user = await userSession.getUser();
        
        var session = new FeedSession('rating', {
            gender_type: user.gender_filter,
            media_type: user.media_filter,
            artstyle_type: user.artstyle_filter,
        }, {
            ratings,
        });
        
        this._sessions.ratings[ratings.join(',')] = session;
        return session;
    }
    
    async getSession(id) {
        var res = await api.get('/feeds/sessions/' + id);
        if (!res.data.session) {
            return null;
        }
        var session = new FeedSession(null, {}, {});
        session._populate(res.data.session);
        return session;
    }
}


export default new FeedSessions;
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="trafficjunky-site-verification" content="soyqz92vt" />
    <meta name="juicyads-site-verification" content="f5384156d6e859a56fc95f0b09244170">
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="google-site-verification" content="3hiloSIHOjdaBPoIkLLTLnLOqP-napxV6eVdoYrkUY0">
    
    <title>AIFap - Improving porn with AI and machine learning</title>
    
    <meta name="description" content="AIFap is a porn site that uses AI to learn your preferences and recommend things we think you'll like.">
    <meta itemprop="name" content="AIFap - Improving porn with AI and machine learning">
    <meta itemprop="description" content="AIFap is a porn site that uses AI to learn your preferences and recommend things we think you'll like.">
    <meta name="og:title" content="AIFap - Improving porn with AI and machine learning">
    <meta name="og:description" content="AIFap is a porn site that uses AI to learn your preferences and recommend things we think you'll like.">
    <meta name="og:site_name" content="AIFap">
    <meta name="og:type" content="website">
    
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
        
        
</head>
<body>
        <!-- JuicyAds v3.2P Start -->
        <script type="text/javascript">
        var juicy_tags = ['a', 'img'];
        </script>
        <script type="text/javascript" src="https://js.juicyads.com/jp.php?c=34841333p284u4q2r2d453b494&u=https%3A%2F%2Fwww.patreon.com%2Faif"></script>
        <!-- JuicyAds v3.2P End -->

        <div id="app" class="AIF-App"><div class="AIF-App__BackgroundOverlay"></div> <div class="AIF-App__Content"><div class="AIF-AppFrame"><div class="AIF-MainHeader"><div class="AIF-MainHeader__Container"><div class="AIF-MainHeader__Nav"><div class="AIF-MainHeader__Brand"><div class="AIF-Icon AIF-Icon--brand AIF-MainHeader__BrandIcon"><img src="/fonts/brand.svg?1a25c4139fcedae6673929b182668922"></div>
                AIFap 
                <span class="AIF-MainHeader__BrandHighlight">
                    Beta
                </span> <span class="AIF-MainHeader__Version">
                    ver. 2018-12-11
                </span></div> <a href="/account/login" class="AIF-MainHeader__NavItem">
                    Login
                </a> <a href="/account/signup" class="AIF-MainHeader__NavItem">
                    Signup
                </a> <a href="/help-us" class="AIF-MainHeader__NavItem">
                Help us
            </a></div> <!----></div></div> <div class="AIF-AppFrame__Page"><div class="AIF-AppLayout"><div class="AIF-AppLayoutSection"><div class="AIF-AppPageContainer"><div class="AIF-AppCard">
            <h1 class="AIF-AppCard__Title">
            Improving porn with AI and machine learning

    </h1> <div class="AIF-AppCard__Content"><div class="AIF-AppCardSection"><!----> <div class="AIF-AppCardSection__Content"><p>
                        AIFap is a platform which indexes and recommends porn.
                        Posts are sourced from subreddits, and the recommendations are
                        powered by your own likes and dislikes.
                        The more your rate, the better our recommendations will get.
                    </p></div></div> <div class="AIF-AppCardSection"><div class="AIF-AppCardSection__Title">
        Getting started
    </div> <div class="AIF-AppCardSection__Content">
                    To get started, sign up for an account and then hit 'Porn!'.
                    Leave ratings on posts to help us train our recommendations.
                </div></div> <div class="AIF-AppCardSection"><div class="AIF-AppCardSection__Title">
        Get involved
    </div> <div class="AIF-AppCardSection__Content"><p><a href="https://discord.gg/3x66D9d">
                            Join us on discord
                        </a> 
                        to get development updates and leave your suggestions.
                    </p></div></div> <div class="AIF-AppCardSection"><div class="AIF-AppCardSection__Title">
        AIFap is in active development
    </div> <div class="AIF-AppCardSection__Content"><p>
                        We're expanding AIFap every day, so please be aware that there may be bugs.
                    </p> <p>
                        Check out our
                        <a href="https://gitlab.com/aifap/aifap-issues">
                             project on GitLab
                        </a>
                        to see what we're working on and make bug reports or feature requests.
                    </p></div></div></div></div></div></div> <div class="AIF-AppLayoutSection"><div class="AIF-AppPageContainer"><div class="AIF-AppCard"><div class="AIF-AppCard__Title">
        Update notes
    </div> <div class="AIF-AppCard__Content"><div class="AIF-AppCardSection"><!----> <div class="AIF-AppCardSection__Content"><div class="AIF-UpdateNotes"><div class="AIF-UpdateNotes__Version"><div class="AIF-UpdateNotes__Title">
            2018-12-11
        </div> <pre class="AIF-UpdateNotes__Content">User settings:
    There's now a customization settings section on your profile.
    Added a setting to show or hide video controls on animated posts.</pre></div><div class="AIF-UpdateNotes__Version"><div class="AIF-UpdateNotes__Title">
            2018-12-10
        </div> <pre class="AIF-UpdateNotes__Content">View tracking:
    We won't recommend you posts you've already viewed anymore.
Bugfixes:
    Rating posts in your recommendations won't cause your recommendation feed
    to get shuffle anymore.</pre></div><div class="AIF-UpdateNotes__Version"><div class="AIF-UpdateNotes__Title">
            2018-12-09
        </div> <pre class="AIF-UpdateNotes__Content">Recommendations:
    The first iteration of our recommendation feed is now live!
    There's a slider on the recommendation feed that you can use to adjust
    how experimental the recommendations are.
Random feed:
    A random feed has also been added - it shows posts in a random order.
Feed system overhauled:
    We've restructured how feeds work under the hood. You shouldn't
    notice to many changes, but this lays the groundwork for recommendation
    feeds.
Loved feed:
    You can now view a feed of only the posts you loved, in 'My stuff'.</pre></div><div class="AIF-UpdateNotes__Version"><div class="AIF-UpdateNotes__Title">
            2018-12-08
        </div> <pre class="AIF-UpdateNotes__Content">Privacy policy and DMCA notice:
    Please note that our privacy policy and DMCA notice is now posted in the footer
    of the site. By using AIFap, you agree to our privacy policy.
Ratings summary:
    You can now view your a summary of your favorite gender/media/artstyle types,
    and most liked/most disliked subreddits in your profile.
Bugfixes:
    Fixed an issue where rating a post and quickly pressing next/previous
        would cause the next/previous post to appear with that rating, rather
        than the post you intended to rate.</pre></div><div class="AIF-UpdateNotes__Version"><div class="AIF-UpdateNotes__Title">
            2018-12-07
        </div> <pre class="AIF-UpdateNotes__Content">Flag posts:
    You can now flag posts. 
    Please only flag missing or illegal content, not content we can't display or that you dislike.
    (You can currently filter out content we can't display by using the 'Text' media filter.)
Added new sources:
    r/ahegao
    r/consentacles
    r/hentaibondage
    r/balls
    r/bara
    r/barebackcum
    r/BelAmi
    r/blackballed
    r/BlackMale
    r/BlackTwinks
    r/boysgonewild
    r/broslikeus
    r/Bulges
    r/CelebrityPenis
    r/Chastity
    r/Chesthairporn
    r/CockOutline
    r/CondomToBareback
    r/CumCannonAddicts
    r/CutCocks
    r/cuteguys
    r/DickSlips
    r/embraceyourpenis</pre></div></div></div></div></div></div></div></div></div></div> <!----> <div class="AIF-AppFrame__ControlsShortcut"><i class="fa fa-fw fa-gamepad"></i></div> <div class="AIF-SiteFooter AIF-AppPageContainer"><div class="AIF-SiteFooter__Copyright">
        Copyright © 2018 AIFap. All rights reserved.
    </div> <div class="AIF-SiteFooter__Links"><a href="/legal/privacy" class="AIF-SiteFooter__Link">
            Privacy Policy
        </a> <a href="/legal/dmca" class="AIF-SiteFooter__Link">
            DMCA
        </a></div></div> <div class="AIF-AlertMessage"><div class="AIF-AppPageContainer"><div class="AIF-AlertMessage__Message">
            
        </div></div></div></div></div></div>
        
    <script src="{{ mix('js/app.js') }}"></script>
    <script>
        window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
        ga('create', 'UA-130281211-1', 'auto');
        ga('require', 'urlChangeTracker');
        ga('require', 'cleanUrlTracker', {
            stripQuery: true,
            trailingSlash: 'remove',
        });
        ga('send', 'pageview');
    </script>
    <script async src="https://www.google-analytics.com/analytics.js"></script>
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/autotrack/2.4.1/autotrack.js"></script>
</body>
</html>
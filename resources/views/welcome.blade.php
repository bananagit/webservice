<!doctype html>
<html lang="en">
    <head>
        <meta name="trafficjunky-site-verification" content="soyqz92vt" />
        <meta name="juicyads-site-verification" content="f5384156d6e859a56fc95f0b09244170">
    
        <meta charset="utf-8">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>AIFap - Improving porn with AI and machine learning</title>
        <meta name="description" content="AIFap is a porn site that uses AI to learn your preferences and recommend things we think you'll like.">
        <meta itemprop="name" content="AIFap - Improving porn with AI and machine learning">
        <meta itemprop="description" content="AIFap is a porn site that uses AI to learn your preferences and recommend things we think you'll like.">
        <meta name="og:title" content="AIFap - Improving porn with AI and machine learning">
        <meta name="og:description" content="AIFap is a porn site that uses AI to learn your preferences and recommend things we think you'll like.">
        <meta name="og:site_name" content="AIFap">
        <meta name="og:type" content="website">
        
        <meta name="google-site-verification" content="3hiloSIHOjdaBPoIkLLTLnLOqP-napxV6eVdoYrkUY0" />
        
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#000000">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#000000">
        
        <meta name="trafficjunky-site-verification" content="soyqz92vt" />
    </head>
    <body>
        <!-- JuicyAds v3.2P Start -->
        <script type="text/javascript">
        var juicy_tags = ['a', 'img'];
        </script>
        <script type="text/javascript" src="https://js.juicyads.com/jp.php?c=34841333p284u4q2r2d453b494&u=https%3A%2F%2Fwww.patreon.com%2Faif"></script>
        <!-- JuicyAds v3.2P End -->
        
        <div id="app">
            <div>
                This site requires Javascript.
            </div>
            <a href="https://www.enable-javascript.com/">
                https://www.enable-javascript.com/
            </a>
        </div>
        
        <script src="{{ mix('js/app.js') }}"></script>
        <script>
            window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
            ga('create', 'UA-130281211-1', 'auto');
            ga('require', 'urlChangeTracker');
            ga('require', 'cleanUrlTracker', {
                stripQuery: true,
                trailingSlash: 'remove',
            });
            ga('send', 'pageview');
        </script>
        <script async src="https://www.google-analytics.com/analytics.js"></script>
        <script async src="https://cdnjs.cloudflare.com/ajax/libs/autotrack/2.4.1/autotrack.js"></script>
    </body>
</html>